del /q AllTests\Debug
del /q Concurrency\Debug
del /q Concurrency\ConcurrencyTest\Debug
del /q CppUnitLite\Debug
del /q CppUnitLite\CppUnitLite\Debug
del /q CppUnitLite\CppUnitLiteTestMain\Debug
del /q Fit\FitAdapters\Debug
del /q Fit\FitLib\Debug
del /q Fit\FitServer\Debug
del /q Fit\FitUnitTestMain\Debug
del /q Platforms\VisualCpp\Debug
del /s lib\*.lib
del /s lib\*.exe
pause
