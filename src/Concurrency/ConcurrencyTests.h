//
// Copyright (c) 2004 James Grenning
// Released under the terms of the GNU General Public License version 2 or later.
//

#ifndef D_ConcurrencyTests_H
#define D_ConcurrencyTests_H

IMPORT_TEST_GROUP(ActiveObject)
IMPORT_TEST_GROUP(Delay)
IMPORT_TEST_GROUP(Event)
IMPORT_TEST_GROUP(Mutex)
IMPORT_TEST_GROUP(ScopeLock)
IMPORT_TEST_GROUP(Socket)
IMPORT_TEST_GROUP(Thread)
IMPORT_TEST_GROUP(Timer)
IMPORT_TEST_GROUP(FlagSettingAction);
EXPORT_TEST_GROUP(FlagSettingAction);

#endif

