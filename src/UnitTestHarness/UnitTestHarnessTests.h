//
// Copyright (c) 2004 Michael Feathers and James Grenning
// Released under the terms of the GNU General Public License version 2 or later.
//

#ifndef D_UnitTestHarnessTests_H
#define D_UnitTestHarnessTests_H

///////////////////////////////////////////////////////////////////////////////
//
//  UnitTestHarnessTests.h
//
//  This file is included in one or more main()'s to bring in the statically
//  defined tests
//
///////////////////////////////////////////////////////////////////////////////

IMPORT_TEST_GROUP(Failure);
IMPORT_TEST_GROUP(Utest);
IMPORT_TEST_GROUP(TestOutput);
IMPORT_TEST_GROUP(SimpleString);
IMPORT_TEST_GROUP(TestInstaller);
IMPORT_TEST_GROUP(NullTest);
IMPORT_TEST_GROUP(MemoryLeakWarningTest);

#endif

