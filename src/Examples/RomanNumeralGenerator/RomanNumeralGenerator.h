#pragma once

#include "DomainSteps.h"
#include <string>

class RomanNumeralGenerator
{
    int value;
    std::string romanNumeral;

public:
    RomanNumeralGenerator(void);
public:
    ~RomanNumeralGenerator(void);

    void enter(int x);
    void convert();
    bool checkTranslation(std::string expected);
    std::string display();

DOMAIN_STEPS(RomanNumeralGenerator);

};
