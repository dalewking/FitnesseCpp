#include "RomanNumeralGenerator.h"
#include "DomainStep.h"

RomanNumeralGenerator::RomanNumeralGenerator(void) : value(0), romanNumeral("")
{
}

RomanNumeralGenerator::~RomanNumeralGenerator(void)
{
}

DOMAIN_STEP("I enter (\\d+)", RomanNumeralGenerator, enter)
void RomanNumeralGenerator::enter(int x) {
	value = x;
}

struct conversion {
    int value;
    std::string romanNumeral;
    conversion(int i, std::string rn) : value(i), romanNumeral(rn) {}
};

DOMAIN_STEP("I translate to roman numerals", RomanNumeralGenerator, convert)
void RomanNumeralGenerator::convert() {
    int number = value;
    romanNumeral = "";
    // Validate
    if (number < 0 || number > 3999)
    {
        throw std::invalid_argument("Value must be in the range 0 - 3,999.");
    }
    if (number == 0) romanNumeral = "N";

    // Set up key numerals and numeral pairs
    std::vector<conversion> values;
    values.push_back(conversion(1000,"M"));
    values.push_back(conversion(900,"CM"));
    values.push_back(conversion(500,"D"));
    values.push_back(conversion(400,"CD"));
    values.push_back(conversion(100,"C"));
    values.push_back(conversion(90,"XC"));
    values.push_back(conversion(50,"L"));
    values.push_back(conversion(40,"XL"));
    values.push_back(conversion(10,"X"));
    values.push_back(conversion(9,"IX"));
    values.push_back(conversion(5,"V"));
    values.push_back(conversion(4,"IV"));
    values.push_back(conversion(1,"I"));

    for (int i = 0; i < 13; i++)
    {
        // If the number being converted is less than the test value, append
        // the corresponding numeral or numeral pair to the resultant string
        while (number >= values[i].value)
        {
            number -= values[i].value;
            romanNumeral += values[i].romanNumeral;
        }
    }
}

DOMAIN_STEP("I get (.*)", RomanNumeralGenerator, checkTranslation)
bool RomanNumeralGenerator::checkTranslation(std::string expected) {
	return romanNumeral == expected;
}

DOMAIN_STEP("What I get", RomanNumeralGenerator, display)
std::string RomanNumeralGenerator::display() {
	return romanNumeral;
}


REGISTER_CLASS(RomanNumeralGenerator)
	WITH_CONSTRUCTOR(RomanNumeralGenerator)
	WITH_1_ARG_METHOD(void, RomanNumeralGenerator, enter, int)
	WITH_METHOD(void, RomanNumeralGenerator, convert)
    WITH_1_ARG_METHOD(bool, RomanNumeralGenerator, checkTranslation, std::string)
    WITH_METHOD(std::string, RomanNumeralGenerator, display)
END_REGISTRATION
