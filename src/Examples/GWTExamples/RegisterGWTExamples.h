#ifndef __REGISTER_GWT_EXAMPLES__
#define __REGISTER_GWT_EXAMPLES__

#if GWTEXAMPLES_DLL
#ifdef EXPORT_GWTEXAMPLES
#define EXPORTING_GWTEXAMPLES __declspec(dllexport)
#else
#define EXPORTING_GWTEXAMPLES __declspec(dllimport)
#endif
#else
#define EXPORTING_GWTEXAMPLES
#endif

#if STATIC_BUILD
namespace gwtexample {
	EXPORTING_GWTEXAMPLES void registerGwtExample();
};
#else
extern "C" {
	EXPORTING_GWTEXAMPLES void registerClasses();
};
#endif

#endif	//	__REGISTER_GWT_EXAMPLES__
