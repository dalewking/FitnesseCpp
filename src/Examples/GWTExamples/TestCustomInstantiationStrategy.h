#ifndef __TESTCUSTOMINSTANTIATIONSTRATEGY__ 
#define __TESTCUSTOMINSTANTIATIONSTRATEGY__

#include "DomainStepsForSlim.h"

#include <vector>

namespace slim {
	class AbstractInstance;
};

namespace gwt {
	class MarkedClass;
};

class TestCustomInstantiationStrategy : public gwt::Instantiator
{
	std::vector<slim::AbstractInstance*>& instancesCreated;

public:
	TestCustomInstantiationStrategy(std::vector<slim::AbstractInstance*>& instances);
	~TestCustomInstantiationStrategy(void);

	bool operator()(gwt::MarkedClass* markedClass);
	std::vector<slim::AbstractInstance*>& getInstances();

};

#endif  //  __TESTCUSTOMINSTANTIATIONSTRATEGY__
