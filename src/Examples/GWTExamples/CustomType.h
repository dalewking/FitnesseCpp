#ifndef __CUSTOMTYPE__ 
#define __CUSTOMTYPE__

#include <string>

class CustomType
{
	std::string arg;

public:
	CustomType(void);
	CustomType(const CustomType& rhs);
	bool operator ==(const CustomType& rhs);
	void operator =(const CustomType& rhs);
	~CustomType(void);

	void setArg(const std::string& value);
	std::string getArg() const;
};

std::ostream& operator<<(std::ostream& s, CustomType state);
std::istream& operator>>(std::istream& s, CustomType& state);

#endif  //  __CUSTOMTYPE__
