#include "TestCustomState.h"
#include <iostream>

TestCustomState::TestCustomState(void) : value("not set") {
}

TestCustomState::TestCustomState(const TestCustomState& rhs) : value(rhs.getValue()) {
}

TestCustomState::~TestCustomState(void)
{
}

void TestCustomState::setValue(const std::string& newValue) {
	value = newValue;
}

std::string TestCustomState::getValue() const {
	return value;
}

std::ostream& operator<<(std::ostream& s, TestCustomState state) {
	s << state.getValue() << std::flush;
	return s;
}

std::istream& operator>>(std::istream& s, TestCustomState& state) {
	std::string value;
	s >> value;
	state.setValue(value);
	return s;
}

