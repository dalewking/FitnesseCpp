#include "ComplexSteps.h"
#include "DomainStepsForSlim.h"
#include "DomainStep.h"
#include "CustomInstantiationSteps.h"
#include "TestCustomState.h"
#include "TestCustomInstantiationStrategy.h"
#include "MarkedClass.h"
#include "Class.h"
#include "Instances.h"
#include "conversion.h"
#include "StepsUsingCustomTestState.h"

#include <algorithm>

ComplexSteps::ComplexSteps(void) : gwz(0) {
}

ComplexSteps::~ComplexSteps(void) {
}

DOMAIN_STEP("a custom package for step classes", ComplexSteps, createGivWenZenWithCustomPackage)
void ComplexSteps::createGivWenZenWithCustomPackage() {
	gwz = new gwt::DomainStepsForSlim();
	throw std::runtime_error("custom packages not supported");
}

DOMAIN_STEP("a step '(.*)' defined in a class in the custom package", ComplexSteps, verifyCustomStepIsFound)
void ComplexSteps::verifyCustomStepIsFound(std::string customStep) {
    if (gwz == 0) return;
    gwz->given(customStep);
}

DOMAIN_STEP("the step in the custom package '(.*)' is called", ComplexSteps, callCustomStep)
void ComplexSteps::callCustomStep(std::string customStep) {
    calledSteps.push_back(gwz->when(customStep));
}

DOMAIN_STEP("the step in the custom package '(.*)' is found and executed successfully", ComplexSteps, stepExecutedSuccessfully)
bool ComplexSteps::stepExecutedSuccessfully(std::string customStep) {
	return calledStepsContains(customStep);
}

DOMAIN_STEP("the step in the step class created by the custom instantiation strategy is executed successfully", ComplexSteps, verifyCustomInstantionStepWasCalled)
bool ComplexSteps::verifyCustomInstantionStepWasCalled() {
	return calledStepsContains(CustomInstantiationSteps::CUSTOM_INSTANTIATION_STEP);
}

DOMAIN_STEP("a custom state object is used", ComplexSteps, createGWZWithCustomState)
void ComplexSteps::createGWZWithCustomState() {
	FIND_MARKED_CLASS(StepsUsingCustomTestState);
	FIND_MARKED_CLASS(CustomInstantiationSteps);

	std::vector<std::string> customState;
	TestCustomState state;
	std::string s;
	customState.push_back(slim::Convert<TestCustomState, std::string>(state, s));
	gwz = new gwt::DomainStepsForSlim(customState);
}

DOMAIN_STEP("a step class that is created by the custom instantiation strategy", ComplexSteps, stepClassExistsAndCanBeInstantiatedByCustomInstantiationStrategy)
bool ComplexSteps::stepClassExistsAndCanBeInstantiatedByCustomInstantiationStrategy() {
	if (slim::Instances::hasInstance("CustomInstantiationSteps_target")) return false;
	std::vector<slim::AbstractInstance*> instnces;
	TestCustomInstantiationStrategy instantiator(instnces);
	slim::Class<CustomInstantiationSteps>* clas = new slim::Class<CustomInstantiationSteps>();
	gwt::MarkedClass* markedClass = new gwt::MarkedClass(clas);
	instantiator(markedClass);
	return slim::Instances::getInstance("CustomInstantiationSteps_target") != 0;
}

DOMAIN_STEP("a step is called that exists in the step class created by the custom instantiation strategy", ComplexSteps, callStepInCustomInstantiationSteps)
void ComplexSteps::callStepInCustomInstantiationSteps() {
	calledSteps.push_back(gwz->when(CustomInstantiationSteps::CUSTOM_INSTANTIATION_STEP));
}

DOMAIN_STEP("a custom instantiation strategy for creating custom step classes", ComplexSteps, createGWZWithCustomInstantiationStrategy)
void ComplexSteps::createGWZWithCustomInstantiationStrategy() {
	FIND_MARKED_CLASS(CustomInstantiationSteps);
	std::vector<slim::AbstractInstance*> instances;
    gwz = new gwt::DomainStepsForSlim(new TestCustomInstantiationStrategy(instances));
}

DOMAIN_STEP("another step has access to (.*) in the custom state", ComplexSteps, callAStepThatRetrieveStateValue)
bool ComplexSteps::callAStepThatRetrieveStateValue(std::string value) {
    if (gwz == 0) return false;
    return gwz->then("retrieve custom state") == value;
}

DOMAIN_STEP("calling a step that sets (.*) in the custom state", ComplexSteps, callAStepAndSetStateValue)
void ComplexSteps::callAStepAndSetStateValue(std::string value) {
    if (gwz == 0) return;
    gwz->when("set value " + value + " in custom state");
}

bool ComplexSteps::calledStepsContains(const std::string& step) {
	return std::find(calledSteps.begin(), calledSteps.end(), step) != calledSteps.end();
}


REGISTER_CLASS(ComplexSteps)
	WITH_CONSTRUCTOR(ComplexSteps)
	WITH_METHOD(void, ComplexSteps, createGivWenZenWithCustomPackage)
	WITH_1_ARG_METHOD(void, ComplexSteps, verifyCustomStepIsFound, std::string)
	WITH_1_ARG_METHOD(void, ComplexSteps, callCustomStep, std::string)
	WITH_1_ARG_METHOD(bool, ComplexSteps, stepExecutedSuccessfully, std::string)
	WITH_METHOD(bool, ComplexSteps, verifyCustomInstantionStepWasCalled)
	WITH_METHOD(void, ComplexSteps, createGWZWithCustomState)
	WITH_METHOD(bool, ComplexSteps, stepClassExistsAndCanBeInstantiatedByCustomInstantiationStrategy)
	WITH_METHOD(void, ComplexSteps, callStepInCustomInstantiationSteps)
	WITH_METHOD(void, ComplexSteps, createGWZWithCustomInstantiationStrategy)
	WITH_1_ARG_METHOD(bool, ComplexSteps, callAStepThatRetrieveStateValue, std::string)
	WITH_1_ARG_METHOD(void, ComplexSteps, callAStepAndSetStateValue, std::string)
END_REGISTRATION
