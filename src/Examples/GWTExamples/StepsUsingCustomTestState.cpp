#include "StepsUsingCustomTestState.h"
#include "DomainStep.h"

StepsUsingCustomTestState::StepsUsingCustomTestState(TestCustomState customState) : state(customState) {
}

StepsUsingCustomTestState::~StepsUsingCustomTestState() {
}

DOMAIN_STEP("set value (.+) in custom state", StepsUsingCustomTestState, setStateValue)
void StepsUsingCustomTestState::setStateValue(std::string value) {
	state.setValue(value);
}

DOMAIN_STEP("retrieve custom state", StepsUsingCustomTestState, getStateValue)
std::string StepsUsingCustomTestState::getStateValue() {
	return state.getValue();
}


REGISTER_CLASS(StepsUsingCustomTestState)
	WITH_1_ARG_CONSTRUCTOR(StepsUsingCustomTestState, TestCustomState)
	WITH_1_ARG_METHOD(void, StepsUsingCustomTestState, setStateValue, std::string)
	WITH_METHOD(std::string, StepsUsingCustomTestState, getStateValue)
END_REGISTRATION
