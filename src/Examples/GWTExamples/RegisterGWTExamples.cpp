#include "RegisterGWTExamples.h"
#include "../GWT4cpp/DomainSteps.h"
#include "Calculator.h"
#include "ComplexSteps.h"
#include "SimpleTestSteps.h"
#include "RomanNumeralGenerator.h"

#if STATIC_BUILD
namespace gwtexample {
    void registerGwtExample() {
#else
extern "C" {
	void registerClasses() {
#endif
		FIND_MARKED_CLASS(Calculator);
		FIND_MARKED_CLASS(ComplexSteps);
		FIND_MARKED_CLASS(SimpleTestSteps);
		FIND_MARKED_CLASS(RomanNumeralGenerator);
	}
};

