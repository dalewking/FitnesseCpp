#ifndef __STEPSUSINGCUSTOMTESTSTATE__ 
#define __STEPSUSINGCUSTOMTESTSTATE__

#include "TestCustomState.h"
#include "DomainSteps.h"
#include <string>

class StepsUsingCustomTestState
{
	TestCustomState state;

public:
	DOMAIN_STEPS(StepsUsingCustomTestState);

	StepsUsingCustomTestState(TestCustomState state);
	~StepsUsingCustomTestState();

	void setStateValue(std::string value);
	std::string getStateValue();
};

#endif  //  __STEPSUSINGCUSTOMTESTSTATE__
