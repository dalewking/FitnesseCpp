#include "CustomType.h"
#include <iostream>

CustomType::CustomType(void):arg("")
{
}

CustomType::CustomType(const CustomType& rhs) : arg(rhs.arg) {
}

CustomType::~CustomType(void)
{
}

void CustomType::setArg(const std::string& value) {
	arg = value;
}

std::string CustomType::getArg() const {
	return arg;
}

bool CustomType::operator ==(const CustomType& rhs) {
	return arg == rhs.arg;
}

void CustomType::operator =(const CustomType& rhs) {
	arg = rhs.arg;
}

std::ostream& operator<<(std::ostream& s, CustomType state) {
	s << state.getArg() << std::flush;
	return s;
}

std::istream& operator>>(std::istream& s, CustomType& state) {
	std::string arg;
	s >> arg;
	state.setArg(arg);
	return s;
}

