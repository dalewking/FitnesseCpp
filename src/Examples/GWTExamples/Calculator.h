#ifndef __CALCULATOR__ 
#define __CALCULATOR__

#include "DomainSteps.h"
#include <iostream>
#include <stack>

class Calculator
{
	std::stack<int> values;

public:
	Calculator(void);
	~Calculator(void);

	void enter(int x);
	void add();

	bool checkTotal(int expectedValue);
	int displayTotal();

	DOMAIN_STEPS(Calculator);
};

#endif  //  __CALCULATOR__
