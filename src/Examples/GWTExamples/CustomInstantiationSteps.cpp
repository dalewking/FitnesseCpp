#include "CustomInstantiationSteps.h"
#include "DomainStep.h"

const std::string CustomInstantiationSteps::CUSTOM_INSTANTIATION_STEP = "custom instantiation step";

CustomInstantiationSteps::CustomInstantiationSteps(void)
{
}

CustomInstantiationSteps::~CustomInstantiationSteps(void)
{
}

DOMAIN_STEP("custom instantiation step", CustomInstantiationSteps, domainStep)
std::string CustomInstantiationSteps::domainStep() {
	return CUSTOM_INSTANTIATION_STEP;
}


REGISTER_CLASS(CustomInstantiationSteps)
	WITH_CONSTRUCTOR(CustomInstantiationSteps)
	WITH_METHOD(std::string, CustomInstantiationSteps, domainStep)
END_REGISTRATION
