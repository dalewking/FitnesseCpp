#ifndef __SIMPLETESTSTEPS__ 
#define __SIMPLETESTSTEPS__

#include "DomainSteps.h"
#include "CustomType.h"

#include <map>
#include <string>

namespace gwt {
	class DomainStepNames;
};


class SimpleTestSteps
{
	std::map<std::string, bool> stepCalls;
	int paramValue;
	CustomType customTypeParam;

public:
	SimpleTestSteps();
	~SimpleTestSteps(void);

	  void verifyStepExists(std::string step);
	  void callStep(std::string stepToCall);
	  bool stepExecutedSuccessfully(std::string executedStep);
	  void simpleNoParamStep();
	  void simpleIntParamTest(int intParam);
	  void simpleCustomTypeParamTest(CustomType customType);
	  bool verifyParamValue(int paramValue);
	  bool veryifyCustomTypeParam(CustomType customType);
	  void checkTheCustomTypeHasAPropertyEditor();

	  DOMAIN_STEPS(SimpleTestSteps)
};

#endif  //  __SIMPLETESTSTEPS__
