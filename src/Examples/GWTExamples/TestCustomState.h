#ifndef __TEST_CUSTOM_STATE__
#define __TEST_CUSTOM_STATE__

#include <string>

class TestCustomState
{
	std::string value;
public:
	TestCustomState(void);
	TestCustomState(const TestCustomState& rhs);
	~TestCustomState(void);

	void setValue(const std::string& newValue);
	std::string getValue() const;
};

std::ostream& operator<<(std::ostream& s, TestCustomState state);
std::istream& operator>>(std::istream& s, TestCustomState& state);

#endif  //  __TEST_CUSTOM_STATE__
