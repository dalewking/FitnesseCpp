#ifndef __CUSTOMINSTANTIATIONSTEPS__ 
#define __CUSTOMINSTANTIATIONSTEPS__

#include "DomainSteps.h"
#include <string>

class CustomInstantiationSteps
{
public:
	DOMAIN_STEPS(CustomInstantiationSteps);

	static const std::string CUSTOM_INSTANTIATION_STEP;
	CustomInstantiationSteps(void);
	~CustomInstantiationSteps(void);

	std::string domainStep();
};

#endif  //  __CUSTOMINSTANTIATIONSTEPS__
