#include "Calculator.h"
#include "DomainStep.h"

Calculator::Calculator(void) {
}

Calculator::~Calculator(void) {
}

DOMAIN_STEP("i have entered (\\d+) into the calculator", Calculator, enter)
void Calculator::enter(int x) {
	values.push(x);
}

DOMAIN_STEP("i press add", Calculator, add)
void Calculator::add() {
    if (values.size() < 2) return;
	int total = values.top();
	values.pop();
	total += values.top();
	values.pop();
	values.push(total);
}

DOMAIN_STEP("the total is (\\d+)", Calculator, checkTotal)
bool Calculator::checkTotal(int expectedValue) {
	return values.top() == expectedValue;
}

DOMAIN_STEP("what is the total", Calculator, displayTotal)
int Calculator::displayTotal() {
	return values.top();
}


REGISTER_CLASS(Calculator)
	WITH_CONSTRUCTOR(Calculator)
	WITH_METHOD(void, Calculator, add)
	WITH_1_ARG_METHOD(void, Calculator, enter, int)
	WITH_1_ARG_METHOD(bool, Calculator, checkTotal, int)
	WITH_METHOD(int, Calculator, displayTotal)
END_REGISTRATION
