#include "SimpleTestSteps.h"
#include "DomainStep.h"
#include "DomainStepNames.h"
#include "DomainStepNamesInjectingInstantiator.h"
#include "DomainStepsForSlim.h"
#include "DomainStepNamesAccessor.h"

SimpleTestSteps::SimpleTestSteps()
{
}

SimpleTestSteps::~SimpleTestSteps(void)
{
}

gwt::DomainStepNames& gwz() {
    gwt::DomainStepNamesAccessor accessor;
    return accessor.getDomainStepNames();
}

DOMAIN_STEP("a step annotated with '(.*)' .*", SimpleTestSteps, verifyStepExists)
void SimpleTestSteps::verifyStepExists(std::string step) {
	gwz().given(step);
}

DOMAIN_STEP("the step '(.*)' is called", SimpleTestSteps, callStep)
void SimpleTestSteps::callStep(std::string stepToCall) {
	gwz().when(stepToCall);
}

DOMAIN_STEP("the step '(.*)' executes successfully", SimpleTestSteps, stepExecutedSuccessfully)
bool SimpleTestSteps::stepExecutedSuccessfully(std::string executedStep) {
	return stepCalls[executedStep] == true;
}

DOMAIN_STEP("simple no parameter step", SimpleTestSteps, simpleNoParamStep)
void SimpleTestSteps::simpleNoParamStep() {
	stepCalls["simple no parameter step"] = true;
}

DOMAIN_STEP("simple step with int parameter (\\d+)", SimpleTestSteps, simpleIntParamTest)
void SimpleTestSteps::simpleIntParamTest(int intParam) {
    std::ostringstream buff;
    buff << "simple step with int parameter " << intParam;
	stepCalls[buff.str()] = true;
	paramValue = intParam;
}

DOMAIN_STEP("simple step with custom type parameter (.*)", SimpleTestSteps, simpleCustomTypeParamTest)
void SimpleTestSteps::simpleCustomTypeParamTest(CustomType customType) {
	stepCalls["simple step with custom type parameter CustomType"] = true;
	customTypeParam = customType;
}

DOMAIN_STEP("the int value (\\d+) is passed as a parameter", SimpleTestSteps, verifyParamValue)
bool SimpleTestSteps::verifyParamValue(int paramValue) {
	return this->paramValue == paramValue;
}

DOMAIN_STEP("the custom value (.*) is passed as a parameter", SimpleTestSteps, veryifyCustomTypeParam)
bool SimpleTestSteps::veryifyCustomTypeParam(CustomType customType) {
	return customType == customTypeParam;
}

DOMAIN_STEP("the CustomType has a CustomTypeEditor in the same package as the CustomType", SimpleTestSteps, checkTheCustomTypeHasAPropertyEditor)
void SimpleTestSteps::checkTheCustomTypeHasAPropertyEditor() {

}

REGISTER_CLASS(SimpleTestSteps)
    WITH_CONSTRUCTOR(SimpleTestSteps)
	WITH_1_ARG_METHOD(void, SimpleTestSteps, verifyStepExists, std::string)
	WITH_1_ARG_METHOD(void, SimpleTestSteps, callStep, std::string) 
	WITH_1_ARG_METHOD(bool, SimpleTestSteps, stepExecutedSuccessfully, std::string)
    WITH_1_ARG_METHOD(void, SimpleTestSteps, simpleCustomTypeParamTest, CustomType)
    WITH_METHOD(void, SimpleTestSteps, checkTheCustomTypeHasAPropertyEditor)
    WITH_1_ARG_METHOD(bool, SimpleTestSteps, veryifyCustomTypeParam, CustomType)
    WITH_1_ARG_METHOD(void, SimpleTestSteps, simpleIntParamTest, int)
    WITH_1_ARG_METHOD(bool, SimpleTestSteps, verifyParamValue, int)
    WITH_METHOD(void, SimpleTestSteps, simpleNoParamStep)
END_REGISTRATION
