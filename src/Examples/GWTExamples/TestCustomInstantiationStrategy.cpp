#include "TestCustomInstantiationStrategy.h"
#include "MarkedClass.h"
#include "CustomInstantiationSteps.h"
#include "Instances.h"

TestCustomInstantiationStrategy::TestCustomInstantiationStrategy(std::vector<slim::AbstractInstance*>& instances) : instancesCreated(instances)
{
}

TestCustomInstantiationStrategy::~TestCustomInstantiationStrategy(void)
{
}

bool TestCustomInstantiationStrategy::operator()(gwt::MarkedClass* markedClass) {
	if (markedClass->getName() != "CustomInstantiationSteps") return false;
    
	slim::Instance<CustomInstantiationSteps>* instance = 
		new slim::Instance<CustomInstantiationSteps>(dynamic_cast<slim::Class<CustomInstantiationSteps> &>(*markedClass->getClass()), 
							new CustomInstantiationSteps());
	slim::Instances::registerInstance("CustomInstantiationSteps_target", instance);
	instancesCreated.push_back(instance);
    return true;
}

std::vector<slim::AbstractInstance*>& TestCustomInstantiationStrategy::getInstances() {
	return instancesCreated;
}
