#ifndef __COMPLEXSTEPS__ 
#define __COMPLEXSTEPS__

#include "DomainSteps.h"
#include <string>
#include <vector>

namespace gwt {
	class DomainStepsForSlim;
};

class ComplexSteps
{
	gwt::DomainStepsForSlim* gwz;
	std::vector<std::string> calledSteps;

public:
	DOMAIN_STEPS(ComplexSteps);

	ComplexSteps(void);
	~ComplexSteps(void);

    void createGivWenZenWithCustomPackage();

	void verifyCustomStepIsFound(std::string customStep);

    void callCustomStep(std::string customStep);

	bool stepExecutedSuccessfully(std::string customStep);

    bool verifyCustomInstantionStepWasCalled();

    void createGWZWithCustomState();

    bool stepClassExistsAndCanBeInstantiatedByCustomInstantiationStrategy();

    void callStepInCustomInstantiationSteps();

    void createGWZWithCustomInstantiationStrategy();

	bool callAStepThatRetrieveStateValue(std::string value);

	void callAStepAndSetStateValue(std::string value);

private:
	bool calledStepsContains(const std::string& step);
};

#endif  //  __COMPLEXSTEPS__
