/* 
 * this file exists to make up for a problem in VS.NET
 * VS.NET will not create a library without anything in it (as VC6 will).
 * This file can be deleted once you add your own files into this library
 */

int __dummy__dummy = 0;