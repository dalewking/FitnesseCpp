//
// Copyright (c) 2004 James Grenning
// Released under the terms of the GNU General Public License version 2 or later.
//

#ifndef HomeGuardInit_H
#define HomeGuardInit_H

extern "C"
{
#include "../HomeGuardLib/HomeGuard.h"
}

void HomeGuardInit();
void HomeGuardDestroy();

#endif
