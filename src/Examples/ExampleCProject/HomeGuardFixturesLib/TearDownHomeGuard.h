//
// Copyright (c) 2004 James Grenning
// Released under the terms of the GNU General Public License version 2 or later.
//

#ifndef D_TearDown_h
#define D_TearDown_h

#include <Fit/Fixture.h>

class TearDownHomeGuard : public Fixture 
{
  public:
    TearDownHomeGuard();

};

#endif  // D_TearDown_h


