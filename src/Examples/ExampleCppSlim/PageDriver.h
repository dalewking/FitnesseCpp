#ifndef __PAGE_DRIVER_H__
#define __PAGE_DRIVER_H__

#include <SlimLib/RegisterClass.h>

#include <map>
#include <string>

class PageDriver
{
	std::map<std::string, std::string> pagesByName;
	std::string currentPage;

public:
	DECLARE_CLASS_REGISTRATION(PageDriver)

	PageDriver() {}
	~PageDriver() {}

	void createPageWithContent(std::string pageName, std::string htmlText) {
		pagesByName[pageName] = htmlText;
	}

	int requestPage(std::string pageName) {
		if (pagesByName.find(pageName) == pagesByName.end()) return 404;
		currentPage = pagesByName[pageName];
		return 200;
	}

	bool contentMatches(std::string expectedContent) {
		return currentPage == expectedContent;
	}

	std::string content() {
		return currentPage;
	}
};

#endif	//	__PAGE_DRIVER_H__
