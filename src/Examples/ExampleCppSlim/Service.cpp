#include "Service.h"

REGISTER_CLASS(Service)
	WITH_CONSTRUCTOR(Service)
    WITH_1_ARG_METHOD(void, Service, createPerson, std::string)
    WITH_1_ARG_METHOD(bool, Service, exists, std::string)
END_REGISTRATION

