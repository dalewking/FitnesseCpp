#include "EmployeesHiredBefore.h"


std::ostream& operator >>(std::ostream& ostr, Date& date) {
	return ostr;
}

REGISTER_CLASS(EmployeesHiredBefore)
	WITH_1_ARG_CONSTRUCTOR(EmployeesHiredBefore, Date)
	WITH_METHOD(StringList, EmployeesHiredBefore, query)
END_REGISTRATION
