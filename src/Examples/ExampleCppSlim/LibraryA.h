#ifndef __LIBRARY_A__
#define __LIBRARY_A__

#include <SlimLib/RegisterClass.h>

#include <string>

class LibraryA {
public:
    std::string outputFromLibraryA() { return "A"; }
    std::string outputFromACommonMethod() { return "A"; }
	DECLARE_CLASS_REGISTRATION(LibraryA)
};

#endif  //  __LIBRARY_A__
