#ifndef __EMPLOYEES_HIRED_BEFORE_H__
#define __EMPLOYEES_HIRED_BEFORE_H__

#include <SlimLib/SlimList.h>
#include <SlimLib/encoding.h>
#include <SlimLib/RegisterClass.h>

#include <iostream>

class Date {
};

std::ostream& operator >>(std::ostream& ostr, Date& date);

class EmployeesHiredBefore {
private:
    Date date;

    typedef slim::StringList StringList;

    StringList list(const std::string& fieldName, const std::string& value) {
        StringList list;
        list.push_back(fieldName);
        list.push_back(value);
        return list;
    }

    StringList list(StringList& empNo, StringList& fname, StringList& lname, StringList& hireDate) {
        StringList list;
        list.push_back(empNo);
        list.push_back(fname);
        list.push_back(lname);
        list.push_back(hireDate);
        return list;
    }

    StringList list(StringList& row1, StringList& row2) {
        StringList list;
        list.push_back(row1);
        list.push_back(row2);
        return list;
    }

    StringList row(const std::string& empNum, const std::string& firstName, const std::string& lastName, const std::string& hireDate) {
        StringList empList = list("employee number", empNum);
        StringList firstNameList = list("first name", firstName);
        StringList lastNameList = list("last name", lastName);
        StringList hireDateList = list("hire date", hireDate);
        return list(empList, firstNameList, lastNameList, hireDateList);
    }


public:
    EmployeesHiredBefore(Date date) {
        this->date = date;
    }

    StringList query() {
        StringList row1 = row("1429", "Bob", "Martin", "10-Oct-1974");
        StringList row2 = row("1429", "Bob", "Martin", "15-Dec-1979");

        return list(row1, row2);
    }

    DECLARE_CLASS_REGISTRATION(EmployeesHiredBefore);
};

#endif  //  __EMPLOYEES_HIRED_BEFORE_H__
