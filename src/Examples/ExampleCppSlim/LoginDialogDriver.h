#ifndef __LOGIN_DIALOG_DRIVER_H__
#define __LOGIN_DIALOG_DRIVER_H__

#include <SlimLib/RegisterClass.h>

#include <string>

class LoginDialogDriver
{
public:
	LoginDialogDriver(const std::string& userName, const std::string& password);
	~LoginDialogDriver(void);

	bool loginWithUsernameAndPassword(std::string userName, std::string password);

	std::string loginMessage();
	int numberOfLoginAttempts();
	std::string currentUser();

	DECLARE_CLASS_REGISTRATION(LoginDialogDriver)

private:
	std::string name;
	std::string password;
	std::string message;
	unsigned int loginAttempts;
	std::string user;
};


#endif	//	__LOGIN_DIALOG_DRIVER_H__
