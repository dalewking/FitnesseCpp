#include "LibraryA.h"

	REGISTER_CLASS(LibraryA)
		WITH_CONSTRUCTOR(LibraryA)
		WITH_METHOD(std::string, LibraryA, outputFromLibraryA)
		WITH_METHOD(std::string, LibraryA, outputFromACommonMethod)
	END_REGISTRATION
