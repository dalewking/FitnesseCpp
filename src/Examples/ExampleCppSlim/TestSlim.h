#ifndef __TEST_SLIM_H__
#define __TEST_SLIM_H__

#include <SlimLib/RegisterClass.h>
#include <string>
#include <stdexcept>

class TestSlim
{
public:
	DECLARE_CLASS_REGISTRATION(TestSlim)

	TestSlim() {}
	~TestSlim() {}

	int echoInt(int i) {
		return i;
	}

	std::string echoString(std::string s) {
		return s;
	}

    void throwNormal() {
        throw std::exception("java.lang.Exception: This is my exception");
    }

    void throwStopping() {
        throw std::exception("fitnesse.slim.test.TestSlim$StopTestException: This is a stop test exception");
    }
};

#endif	//	__TEST_SLIM_H__
