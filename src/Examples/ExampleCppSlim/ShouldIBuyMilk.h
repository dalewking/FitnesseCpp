#ifndef __SHOULD_I_BUY_MILK_H__
#define __SHOULD_I_BUY_MILK_H__

#include <SlimLib/RegisterClass.h>
#include <string>

class ShouldIBuyMilk
{
private:
	int dollars;
	int pints;
	bool creditCard;

public:
	DECLARE_CLASS_REGISTRATION(ShouldIBuyMilk)

	ShouldIBuyMilk(){}

	void setCashInWallet(int dollars) {
		this->dollars = dollars;
	}

	void setPintsOfMilkRemaining(int pints) {
		this->pints = pints;
	}

	void setCreditCard(std::string valid) {
		creditCard = "yes" == valid;
	}

	std::string goToStore() {
		return (pints == 0 && (dollars > 2 || creditCard)) ? "yes" : "no";
	}

	// The following functions are optional.  If they aren't declared they'll be ignored.
	void execute() {
	}

	void reset() {
	}

	void table(std::string table) {
	}
};

#endif	//	__SHOULD_I_BUY_MILK_H__
