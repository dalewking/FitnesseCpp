#pragma once

#include "Database.h"
#include "Person.h"
#include <string>
#include <SlimLib/RegisterClass.h>

class Service {

public:
  void createPerson(std::string name) {
      Database::persist(new Person(name));
  }

  bool exists(std::string name) {
      return Database::get(name) != 0;
  }

  DECLARE_CLASS_REGISTRATION(Service)
};
