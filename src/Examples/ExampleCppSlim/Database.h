#pragma once

#include "Person.h"
#include <map>

class Database
{
    Database(void);
    ~Database(void);

    static std::map<std::string, Person*> peopleByName;
    typedef std::map<std::string, Person*>::const_iterator NamePersonPair;

public:
    static void clean() {
		for (NamePersonPair i = peopleByName.begin(); i != peopleByName.end(); i++)
			delete (*i).second;
		peopleByName.clear();
    }

    static void persist(Person* person) {
        peopleByName[person->getName()] = person;
    }

    static Person* get(const std::string& name) {
        if (peopleByName.find(name) != peopleByName.end())
            return peopleByName[name];

        return 0;
    }
};

