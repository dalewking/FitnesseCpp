#include "Register.h"
#include <SlimLib/RegisterClass.h>
#include "LoginDialogDriver.h"
#include "EmployeesHiredBefore.h"
#include "PageDriver.h"
#include "ShouldIBuyMilk.h"
#include "TestSlim.h"
#include "SlimDriver.h"
#include "Service.h"
#include "LibraryA.h"
#include "LibraryB.h"

#if STATIC_BUILD
namespace slimexample {
    void registerSlimExamples() {
#else
extern "C" {
	void registerClasses() {
#endif
		REGISTER(LoginDialogDriver)
		REGISTER(PageDriver);
		REGISTER(TestSlim);
		REGISTER(ShouldIBuyMilk);
		REGISTER(EmployeesHiredBefore);
        REGISTER(SlimDriver);
        REGISTER(Service);
        REGISTER(LibraryA);
        REGISTER(LibraryB);
	}
};

