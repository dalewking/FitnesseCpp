#include "LoginDialogDriver.h"

LoginDialogDriver::LoginDialogDriver(const std::string& userName, const std::string& password) : 
	name(userName), password(password), message(""), loginAttempts(0), user("")
{
}

LoginDialogDriver::~LoginDialogDriver(void)
{
}

bool LoginDialogDriver::loginWithUsernameAndPassword(std::string userName, std::string password) {
	loginAttempts++;
	bool accepted = this->password == password && name == userName;
	message = userName + (accepted ? "" : " not") + " logged in.";
	user = accepted ? userName : "";
	return accepted;
}

std::string LoginDialogDriver::loginMessage() {
	return message;
}

int LoginDialogDriver::numberOfLoginAttempts () {
	return loginAttempts;
}

std::string LoginDialogDriver::currentUser() {
	return user;
}


REGISTER_CLASS(LoginDialogDriver)
	WITH_2_ARG_CONSTRUCTOR(LoginDialogDriver, std::string, std::string)
	WITH_2_ARG_METHOD(bool, LoginDialogDriver, loginWithUsernameAndPassword, std::string, std::string)
	WITH_METHOD(std::string, LoginDialogDriver, loginMessage)
	WITH_METHOD(int, LoginDialogDriver, numberOfLoginAttempts)
	WITH_METHOD(std::string, LoginDialogDriver, currentUser)
END_REGISTRATION
