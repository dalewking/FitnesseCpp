#include "PageDriver.h"

REGISTER_CLASS(PageDriver)
		WITH_CONSTRUCTOR(PageDriver)
		WITH_2_ARG_METHOD(void, PageDriver, createPageWithContent, std::string, std::string)
		WITH_1_ARG_METHOD(int, PageDriver, requestPage, std::string)
		WITH_1_ARG_METHOD(bool, PageDriver, contentMatches, std::string)
		WITH_METHOD(std::string, PageDriver, content)
	END_REGISTRATION

