#pragma once

#include "Database.h"
#include "Service.h"
#include <SlimLib/RegisterClass.h>

class SlimDriver
{
public:

  // field MUST be declared PUBLIC
  Service* service;

  void init() {
      Database::clean();
  }

  DECLARE_CLASS_REGISTRATION(SlimDriver)

};
