#ifndef __LIBRARY_B__
#define __LIBRARY_B__

#include <SlimLib/RegisterClass.h>

#include <string>

class LibraryB {
public:
    std::string outputFromLibraryB() { return "B"; }
    std::string outputFromACommonMethod() { return "B"; }
	DECLARE_CLASS_REGISTRATION(LibraryB)
};

#endif  //  __LIBRARY_B__
