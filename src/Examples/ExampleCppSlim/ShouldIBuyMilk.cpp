#include "ShouldIBuyMilk.h"

	REGISTER_CLASS(ShouldIBuyMilk)
		WITH_CONSTRUCTOR(ShouldIBuyMilk)
		WITH_1_ARG_METHOD(void, ShouldIBuyMilk, setCashInWallet, int)
		WITH_1_ARG_METHOD(void, ShouldIBuyMilk, setCreditCard, std::string)
		WITH_1_ARG_METHOD(void, ShouldIBuyMilk, setPintsOfMilkRemaining, int)
		WITH_METHOD(std::string, ShouldIBuyMilk, goToStore)
	END_REGISTRATION
