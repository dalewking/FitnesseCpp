#ifndef __REGISTER_H__
#define __REGISTER_H__

#if SLIMEXAMPLE_DLL
#ifdef EXPORT_SLIMEXAMPLE
#define EXPORTING_SLIMEXAMPLE __declspec(dllexport)
#else
#define EXPORTING_SLIMEXAMPLE __declspec(dllimport)
#endif
#else
#define EXPORTING_SLIMEXAMPLE
#endif

#if STATIC_BUILD
namespace slimexample {
	EXPORTING_SLIMEXAMPLE void registerSlimExamples();
};
#else
extern "C" {
	EXPORTING_SLIMEXAMPLE void registerClasses();
};
#endif

#endif	//	__REGISTER_H__
