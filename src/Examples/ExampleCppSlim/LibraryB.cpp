#include "LibraryB.h"

	REGISTER_CLASS(LibraryB)
		WITH_CONSTRUCTOR(LibraryB)
		WITH_METHOD(std::string, LibraryB, outputFromLibraryB)
		WITH_METHOD(std::string, LibraryB, outputFromACommonMethod)
	END_REGISTRATION
