#pragma once

#include <string>

class Person
{
    std::string name;
public:

    Person(const std::string& personName) : name(personName)
    {
    }

public:

    ~Person(void)
    {
    }

    std::string getName() const {
        return name;
    }
};
