#include "TestSlim.h"

REGISTER_CLASS(TestSlim)
		WITH_CONSTRUCTOR(TestSlim)
		WITH_1_ARG_METHOD(int, TestSlim, echoInt, int)
		WITH_1_ARG_METHOD(std::string, TestSlim, echoString, std::string)
        WITH_METHOD(void, TestSlim, throwNormal)
        WITH_METHOD(void, TestSlim, throwStopping)
	END_REGISTRATION

