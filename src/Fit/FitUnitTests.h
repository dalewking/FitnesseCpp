#ifndef D_FitUnitTests_H
#define D_FitUnitTests_H

IMPORT_TEST_GROUP(FitnesseServer)
IMPORT_TEST_GROUP(ActionFixture)
IMPORT_TEST_GROUP(Fixture)
IMPORT_TEST_GROUP(Framework)
IMPORT_TEST_GROUP(RowFixture)
IMPORT_TEST_GROUP(Summary)
IMPORT_TEST_GROUP(FixtureMaker)
IMPORT_TEST_GROUP(FitFixtureMaker);

#endif

