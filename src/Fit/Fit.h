// Copyright (c) 2003 Cunningham & Cunningham, Inc.
// Released under the terms of the GNU General Public License version 2 or later.
//
// C++ translation by Michael Feathers <mfeathers@objectmentor.com>

#ifndef FIT_H
#define FIT_H

#include "ActionFixture.h"
#include "ColumnFixture.h"
#include "RowFixture.h"
#include "PrimitiveFixture.h"

#endif
