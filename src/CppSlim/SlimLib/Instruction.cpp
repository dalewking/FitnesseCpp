#include "Instruction.h"
#include "MalformedInstruction.h"
#include "Instances.h"
#include "Log.h"
#include "Flags.h"
#include "SymbolTable.h"
#include "StringListWrapper.h"

#include <algorithm>


namespace slim {

Instruction::Instruction(const std::string& newId) : id(newId), result(createStringListWrapper()), symbols(0) {
}

Instruction::Instruction(const Instruction& rhs) : symbols(rhs.symbols) {
	id = rhs.id;
    result = rhs.result;
}

void Instruction::operator =(const Instruction& rhs) {
	id = rhs.id;
}

Instruction::~Instruction(void) {
    delete result;
}

void Instruction::setSymbolTable(SymbolTable* symbolsTable) {
	symbols = symbolsTable;
}

std::string Instruction::getId(void) {
	return id;
}

std::string Instruction::getCommand(void) {
	return command;
}

std::vector<std::string> Instruction::getArguments() {
    return std::vector<std::string>(arguments.begin(), arguments.end());
}

void Instruction::setCommand(const std::string& c) {
	command = c;
}

void Instruction::addArgument(const std::string& arg) {
	arguments.push_back(arg);
}

void Instruction::addToResult(const std::string& partialResult) {
	result->push_back(partialResult);
}

void Instruction::recordId() {
	addToResult(id);
}

void Instruction::execute() {
	recordId();
}

class ReplaceSymbolsWithValuesFrom {
	SymbolTable& symbols;
public:
	ReplaceSymbolsWithValuesFrom(SymbolTable& symbols) : symbols(symbols){}

	std::string operator()(const std::string& key) {
		std::string value = key;
		if (key.at(0) == '$')
			value = symbols.valueForSymbol(key.substr(1));
		return value;
	}
};

std::vector<std::string> Instruction::lookUpArgs(int argumentOffset) {
	std::vector<std::string> args;
	std::copy(arguments.begin() + argumentOffset, arguments.end(), std::back_inserter(args));
	std::transform(args.begin(), args.end(), args.begin(), ReplaceSymbolsWithValuesFrom(*symbols));
	return args;
}

void Instruction::validateNumberOfArguments(unsigned int expectedCount) {
	if (arguments.size() < expectedCount) throw MalformedInstruction(getCommand());
}

StringListWrapper* Instruction::getResult() const {
    StringListWrapper* answer = result;
    result = 0;
	return answer;
}

} // namespace slim
