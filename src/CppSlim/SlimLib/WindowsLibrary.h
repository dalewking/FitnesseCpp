#ifndef __WINDOWS_LIBRARY__
#define __WINDOWS_LIBRARY__

#include <string>

namespace slim {

    class InstanceHolder;

class WindowsLibrary {
    std::string name;
    InstanceHolder* handle;

public:

    WindowsLibrary(const std::string& path);

    ~WindowsLibrary();

    void init(const std::string& path);

    bool isLoaded() const;

    std::string getName() const;

    void call(const std::string& functionName) const;

    void unload();
};

std::ostream& operator<<(std::ostream& ostr, const WindowsLibrary& lib);

};

#endif  //  __WINDOWS_LIBRARY__
