#include "CallAndAssignInstruction.h"
#include "SymbolTable.h"

namespace slim {

CallAndAssignInstruction::CallAndAssignInstruction(const std::string& id) : CallInstruction(id) {}
void CallAndAssignInstruction::execute() {
	recordId();
	addToResult(callAndAssign());
}
std::string CallAndAssignInstruction::callAndAssign() {
	validateNumberOfArguments(3);
	std::string result = call(1);

	return saveResult(arguments[0], result);
}
std::string CallAndAssignInstruction::saveResult(const std::string& symbolName, const std::string& value) {
	symbols->setSymbol(symbolName, value);
	return value;
}

}
