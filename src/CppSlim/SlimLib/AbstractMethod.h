#ifndef __ABSTRACT_METHOD_H__
#define __ABSTRACT_METHOD_H__

#include "SupportsArguments.h"

namespace slim {

template<class T>
class AbstractMethod : public SupportsArguments {
public:
	virtual std::string invoke(T& t, const std::vector<std::string>& args) const = 0;
};

} // namespace slim

#endif	//	__ABSTRACT_METHOD_H__
