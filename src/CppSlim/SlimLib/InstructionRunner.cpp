#include "InstructionRunner.h"

#include "Instruction.h"
#include "InstructionHandler.h"
#include "ResultSender.h"
#include "ListDeserializer.h"
#include "SlimException.h"
#include "ListUtilities.h"
#include "TestAborted.h"
#include "StringListWrapperImpl.h"
#include "encoding.h"

#include <algorithm>
#include <iostream>
#include <boost/scoped_ptr.hpp>

namespace slim {

const std::string InstructionRunner::BYE = "bye";
InstructionRunner::InstructionRunner(ResultSender* sendTo) : sender(sendTo){}

bool InstructionRunner::process(const std::string& serializedInstructions){
	if (serializedInstructions == BYE) return true;

    StringList instructions;
    try {
        instructions = ListDeserializer::deserialize(serializedInstructions);
    } catch(ListDeserializer::SyntaxError& e) {
        results.push_back(asList("0", e.what(), 0));
        sender->sendResults(results);
        return true;
    }

    try {
    	std::for_each(instructions.begin(), instructions.end(), boost::apply_visitor(*this));
    } catch(TestAborted&) {
    }
	sender->sendResults(results);
	return false;
}

void InstructionRunner::operator()(const std::string& s) {
	//todo throw exception?
}

void InstructionRunner::operator()(const StringList& instructions) {
	InstructionHandler handler;
	std::for_each(instructions.begin(), instructions.end(), boost::apply_visitor(handler));
	handler.finish();
   
	Instruction* inst = handler.nextInstruction();
	while(inst) {
        boost::scoped_ptr<Instruction> instruction(inst);
		try {
			instruction->setSymbolTable(&symbolTable);
			instruction->execute();
            boost::scoped_ptr<StringListWrapperImpl> instructionResult(dynamic_cast<StringListWrapperImpl*>(instruction->getResult()));
			results.push_back(instructionResult->getList());
		} catch(SlimException& ex) {
			handleStopTest(instruction.get(), ex);
			results.push_back(asList(instruction->getId().c_str(), ex.what(), 0));
		} catch(std::exception& ex) {
			handleStopTest(instruction.get(), ex);
            std::string message("__EXCEPTION__:");
            message += ex.what();
			message += "";
            results.push_back(asList(instruction->getId().c_str(), message.c_str(), 0));
        } catch(...) {
            results.push_back(asList(instruction->getId().c_str(), "__EXCEPTION__:unknown exception", 0));
        }
		inst = handler.nextInstruction();
	}
}

void InstructionRunner::handleStopTest(Instruction* instruction, std::exception& ex) {
	std::string what(ex.what());
	if (what.find("StopTest") != std::string::npos) {
        std::string message = "__EXCEPTION__:ABORT_SLIM_TEST:" + what;
        results.push_back(asList(instruction->getId().c_str(), message.c_str(), 0));
        throw TestAborted();
	}
}

} // namespace slim
