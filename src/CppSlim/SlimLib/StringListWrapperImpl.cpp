#include "StringListWrapperImpl.h"
#include "encoding.h"

namespace slim {
        StringListWrapperImpl::~StringListWrapperImpl() {
		    list.clear();
        }

        void StringListWrapperImpl::push_back(const std::string& s) {
            list.push_back(s);
        }

        void StringListWrapperImpl::push_back(StringListWrapper& subList) {
            StringListWrapperImpl& wrappedList = dynamic_cast<StringListWrapperImpl&>(subList);
            list.push_back(wrappedList.getList());
        }

        std::string StringListWrapperImpl::serialize() const {
            return slim::serialize(list);
        }

        StringList StringListWrapperImpl::getList() const {
            return list;
        }

        std::string StringListWrapperImpl::front() const {
            return boost::get<std::string>(list.front());
        }

        std::string StringListWrapperImpl::back() const {
            return boost::get<std::string>(list.back());
        }

        int StringListWrapperImpl::size() const {
            return list.size();
        }

        StringListWrapper* createStringListWrapper() {
            return new StringListWrapperImpl;
        }

};
