#ifndef __NO_METHOD_FOUND_H__
#define __NO_METHOD_FOUND_H__

#include "SlimException.h"

namespace slim {

class NoMethodFound : public SlimException
{
public:
    NoMethodFound(const std::string& method, const std::string& className):
      SlimException("NO_METHOD_IN_CLASS " + className + "::" + method){}
};

} // namespace slim

#endif  //  __NO_METHOD_FOUND_H__
