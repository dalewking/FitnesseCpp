#ifndef __ABSTRACTCLASSREGISTRY__
#define __ABSTRACTCLASSREGISTRY__

#include <string>
#include <map>

#include "SlimLibExports.h"

namespace slim {

class AbstractClass;

    typedef void (*Registration)();

    class EXPORTING_SLIMLIB AbstractClassRegistry {
	    static AbstractClassRegistry defaultInstance;

    public:
	    static AbstractClassRegistry& instance();

	    AbstractClassRegistry();
	    void registerClass(AbstractClass* clazz);

	    void clear();

	    AbstractClass* forName(const std::string& name);
	    AbstractClass* release(const std::string& name);

    private:
	    static std::map<std::string, AbstractClass*> classesByName;
	    typedef std::map<std::string, AbstractClass*>::const_iterator NameClassPair;
    };
};
#endif	//	__ABSTRACTCLASSREGISTRY__
