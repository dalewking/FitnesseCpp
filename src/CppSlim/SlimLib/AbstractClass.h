#ifndef __ABSTRACT_CLASS_H__
#define __ABSTRACT_CLASS_H__


#include <stdexcept>
#include <string>
#include <vector>

#include "SlimLibExports.h"

namespace slim {
class AbstractInstance;
class MethodSignature;

class EXPORTING_SLIMLIB AbstractClass {
public:
	AbstractClass(){}
	AbstractClass(const std::string& className) : name(className){}
	virtual ~AbstractClass() {}
	virtual AbstractInstance* newInstance(const std::vector<std::string>& args) = 0;

	std::string getName() {
		return name;
	}

    virtual bool hasMethod(const MethodSignature& signature) const = 0;

private:
	std::string name;
};

} // namespace slim

#endif	//	__ABSTRACT_CLASS_H__

