#ifndef __NO_INSTANCE_H__
#define __NO_INSTANCE_H__

#include "SlimException.h"

namespace slim {

class NoInstance : public SlimException {
public:
    NoInstance(const std::string& instanceName) :
      SlimException("NO_INSTANCE " + instanceName) {};
};

} // namespace slim

#endif  //  __NO_INSTANCE_H__
