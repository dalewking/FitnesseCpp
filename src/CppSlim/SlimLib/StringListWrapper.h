#ifndef __SLIM_LIST_WRAPPER__
#define __SLIM_LIST_WRAPPER__

#include "SlimLibExports.h"
#include <string>

namespace slim {
    class EXPORTING_SLIMLIB StringListWrapper {
    public:
        virtual ~StringListWrapper() = 0;
        virtual void push_back(const std::string& s) = 0;
        virtual void push_back(StringListWrapper& subList) = 0;
        virtual std::string serialize() const = 0;
        virtual std::string front() const = 0;
        virtual std::string back() const = 0;
        virtual int size() const = 0;
    };

    EXPORTING_SLIMLIB StringListWrapper* createStringListWrapper();
};

#endif  //  __SLIM_LIST_WRAPPER__
