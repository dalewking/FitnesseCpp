#ifndef __NO_CONSTRUCTOR_FOUND_H__
#define __NO_CONSTRUCTOR_FOUND_H__

#include "SlimException.h"

namespace slim {

class NoConstructorFound : public SlimException
{
public:
    NoConstructorFound(const std::string& className):
        SlimException("NO_CONSTRUCTOR " + className){}
};

} // namespace slim

#endif  //  __NO_CONSTRUCTOR_FOUND_H__
