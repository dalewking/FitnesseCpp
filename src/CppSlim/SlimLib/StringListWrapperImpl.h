#ifndef __STRINGLISTWRAPPERIMPL__
#define __STRINGLISTWRAPPERIMPL__

#include "StringListWrapper.h"
#include "SlimList.h"

namespace slim {
    class StringListWrapperImpl : public StringListWrapper {
	    StringList list;
    public:
        virtual ~StringListWrapperImpl();

        void push_back(const std::string& s);

        void push_back(StringListWrapper& subList);

        std::string serialize() const;

        StringList getList() const;

        std::string front() const;

        std::string back() const;

        int size() const;
    };
};

#endif  //  __STRINGLISTWRAPPERIMPL__
