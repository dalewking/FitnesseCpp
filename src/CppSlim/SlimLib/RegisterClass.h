#ifndef __REGISTERCLASS__
#define __REGISTERCLASS__

#include "AbstractClassRegistry.h"
#include "Class.h"
#include "Constructors.h"

#define DECLARE_CLASS_REGISTRATION(classname) \
    static void _register##classname(); \

#define REGISTER_CLASS(classname) \
	void classname::_register##classname() { \
	slim::Class<classname>* clas = new slim::Class<classname>(#classname); \
	slim::AbstractClassRegistry::instance().registerClass(clas);

#define WITH_CONSTRUCTOR(classname) \
	clas->addConstructor(new slim::ZeroArgumentConstructor<classname>());

#define WITH_1_ARG_CONSTRUCTOR(classname, param1type) \
	clas->addConstructor(new slim::OneArgumentConstructor<classname, param1type>());

#define WITH_DIRECT_1_ARG_CONSTRUCTOR(classname, param1type) \
	clas->addConstructor(new slim::DirectOneArgumentConstructor<classname, param1type>());

#define WITH_2_ARG_CONSTRUCTOR(classname, param1type, param2type) \
	clas->addConstructor(new slim::TwoArgumentConstructor<classname, param1type, param2type>());

#define WITH_METHOD(returntype, classname, method) \
	clas->addMethod(#method, new slim::ZeroArgumentMethod<classname, returntype>(&classname::method));

#define WITH_1_ARG_METHOD(returntype, classname, method, param1type) \
	clas->addMethod(#method, new slim::OneArgumentMethod<classname, returntype, param1type>(&classname::method));

#define WITH_2_ARG_METHOD(returntype, classname, method, param1type, param2type) \
	clas->addMethod(#method, new slim::TwoArgumentMethod<classname, returntype, param1type, param2type>(&classname::method));

#define END_REGISTRATION } 

#define REGISTER(classname) classname::_register##classname();

#endif	//	__REGISTERCLASS__
