#include "encoding.h"

#include <cstdarg>
#include <iomanip>
#include <ios>
#include <iostream>
#include <algorithm>

namespace slim {

void encode(std::ostream& o, size_t x);
void encode(std::ostream& o, const std::string& s);

class brackets {
    std::ostream& os;
public:
    brackets(std::ostream& o) : os(o) { os << '['; }
    ~brackets() { os << ']'; }
};

std::string serialize(const StringList& list) {
    encoder e(list);
    return e.asString();
}

std::string serialize(const std::string& instruction) {
    encoder e(instruction);
    return e.asString();
}

encoder::encoder(const StringList& list) {
    brackets around(theString);
    encode(theString, list.size());
    std::for_each(list.begin(), list.end(), boost::apply_visitor(*this));
}

encoder::encoder(const std::string& instruction) {
    encode(theString, instruction);
    theString << ':';
}

std::string encoder::asString() { return theString.str(); }

void encoder::operator()(const std::string& t)
{
    encode(theString, t);
    theString << ':';
}

void encoder::operator()(const StringList& t)
{
    encoder e(t);
    encode(theString, e.asString());
    theString << ':';
}

void encode(std::ostream& o, size_t x) {
    o << std::setw(6) << std::setfill('0') << x;
    o << ':';
}

void encode(std::ostream& o, const std::string& s) {
    o << std::setw(6) << std::setfill('0') << s.size();
    o << ':' << s;
}

std::string prefixWithLength(const std::string& instruction) {
    std::ostringstream o;
    encode(o, instruction);
    return o.str();
}

} // namespace slim
