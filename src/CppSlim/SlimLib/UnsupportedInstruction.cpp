#include "UnsupportedInstruction.h"
#include "Log.h"
#include "Flags.h"

namespace slim {

UnsupportedInstruction::UnsupportedInstruction(const std::string& id) : Instruction(id) {}

void UnsupportedInstruction::execute() {
	recordId();
	if (Flags::VERBOSE())
		log("unsupported command: " + getCommand());
	addToResult("OK");
}

}
