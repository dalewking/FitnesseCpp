#ifndef __INSTRUCTION_HANDLER__
#define __INSTRUCTION_HANDLER__

#include "SlimList.h"
#include "Instruction.h"
#include "InstructionFactory.h"

#include <boost/variant/static_visitor.hpp>
#include <string>
#include <list>
#include <functional>

#include "SlimLibExports.h"

namespace slim {

class EXPORTING_SLIMLIB InstructionHandler : public boost::static_visitor<> {
    std::list<Instruction*> instructions;
    InstructionFactory factory;

public:
    InstructionHandler();
    ~InstructionHandler();
    void operator()(const std::string& s);
    void operator()(const StringList& t);

    int instructionCount() const;
    Instruction* nextInstruction();
    void finish();

private:
    void getLastCompletedInstruction();
};

} // namespace slim

#endif  //  __INSTRUCTION_HANDLER__
