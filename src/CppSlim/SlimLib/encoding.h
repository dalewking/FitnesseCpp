#ifndef __ENCODING__
#define __ENCODING__

#include "SlimList.h"
#include "conversion.h"

#include <sstream>
#include <string>

#include "SlimLibExports.h"

namespace slim {

class encoder : public boost::static_visitor<> {
	std::ostringstream theString;

public:
	encoder(const StringList& list);
	encoder(const std::string& list);
	void operator()(const std::string& t);
	void operator()(const StringList& t);

	std::string asString();
};

EXPORTING_SLIMLIB std::string serialize(const StringList& instruction);
EXPORTING_SLIMLIB std::string serialize(const std::string& instruction);
EXPORTING_SLIMLIB std::string prefixWithLength(const std::string& instruction);

namespace {
	template<>
		std::string Convert<StringList,std::string>(const StringList& in, std::string& out) {
			out = serialize(in);
			return out;
		}
};

} // namespace slim

#endif
