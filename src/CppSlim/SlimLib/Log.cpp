#include "Log.h"
#include "Flags.h"

#include <iterator>

namespace slim {

Log Log::instance;

void log(const std::string& message) {
    if (Flags::VERBOSE())
        Log::get().write(message);
}

Log::Log()
{
}

Log::~Log()
{
}

Log& Log::get() {
    return instance;
}

void Log::dump(std::ostream& s) {
    std::copy(messages.begin(), messages.end(), std::ostream_iterator<std::string>(s));
    s << std::flush;
}

void Log::write(const std::string& message) {
    messages.push_back("[Slim]: " + message + "\n");
}

void Log::clear() {
    messages.clear();
}

} // namespace slim
