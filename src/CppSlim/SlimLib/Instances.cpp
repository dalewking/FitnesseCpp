#include "Instances.h"

namespace slim {

    InstancesImpl impl;
    InstancesImpl& Instances::instances_ = impl;


    InstancesImpl& Instances::instances() {
        return instances_;
    }

    AbstractInstance* Instances::find(const std::string& instanceName) {
		return instances().doFind(instanceName);
	}

	bool Instances::hasInstance(const std::string& instanceName) {
		return instances().doHasInstance(instanceName);
	}

	void Instances::clear() {
		instances().doClear();
	}

	AbstractInstance* Instances::getInstance(const std::string& instanceName) {
		return instances().doGetInstance(instanceName);
	}


    bool isOtherActor(const std::string& instanceName) {
        return instanceName.find("library") == 0;
    }

    void InstancesImpl::doRegisterInstance(const std::string& instanceName, AbstractInstance* instanceOfT) {
        if (isOtherActor(instanceName)) otherActors.push_back(instanceOfT);
        else instancesByAlias[instanceName] = instanceOfT;
	}

	AbstractInstance* InstancesImpl::doFind(const std::string& instanceName) {
		if (instancesByAlias.find(instanceName) != instancesByAlias.end()) return instancesByAlias[instanceName];
        return 0;
	}

	bool InstancesImpl::doHasInstance(const std::string& instanceName) {
		return instancesByAlias.find(instanceName) == instancesByAlias.end();
	}

	AbstractInstance* InstancesImpl::doGetInstance(const std::string& instanceName) {
		return doFind(instanceName);
	}

	void InstancesImpl::doClear() {
		for (NameInstancePair i = instancesByAlias.begin(); i != instancesByAlias.end(); i++)
			delete (*i).second;
		instancesByAlias.clear();
		for (LibraryActors i = otherActors.rbegin(); i != otherActors.rend(); i++)
			delete (*i);
        otherActors.clear();
	}

    AbstractInstance* InstancesImpl::doFindInstanceWithMethodSignature(const MethodSignature& signature) const {
        for (LibraryActors instance = otherActors.rbegin(); instance < otherActors.rend(); ++instance) {
            if ((*instance)->hasSignature(signature)) return *instance;
        }
        return 0;
    }

} // namespace slim
