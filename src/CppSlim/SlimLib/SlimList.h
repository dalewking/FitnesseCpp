#ifndef __SLIM_LIST__
#define __SLIM_LIST__

#include <boost/variant.hpp>
#include <list>

namespace slim {

typedef 
	boost::make_recursive_variant<std::string, std::list< boost::recursive_variant_ > >::type 
	string_tree_t;

// StringList is a list that can contain either strings or other StringLists
typedef std::list< string_tree_t > StringList;

} // namespace slim

#endif	// __SLIM_LIST__
