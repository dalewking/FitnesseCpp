#include "StringUtilities.h"

#include <sstream>
#include <algorithm>

namespace slim {

int parseInt(const std::string& length) {
	std::stringstream ss;
	ss << length;
	int amount;
	ss >> amount;
	return amount;
}

std::string comma(const std::string& s) {
	return s + ", ";
}

std::string asString(const std::vector<std::string>& args) {
	std::ostringstream s;
	s << "(";
	std::transform(args.begin(), args.end() - 1, std::ostream_iterator<std::string>(s), comma);
	s << args[args.size() - 1];
	s << ")";
	return s.str();
}

} // namespace slim
