#ifndef __ABSTRACT_CONSTRUCTOR_H__
#define __ABSTRACT_CONSTRUCTOR_H__

#include "SupportsArguments.h"

namespace slim {

template<class T>
class AbstractConstructor : public SupportsArguments {
public:
	virtual T* invoke(const std::vector<std::string>& args) const = 0;
};

} // namespace slim

#endif	//	__ABSTRACT_CONSTRUCTOR_H__
