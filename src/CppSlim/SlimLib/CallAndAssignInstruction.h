#include "CallInstruction.h"
#include "SlimLibExports.h"

namespace slim {

class EXPORTING_SLIMLIB CallAndAssignInstruction : public CallInstruction {
public:
   	CallAndAssignInstruction(const std::string& id);
	virtual void execute();
private:
	std::string callAndAssign();
	std::string saveResult(const std::string& symbolName, const std::string& value);
};

};
