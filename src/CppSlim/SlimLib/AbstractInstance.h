#ifndef __ABSTRACT_INSTANCE_H__
#define __ABSTRACT_INSTANCE_H__

#include <string>
#include <vector>

#include "SlimLibExports.h"

namespace slim {

class MethodSignature;

class EXPORTING_SLIMLIB AbstractInstance {
public:
    virtual std::string invoke(const MethodSignature& signature) = 0;
    virtual bool hasSignature(const MethodSignature& signature) = 0;
    virtual std::string className() const = 0;

    virtual bool hasSystemUnderTest() const = 0;
    virtual AbstractInstance* getSystemUnderTest() const = 0;

    virtual ~AbstractInstance(){}
};

} // namespace slim

#endif  //  __ABSTRACT_INSTANCE_H__
