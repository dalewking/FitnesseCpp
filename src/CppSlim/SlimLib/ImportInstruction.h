#ifndef __IMPORTINSTRUCTION__
#define __IMPORTINSTRUCTION__

#include "Instruction.h"

namespace slim {

class ImportInstruction : public Instruction
{
public:
    ImportInstruction(const std::string& id);
	virtual void execute();
protected:
    std::string import();
};

};

#endif  //  __IMPORTINSTRUCTION__
