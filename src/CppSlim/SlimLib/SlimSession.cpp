#include "SlimSession.h"
#include "encoding.h"
#include "StringUtilities.h"
#include "ListDeserializer.h"
#include "InstructionRunner.h"
#include "Flags.h"

#include <memory>
#include <boost/thread.hpp>
#include "boost/date_time/posix_time/posix_time_types.hpp"

namespace slim {

std::string SlimSession::SLIM_VERSION("Slim -- V0.0\n");

SlimSession::SlimSession(boost::asio::io_service& io_service) : socket_(io_service), writer(socket_), reader(socket_)
{
}

SlimSession::~SlimSession(){
	boost::asio::io_service & ios = socket_.get_io_service();
	ios.stop();
}

tcp::socket& SlimSession::socket()
{
	return socket_;
}

void SlimSession::start()
{
	writer.write(SLIM_VERSION);
    boost::this_thread::sleep(boost::posix_time::milliseconds(Flags::WAIT() * 1000));

	bool done = false;
	while (!done) {
		int length = parseInt(reader.read(6));
		reader.read(1);
		std::string instructions = reader.read(length);
		done = processInstructions(instructions);
	}
}

bool SlimSession::processInstructions(const std::string& serializedInstructions) {
	InstructionRunner runner(this);
	return runner.process(serializedInstructions);
}

void SlimSession::sendResults(const StringList& results) {
	writer.write(prefixWithLength(serialize(results)));
}

} // namespace slim
