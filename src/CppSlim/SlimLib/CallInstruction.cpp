#include "CallInstruction.h"
#include "MethodSignature.h"
#include "Instances.h"
#include "Log.h"
#include "Flags.h"
#include "NoInstance.h"
#include "NoMethodFound.h"

namespace slim {

CallInstruction::CallInstruction(const std::string& id) : Instruction(id) {}

void CallInstruction::execute() {
	recordId();
	addToResult(call());
}

std::string CallInstruction::call() {
	validateNumberOfArguments(2);
	return call(0);
}

std::string invokeMethodOnInstance(AbstractInstance* instance, std::string& instanceName, const MethodSignature& signature) {
    std::string result = instance->invoke(signature);
    if (Flags::VERBOSE())
	    log("called " + instance->className() + "::" + signature.asString() + " on " + instanceName);
    return result;
}

std::string invokeMethodOnInstance(std::string& instanceName, const MethodSignature& signature) {
	AbstractInstance* scriptActor = Instances::find(instanceName);

    if (scriptActor != 0 && scriptActor->hasSignature(signature)) {
        return invokeMethodOnInstance(scriptActor, instanceName, signature);
    }
    /* else if (???) {???} system under test stuff goes here */

    AbstractInstance* instance = Instances::findInstanceWithMethodSignature(signature);
    if (instance != 0) {
        return invokeMethodOnInstance(instance, std::string("some other actor"), signature);
    }

    if (scriptActor == 0) throw NoInstance(instanceName);

    throw NoMethodFound(signature.getName(), scriptActor->className());
}

std::string CallInstruction::call(int callInstructionOffset) {
    return invokeMethodOnInstance(arguments[callInstructionOffset], 
        MethodSignature(arguments[callInstructionOffset + 1], lookUpArgs(callInstructionOffset + 2)));
}

}
