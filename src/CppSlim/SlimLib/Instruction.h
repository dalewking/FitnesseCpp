#ifndef __INSTRUCTION_H__
#define __INSTRUCTION_H__

#include <string>
#include <deque>
#include <vector>

#include "SlimLibExports.h"

namespace slim {

class SymbolTable;
class StringListWrapper;

class EXPORTING_SLIMLIB Instruction
{
	std::string id;
	std::string command;
	mutable StringListWrapper* result;

public:
	Instruction(const std::string& id);
	Instruction(const Instruction& rhs);

	void operator =(const Instruction& rhs);
	virtual ~Instruction(void);

	std::string getId(void);
	std::string getCommand(void);
	std::vector<std::string> getArguments();

	void setCommand(const std::string& command);
	void addArgument(const std::string& arg);

	virtual void execute();
	StringListWrapper* getResult() const;


	void setSymbolTable(SymbolTable* symbolsTable);

protected:
	SymbolTable* symbols;
	std::deque<std::string> arguments;

    void addToResult(const std::string& partialResult); 
    void recordId();
	void validateNumberOfArguments(unsigned int expectedCount);
	std::vector<std::string> lookUpArgs(int argumentOffset);
	std::string lookupSymbol(const std::string& key);
};

} // namespace slim

#endif	//	__INSTRUCTION_H__
