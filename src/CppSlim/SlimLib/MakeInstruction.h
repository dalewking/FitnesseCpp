#include "Instruction.h"
#include "SlimLibExports.h"

namespace slim {

class EXPORTING_SLIMLIB MakeInstruction : public Instruction {
public:
    MakeInstruction(const std::string& id);
	virtual void execute();
private:
   	std::string make();
};

}
