#include "SocketWriter.h"


namespace slim {

Writer::Writer(	tcp::socket& socket) : socket_(socket) {}

void Writer::write(const std::string& message) {
	boost::asio::write(socket_, boost::asio::buffer(message),boost::asio::transfer_all());
}

} // namespace slim
