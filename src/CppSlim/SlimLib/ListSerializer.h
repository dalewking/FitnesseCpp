#ifndef __LIST_SERIALIZER__
#define __LIST_SERIALIZER__

#include <list>
#include <string>

#include "SlimLibExports.h"

namespace slim {

class EXPORTING_SLIMLIB ListSerializer {
public:
    static std::string serialize(const std::list<std::list<std::string> >& list);
    static std::string serialize(const std::list<std::string>& list);
};

} // namespace slim


#endif
