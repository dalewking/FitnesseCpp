#ifndef __SOCKET_READER__
#define __SOCKET_READER__

#include <boost/asio.hpp>
#include "SlimLibExports.h"

namespace slim {

using boost::asio::ip::tcp;

class EXPORTING_SLIMLIB Reader {
    tcp::socket& socket_;
public:
    Reader( tcp::socket& socket);
    std::string read(int amount);
};

} // namespace slim

#endif  //  __SOCKET_READER__
