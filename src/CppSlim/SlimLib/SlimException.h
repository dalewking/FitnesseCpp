#ifndef __SLIM_EXCEPTION__
#define __SLIM_EXCEPTION__

#include <stdexcept>
#include <string>

namespace slim {

class SlimException : public std::logic_error {
public:
	explicit SlimException(const std::string& message): std::logic_error("__EXCEPTION__:message:<<" + message + " >>") {};
};

} // namespace slim

#endif	//	__SLIM_EXCEPTION__
