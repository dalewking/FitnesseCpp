#ifndef __INSTANCE_H__
#define __INSTANCE_H__

#include "AbstractInstance.h"

namespace slim {

template <class T> class Class;

template <class T>
class Instance : public AbstractInstance {
	Class<T>& classOfT;
	T* t;
    AbstractInstance* systemUnderTest;

public:
	Instance(Class<T>& clazz, T* t): classOfT(clazz), t(t), systemUnderTest(0) {}

	~Instance() {
		delete t;
	}

	virtual std::string invoke(const MethodSignature& signature) {
		return classOfT.invokeMethod(t, signature);
	}

	virtual bool hasSignature(const MethodSignature& signature) {
		return classOfT.hasMethod(signature);
	}

	virtual std::string className() const {
		return classOfT.getName();
	}

    virtual bool hasSystemUnderTest() const {
        return systemUnderTest != 0;
    }

    virtual AbstractInstance* getSystemUnderTest() const {
        return systemUnderTest;
    }

};

} // namespace slim

#endif	//	__INSTANCE_H__
