#ifndef __LIST_UTILITIES__
#define __LIST_UTILITIES__

#include "SlimList.h"
#include "SlimLibExports.h"

namespace slim {

EXPORTING_SLIMLIB StringList asList(const char* id ...);

} // namespace slim

#endif  // __LIST_UTILITIES__
