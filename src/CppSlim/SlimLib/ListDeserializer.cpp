#include "ListDeserializer.h"

#include "StringUtilities.h"

#include <list>

#include <iostream>
#include <sstream>
#include <algorithm>

namespace slim {

StringList ListDeserializer::deserialize(const std::string& s) {
	return deserialize(s, 1);
}

StringList ListDeserializer::deserialize(const std::string& s, int maxDepth) {
	ListDeserializer deserializer(s);
	deserializer.setMaxDepth(maxDepth);
	return deserializer.deserialize();
}

ListDeserializer::ListDeserializer(const std::string& s) : serialized(s), index(0), maxDepth(0) {}

void ListDeserializer::setMaxDepth(int depth) {
	maxDepth = depth;
}

StringList ListDeserializer::deserialize() {
	checkSerializedStringIsValid();
	return deserializeString();
}

StringList ListDeserializer::deserializeString() {
	checkForOpenBracket();
	StringList result = deserializeList();
	checkForClosedBracket();
	return result;
}

StringList ListDeserializer::deserializeList() {
	int itemCount = getLength();
	for (int i = 0; i < itemCount; i++)
	  deserializeItem();

	return result;
}

void ListDeserializer::deserializeItem() {
	int itemLength = getLength();
	std::string item = getString(itemLength);
	try {
		if (maxDepth > 0) {
			ListDeserializer deserializer(item);
			deserializer.setMaxDepth(maxDepth - 1);
			result.push_back(deserializer.deserialize());
		} else {
			result.push_back(item);
		}
	} catch (SyntaxError&) {
		result.push_back(item);
	}
}

std::string ListDeserializer::getString(int length) {
	std::string result = serialized.substr(index, length);
	index += length;
	checkForColon();
	return result;
}

int ListDeserializer::getLength() {
	int lengthSize = 6;
	std::string lengthString = serialized.substr(index, lengthSize);
	int length = parseInt(lengthString);
	index += lengthSize;
	checkForColon();
	return length;
}

void ListDeserializer::checkForColon() {
    if (getChar() != ':') throw SyntaxError("':' expected after \"" + getStringSoFar() + "\"");
}

void ListDeserializer::checkForOpenBracket() {
	if (getChar() != '[') throw SyntaxError("'[' expected after \"" + getStringSoFar() + "\"");
}

void ListDeserializer::checkForClosedBracket() {
	if (!charsLeft() || getChar() != ']')
		throw SyntaxError("']' expected after \"" + getStringSoFar() + "\"");
}

bool ListDeserializer::charsLeft() { return index < serialized.size(); }
char ListDeserializer::getChar() { return serialized[index++]; }
std::string ListDeserializer::getStringSoFar() { return serialized.substr(0, index - 1); }

void ListDeserializer::checkSerializedStringIsValid() {
	if (serialized.empty()) throw SyntaxError("empty strings not supported");
}

} // namespace slim
