#include "MockLibrary.h"
#include <iostream>

namespace slim {
    MockLibrary::MockLibrary(const std::string& path) : name(path) {
    }

    std::string MockLibrary::getName() const {
        return name;
    }

    void MockLibrary::call(const std::string& functionName) const {
    }

    std::ostream& operator<<(std::ostream& ostr, const MockLibrary& lib) {
        ostr << "library " << lib.getName();
        return ostr;
    }


}
