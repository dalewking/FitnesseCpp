#ifndef __CONVERSION_H__
#define __CONVERSION_H__

#include <string>
#include <sstream>

namespace slim {

	template <class T1, class T2>
	class Converter {
	public:
		T2 operator()(const T1& in, T2& out) {
			std::stringstream s;
			s << std::boolalpha << in;
			s >> out;
			s.exceptions(std::ios_base::badbit|std::ios_base::failbit);
			return out;
		}
	};

	template <class T1>
	class Converter<T1, std::string> {
	public:
		std::string operator()(const T1& in, std::string& out) {
			std::stringstream s;
			s << std::boolalpha << in;
			return s.str();
		}
	};

namespace {
template<class T1, class T2>
T2 Convert(const T1& in, T2& out)
{
	Converter<T1, T2> convert;
	return convert(in, out);
}

//  template <> void Convert<string>(const string& in, string& out)
template<>
std::string Convert<std::string,std::string>(const std::string& in, std::string& out)
{
	out = in;
	return in;
}

template <typename T>
T argumentOfType(const std::string& arg) {
	T t;
	Convert<std::string, T>(arg, t);
	return t;
}

template <typename R>
std::string convertReturnType(R r) {
	std::string result;
	return Convert<R, std::string>(r, result);
}

};

} // namespace slim

#endif	//	__CONVERSION_H__
