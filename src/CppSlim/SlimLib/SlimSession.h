#ifndef __SLIM_SESSION_H__
#define __SLIM_SESSION_H__

#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include "SocketReader.h"
#include "SocketWriter.h"
#include "ResultSender.h"

namespace slim {

using boost::asio::ip::tcp;


class SlimSession : public ResultSender
{
public:
	SlimSession(boost::asio::io_service& io_service);
public:
	~SlimSession();

	tcp::socket& socket();
	void start();
	bool processInstructions(const std::string& instructions);
	void sendResults(const StringList& results);
private:
	tcp::socket socket_;
	
	enum { lengthLength = 6 };
	char nextLength[lengthLength];

	static std::string SLIM_VERSION;
	Writer writer;
	Reader reader;
};

} // namespace slim

#endif	//	__SLIM_SESSION_H__
