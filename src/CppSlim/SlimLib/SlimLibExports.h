#if SLIM_DLL
#ifdef EXPORT_SLIMLIB
#define EXPORTING_SLIMLIB __declspec(dllexport)
#define EXPORT_SLIM_LIB_TEST_GROUP(testGroup)\
  __declspec(dllexport) int externTestGroup##testGroup = 0;
#else
#define EXPORTING_SLIMLIB __declspec(dllimport)
#define IMPORT_SLIM_LIB_TEST_GROUP(testGroup)\
  __declspec(dllimport) extern int externTestGroup##testGroup;\
  int* p##testGroup = &externTestGroup##testGroup;
#endif
#else
#define EXPORTING_SLIMLIB
#endif
