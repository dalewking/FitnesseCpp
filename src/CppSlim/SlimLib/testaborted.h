/* 
 * File:   testaborted.h
 * Author: SG0173087
 *
 * Created on May 15, 2009, 10:54 AM
 */

#ifndef _TESTABORTED_H
#define	_TESTABORTED_H

#include <stdexcept>

namespace slim {

class TestAborted : public std::runtime_error {
public:
    explicit TestAborted();
};

} // namespace slim

#endif	/* _TESTABORTED_H */

