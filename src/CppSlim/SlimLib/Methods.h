#ifndef __METHODS_H__
#define __METHODS_H__

#include "ValidatingMethod.h"
#include "UnsupportedArgumentsPolicy.h"
#include "conversion.h"
#include <functional>

namespace slim {

template <class T, class R>
class ZeroArgumentMethod : public ValidatingMethod<T, UnsupportedArgumentsPolicy<0> > {
public:
    typedef R (T::*MemberFunction)(void);

    MemberFunction callee;
    ZeroArgumentMethod(MemberFunction m) : callee(m) {}
    std::string invoke(T& t, const std::vector<std::string>& args) const {
        ValidatingMethod<T, UnsupportedArgumentsPolicy<0> >::argumentPolicy.validate(args);
        return convertReturnType((t.*callee)());
    }
};

template <class T, class R>
class ZeroArgumentConstMethod : public ValidatingMethod<T, UnsupportedArgumentsPolicy<0> > {
public:
    typedef R (T::*MemberFunction)(void) const;

    MemberFunction callee;
    ZeroArgumentConstMethod(MemberFunction m) : callee(m) {}
    std::string invoke(T& t, const std::vector<std::string>& args) const {
        ValidatingMethod<T, UnsupportedArgumentsPolicy<0> >::argumentPolicy.validate(args);
        return convertReturnType((t.*callee)());
    }
};

template <class T>
class ZeroArgumentMethod<T, void> : public ValidatingMethod<T, UnsupportedArgumentsPolicy<0> > {
public:
    typedef void (T::*MemberFunction)(void);

    MemberFunction callee;
    ZeroArgumentMethod(MemberFunction m) : callee(m) {}
    std::string invoke(T& t, const std::vector<std::string>& args) const {
        ValidatingMethod<T, UnsupportedArgumentsPolicy<0> >::argumentPolicy.validate(args);
        (t.*callee)();
        return "/__VOID__/";
    }
};

template <class T, class R, typename A1>
class OneArgumentMethod : public ValidatingMethod<T, UnsupportedArgumentsPolicy<1> > {
public:
    typedef R (T::*MemberFunction)(A1);

    MemberFunction callee;
    OneArgumentMethod(MemberFunction m) : callee(m) {}
    std::string invoke(T& t, const std::vector<std::string>& args) const {
        ValidatingMethod<T, UnsupportedArgumentsPolicy<1> >::argumentPolicy.validate(args);
        return convertReturnType((t.*callee)(argumentOfType<A1>(args[0])));
    }
};

template <class T, typename A1>
class OneArgumentMethod<T, void, A1> : public ValidatingMethod<T, UnsupportedArgumentsPolicy<1> > {
public:
    typedef void (T::*MemberFunction)(A1);

    MemberFunction callee;
    OneArgumentMethod(MemberFunction m) : callee(m) {}
    std::string invoke(T& t, const std::vector<std::string>& args) const {
        ValidatingMethod<T, UnsupportedArgumentsPolicy<1> >::argumentPolicy.validate(args);
        (t.*callee)(argumentOfType<A1>(args[0]));
        return "/__VOID__/";
    }
};

template <class T, class R, typename A1, typename A2>
class TwoArgumentMethod : public ValidatingMethod<T, UnsupportedArgumentsPolicy<2> > {
public:
    typedef R (T::*MemberFunction)(A1, A2);

    MemberFunction callee;
    TwoArgumentMethod(MemberFunction m) : callee(m) {}
    std::string invoke(T& t, const std::vector<std::string>& args) const {
        ValidatingMethod<T, UnsupportedArgumentsPolicy<2> >::argumentPolicy.validate(args);
        return convertReturnType((t.*callee)(argumentOfType<A1>(args[0]), argumentOfType<A2>(args[1])));
    }
};

template <class T, typename A1, typename A2>
class TwoArgumentMethod<T, void, A1, A2> : public ValidatingMethod<T, UnsupportedArgumentsPolicy<2> > {
public:
    typedef void (T::*MemberFunction)(A1, A2);

    MemberFunction callee;
    TwoArgumentMethod(MemberFunction m) : callee(m) {}
    std::string invoke(T& t, const std::vector<std::string>& args) const {
        ValidatingMethod<T, UnsupportedArgumentsPolicy<2> >::argumentPolicy.validate(args);
        (t.*callee)(argumentOfType<A1>(args[0]), argumentOfType<A2>(args[1]));
        return "/__VOID__/";
    }
};

} // namespace slim

#endif  //  __METHODS_H__
