#include "ListSerializer.h"

#include <sstream>
#include <iomanip>
#include <ios>
#include <iostream>
#include <algorithm>

namespace slim {

class Brackets {
	std::ostream& os;
public:
	Brackets(std::ostream& o) : os(o) { os << '['; }
	~Brackets() { os << ']'; }
};

void formatValue(std::ostream& os, size_t value) {
	os << std::setw(6) << std::setfill('0') << value << ':';
}

void writeString(std::ostream& os, const std::string& s) {
	formatValue(os, s.size());
	os << s << ':';
}

class StringFormatter {
	std::ostream& os;
public:
	StringFormatter(std::ostream& o) : os(o) {}
	void operator()(const std::string& s) { writeString(os, s); }
};

void formatList(std::ostream& os, const std::list<std::string>& list) {
	Brackets around(os);
	formatValue(os, list.size());
	StringFormatter formatter(os);
	std::for_each(list.begin(), list.end(), formatter);
}

class ListFormatter {
	std::ostream& os;
public:
	ListFormatter(std::ostream& o) : os(o) {}
	void operator()(const std::list<std::string>& list) { 
		std::ostringstream listOS;
		formatList(listOS, list);
		writeString(os, listOS.str());;
	}
};

void formatLists(std::ostream& os, std::list<std::list<std::string> > list) {
	Brackets around(os);
	formatValue(os, list.size());
	ListFormatter formatter(os);
	std::for_each(list.begin(), list.end(), formatter);
}


std::string ListSerializer::serialize(const std::list<std::list<std::string> >& list) {
		std::ostringstream os;
		formatLists(os, list);
		return os.str();
	}

std::string ListSerializer::serialize(const std::list<std::string>& list) {
		std::ostringstream os;
		formatList(os, list);
		return os.str();
	}

} // namespace slim
