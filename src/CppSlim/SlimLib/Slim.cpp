#include "Slim.h"
#include "SlimServerLauncher.h"
#include "Flags.h"

#include <iostream>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/positional_options.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/thread.hpp>
#include "boost/date_time/posix_time/posix_time_types.hpp"

namespace slim {

namespace po = boost::program_options;

void waitForRemoteDebuggerToAttach(po::variables_map& vm) {
	bool wait = vm.count("wait") != 0;
	if (wait) {
        int waitTime = vm["wait"].as<int>();
        Flags::set(Flags::WAIT, waitTime);
	}
}

Slim::Slim(void)
{
}

Slim::~Slim(void)
{
}

struct nameableIntArgument : public boost::program_options::typed_value<int>
{
    std::string my_name;
    int value;
    nameableIntArgument(std::string const& name) : boost::program_options::typed_value<int>(&value),
		my_name(name),
		value(0) {}

    std::string name() const { return my_name; }
};

int Slim::run(int argc, char* argv[]) {

	nameableIntArgument* port = new nameableIntArgument("port-number");
	nameableIntArgument* seconds = new nameableIntArgument("seconds");
    po::options_description desc("Allowed options");
    desc.add_options()("help", "produce help message");
    desc.add_options()("port-number", port, "port number");
    desc.add_options()("verbose,v", "print out things that happened");
    desc.add_options()("wait,w", seconds, "wait for the debugger to attach");

    po::positional_options_description p;
    p.add("port-number", -1);

    po::variables_map vm;
    po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
    po::notify(vm);

	if (vm.count("help")) {
		std::cout << desc << std::endl;
		return 1;
	}

	waitForRemoteDebuggerToAttach(vm);

    bool verbose = vm.count("verbose") != 0;
    Flags::set(Flags::VERBOSE, verbose);

	SlimServerLauncher launcher(vm["port-number"].as<int>());
	return launcher.runServer();
}

} // namespace slim
