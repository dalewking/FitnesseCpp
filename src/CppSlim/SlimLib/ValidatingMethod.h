#ifndef __VALIDATING_METHOD_H__
#define __VALIDATING_METHOD_H__

#include "AbstractMethod.h"

namespace slim {

template<class T, class ArgumentPolicy>
class ValidatingMethod: public AbstractMethod<T> {
protected:
	ArgumentPolicy argumentPolicy;
public:
	virtual bool supports(const std::vector<std::string>& args) const { return argumentPolicy.supports(args); }
};

} // namespace slim

#endif	//	__VALIDATING_METHOD_H__
