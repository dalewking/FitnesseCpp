#ifndef __STRING_UTILITIES__
#define __STRING_UTILITIES__

#include <string>
#include <vector>

#include "SlimLibExports.h"

namespace slim {

EXPORTING_SLIMLIB int parseInt(const std::string& length);
std::string asString(const std::vector<std::string>& args);

} // namespace slim

#endif  //  __STRING_UTILITIES__
