#ifndef __MOCK_LIBRARY__
#define __MOCK_LIBRARY__

#include <string>

namespace slim {

class MockLibrary {
    std::string name;

public:
    MockLibrary(const std::string& path);

    std::string getName() const;

    void call(const std::string& functionName) const;

};

std::ostream& operator<<(std::ostream& ostr, const MockLibrary& lib);

};

#endif  //  __MOCK_LIBRARY__
