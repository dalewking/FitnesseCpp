#ifndef __FLAGS_H__
#define __FLAGS_H__

#include "Flag.h"
#include "SlimLibExports.h"

namespace slim {

class EXPORTING_SLIMLIB Flags {
public:
	static Flag<bool> VERBOSE;
	static Flag<int> WAIT;
	
	template <typename T>
	static void set(Flag<T>& flag, T value) {
		flag.set(value);
	}
};

} // namespace slim

#endif	//	__FLAGS_H__
