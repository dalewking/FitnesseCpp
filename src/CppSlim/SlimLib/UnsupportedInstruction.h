#include "Instruction.h"

namespace slim {

class UnsupportedInstruction : public Instruction {
public:
    UnsupportedInstruction(const std::string& id);
	virtual void execute();
};

}
