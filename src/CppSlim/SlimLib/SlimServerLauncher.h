#ifndef __SlimServerLauncher__
#define __SlimServerLauncher__

#include "SlimLibExports.h"

namespace slim {

class EXPORTING_SLIMLIB SlimServerLauncher
{
    int portNumber;


public:
    SlimServerLauncher(int port);
public:
    ~SlimServerLauncher(void);
    int runServer();
    int port();
};

} // namespace slim

#endif  //  __SlimServerLauncher__
