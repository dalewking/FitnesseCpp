#ifndef __FLAG_H__
#define __FLAG_H__

namespace slim {

template <typename T>
class Flag {
public:
	explicit Flag(T defaultValue):value(defaultValue){}
	void set(T t) { value = t; }
	T operator()() { return value; }
private:
	T value;
};

} // namespace slim

#endif	//	__FLAG_H__
