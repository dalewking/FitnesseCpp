#ifndef __INSTANCES_H__
#define __INSTANCES_H__

#include "Instance.h"
#include <map>
#include <deque>

namespace slim {

#include "SlimLibExports.h"

class EXPORTING_SLIMLIB InstancesImpl {
	std::map<std::string, AbstractInstance*> instancesByAlias;
	std::deque<AbstractInstance*> otherActors;
	typedef std::map<std::string, AbstractInstance*>::const_iterator NameInstancePair;
	typedef std::deque<AbstractInstance*>::const_reverse_iterator LibraryActors;

public:

	AbstractInstance*   doFind(const std::string& instanceName);
    void                doRegisterInstance(const std::string& instanceName, AbstractInstance* instanceOfT);
    void                doClear();
	bool                doHasInstance(const std::string& instanceName);
	AbstractInstance*   doGetInstance(const std::string& instanceName);
    AbstractInstance*   doFindInstanceWithMethodSignature(const MethodSignature& signature) const;
};

class EXPORTING_SLIMLIB Instances {
    static InstancesImpl& instances_;
    static InstancesImpl& instances();

public:
	static AbstractInstance* find(const std::string& instanceName);
	static bool hasInstance(const std::string& instanceName);

	static AbstractInstance* getInstance(const std::string& instanceName);

	template <typename T>
	static void registerInstance(const std::string& instanceName, Instance<T>* instanceOfT) {
		instances().doRegisterInstance(instanceName, instanceOfT);
	}

	static void registerInstance(const std::string& instanceName, AbstractInstance* instance) {
		instances().doRegisterInstance(instanceName, instance);
	}

    static AbstractInstance* findInstanceWithMethodSignature(const MethodSignature& signature) {
        return instances().doFindInstanceWithMethodSignature(signature);
    }

	template <typename T>
	static void registerInstance(const std::string& instanceName, Class<T>& classOfT, T* t) {
		registerInstance(instanceName, createInstance<T>(classOfT, instanceName, t));
	}

	static void clear();
};


} // namespace slim

#endif	//	__INSTANCES_H__
