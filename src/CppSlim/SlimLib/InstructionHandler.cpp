#include "InstructionHandler.h"

#include <iostream>
#include <algorithm>

namespace slim {

InstructionHandler::InstructionHandler() {
	factory.init();
}

template <typename T>
void deleteIt(T* it) {
    delete it;
}

InstructionHandler::~InstructionHandler() {
    std::for_each(instructions.begin(), instructions.end(), deleteIt<Instruction>);
}

void InstructionHandler::operator()(const std::string& s) {
	factory(s);
}

void InstructionHandler::operator()(const StringList& t) {
	getLastCompletedInstruction();
	factory.reset();
	std::for_each(t.begin(), t.end(), boost::apply_visitor(*this));
}

void InstructionHandler::finish() {
	getLastCompletedInstruction();
}

void InstructionHandler::getLastCompletedInstruction() {
	if (factory.hasInstruction()) instructions.push_back(factory.getInstruction());
}

int InstructionHandler::instructionCount() const {
	return (int) instructions.size();
}

Instruction* InstructionHandler::nextInstruction() {
	Instruction* instruction = 0;
	if (instructions.empty()) return instruction;
	
	instruction = instructions.front();
	instructions.pop_front();
	return instruction;
}

} // namespace slim
