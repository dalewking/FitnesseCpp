#ifndef __MALFORMED_INSTRUCTION__
#define __MALFORMED_INSTRUCTION__

#include "SlimException.h"

namespace slim {

class MalformedInstruction : public SlimException {
public:
    MalformedInstruction(const std::string& instruction):
      SlimException("MALFORMED_INSTRUCTION " + instruction) {};
};

} // namespace slim

#endif  //  __MALFORMED_INSTRUCTION__
