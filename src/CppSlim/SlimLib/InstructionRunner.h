#ifndef __INSTRUCTION_RUNNER__
#define __INSTRUCTION_RUNNER__

#include "SlimList.h"
#include "SymbolTable.h"

#include <string>

#include "SlimLibExports.h"

namespace slim {

class ResultSender;
class Instruction;

class EXPORTING_SLIMLIB InstructionRunner : public boost::static_visitor<>{
    ResultSender* sender;
    StringList results;
    SymbolTable symbolTable;

    static const std::string BYE;
public:
    InstructionRunner(ResultSender* sendTo);
    bool process(const std::string& serializedInstructions);
    void operator()(const std::string& s);
    void operator()(const StringList& instructions);
protected:
	void handleStopTest(Instruction* instruction, std::exception& ex);
};

} // namespace slim

#endif  //  __INSTRUCTION_RUNNER__
