#include "MethodSignature.h"
#include "StringUtilities.h"

namespace slim {
    MethodSignature::MethodSignature(const std::string& name, const std::vector<std::string>& arguments) : methodName(name), args(arguments) {
    }

    std::string MethodSignature::getName() const {
        return methodName;
    }

    std::vector<std::string> MethodSignature::getArguments() const {
        return args;
    }

    std::string MethodSignature::asString() const {
        return methodName + (args.empty() ? "()" : slim::asString(args));
    }
};
