#ifndef __INSTRUCTION_FACTORY_H__
#define __INSTRUCTION_FACTORY_H__

#include <string>
#include <functional>

#include "SlimLibExports.h"

namespace slim {

class Instruction;

class FactoryState;
class AddArgumentState;
class SetCommandState;
class ConstructState;

class EXPORTING_SLIMLIB InstructionFactory
{
	FactoryState* add;
	FactoryState* cmd;
	FactoryState* ctor;
	FactoryState* factoryState;
	FactoryState* firstState;
	Instruction* current;

    std::string nextId;
public:
	InstructionFactory(void);
	~InstructionFactory();

	void init();

	void reset();
	void setId(const std::string& id);
	void setCommand(const std::string& command);
	void addArgument(const std::string& argument);
	void operator()(const std::string&);

	Instruction* getInstruction();
	bool hasInstruction() const;
};

} // namespace slim

#endif	//	__INSTRUCTION_FACTORY_H__
