#ifndef __RESULT_SENDER__
#define __RESULT_SENDER__

#include "SlimList.h"

#include "SlimLibExports.h"

namespace slim {

class EXPORTING_SLIMLIB ResultSender {
public:
    virtual ~ResultSender();
    virtual void sendResults(const StringList& results) = 0;
};

} // namespace slim

#endif  //  __RESULT_SENDER__
