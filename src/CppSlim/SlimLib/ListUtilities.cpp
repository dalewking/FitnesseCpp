#include "ListUtilities.h"

#ifndef __SUNPRO_CC
#include <cstdarg>
#else
#include <sys/varargs.h>
#endif

namespace slim {

StringList asList(const char* id ...) {
	StringList instruction;
	instruction.push_back(std::string(id));
	va_list args;
	va_start(args, id);
	for(;;) {
		char* p = va_arg(args, char*);
		if (p == 0) break;
		instruction.push_back(std::string(p));
	}
	va_end(args);

	return instruction;
}

} // namespace slim
