#ifndef __NO_CLASS_EXCEPTION__
#define __NO_CLASS_EXCEPTION__

#include "SlimException.h"

#include <string>

namespace slim {

class NoClass : public SlimException {
public:
	NoClass(const std::string& className): 
	  SlimException(std::string(
			  std::string("COULD_NOT_INVOKE_CONSTRUCTOR ") + 
			  className + 
			  std::string(" No class ") + 
			  className + 
			  std::string(" has been registered"))) {};
};

} // namespace slim

#endif	//	__NO_CLASS_EXCEPTION__
