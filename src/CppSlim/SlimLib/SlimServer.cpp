#include "SlimServer.h"
#include "SlimSession.h"

#include <iostream>

namespace slim {

SlimServer::SlimServer(boost::asio::io_service& io_service, short port): io_service_(io_service), acceptor_(io_service, tcp::endpoint(tcp::v4(), port))
{
    try {
        SlimSession* new_session = new SlimSession(io_service_);
        waitForConnection(new_session);
    } catch (boost::system::system_error& err) {
        std::cout << "error " << err.code() << ", " << err.what() << std::endl;
    }
}

SlimServer::~SlimServer(void)
{
}

void SlimServer::startSession(SlimSession* new_session, const boost::system::error_code& error)
{
    boost::scoped_ptr<SlimSession> session(new_session);
    if (error) return;

    try {
        new_session->start();
    } catch (boost::system::system_error& err) {
        std::cout << "error " << err.code() << ", " << err.what() << std::endl;
    }
}

void SlimServer::waitForConnection(SlimSession* new_session) {
    acceptor_.async_accept(new_session->socket(),
                        boost::bind(&SlimServer::startSession,
                                    this,
                                    new_session,
                                    boost::asio::placeholders::error));
}

} // namespace slim
