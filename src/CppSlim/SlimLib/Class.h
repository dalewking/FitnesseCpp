#ifndef __CLASS_H__
#define __CLASS_H__

#include "AbstractClass.h"
#include "Constructors.h"
#include "Methods.h"
#include "Instance.h"
#include "NoConstructorFound.h"
#include "NoMethodFound.h"
#include "MethodSignature.h"

#include <vector>
#include <map>
#include <string>

namespace slim {

template <typename T>
AbstractInstance* createInstance(Class<T>& classOfT, T* t) {
	return new Instance<T>(classOfT, t);
}


template <class T>
class Class : public AbstractClass {
public:

	explicit Class(const std::string& className) : AbstractClass(className) {}
	explicit Class() : AbstractClass("undefined") {}

	~Class() {
		for (ConstructorPtr ctor = constructors.begin(); ctor != constructors.end(); ctor++) {
			delete *ctor;
		}
		for (NameMethodPair i = methods.begin(); i != methods.end(); i++) {
			delete (*i).second;
		}
	}

	void addConstructor(AbstractConstructor<T>* constructor) {
		constructors.push_back(constructor);
	}

	void addMethod(const std::string& name, AbstractMethod<T>* method) {
		methods.insert(std::make_pair(name, method));
	}

	AbstractInstance* newInstance(const std::vector<std::string>& args) {
		for (ConstructorPtr ctor = constructors.begin(); ctor != constructors.end(); ctor++) {
			if ((*ctor)->supports(args)) {
				T* t = (*ctor)->invoke(args);
				return createInstance<T>(*this, t);
			}
		}
		throw NoConstructorFound(getName());
	}

    template <typename A>
    AbstractInstance* newDirectInstance(A argument) {
        slim::DirectOneArgumentConstructor<T, A>* ctor = dynamic_cast<slim::DirectOneArgumentConstructor<T, A>* >(getConstructorTaking<A>());
        if (ctor == 0)
            throw NoConstructorFound(getName());
        ctor->argumentValue("A", argument);
        std::vector<std::string> args;
        args.push_back("A");
        T* t = ctor->invoke(args);
        return createInstance(*this, t);
    }

	std::string invokeMethod(T* t, const MethodSignature& signature) {
		AbstractMethod<T>* method = findMethod(signature);
		if (method != 0) return method->invoke(*t, signature.getArguments());
		throw NoMethodFound(signature.getName(), getName());
	}

    bool hasMethod(const MethodSignature& signature) const {
		return findMethod(signature) != 0;
    }

    template <class C>
    C* getConstructorOfType() {
		for (ConstructorPtr ctorPtr = constructors.begin(); ctorPtr != constructors.end(); ctorPtr++) {
            AbstractConstructor<T>* ctor = (*ctorPtr);
			C* desiredType = dynamic_cast<C* >(ctor);
			if (desiredType != 0)
				return desiredType;
		}
		return 0;
    }

	ZeroArgumentConstructor<T>* getConstructorTaking() {
        return getConstructorOfType<ZeroArgumentConstructor<T> >();
	}

	template <typename A>
	AbstractOneArgumentConstructor<T, A>* getConstructorTaking() {
		return getConstructorOfType<AbstractOneArgumentConstructor<T, A> >();
	}

	template <typename A1, typename A2>
	TwoArgumentConstructor<T, A1, A2>* getConstructorTaking() {
		return getConstructorOfType<TwoArgumentConstructor<T, A1, A2> >();
	}

    private:
    std::string findSubstitutionsForReservedWords(const std::string& methodName) const {
        if ("and" == methodName) return "_and";
        return methodName;
    }

    AbstractMethod<T>* findMethod(const MethodSignature& signature) const {
        std::string correctName = findSubstitutionsForReservedWords(signature.getName());
        std::pair<NameMethodPair, NameMethodPair> matchingNames = methods.equal_range(correctName);
        for (NameMethodPair namedMethod = matchingNames.first; namedMethod != matchingNames.second; namedMethod++) {
            AbstractMethod<T>* method = (*namedMethod).second;
            if (method->supports(signature.getArguments())) {
	            return method;
            }
        }
        return 0;
    }

	std::vector<AbstractConstructor<T>*> constructors;
	typedef typename std::vector<AbstractConstructor<T>*>::const_iterator ConstructorPtr;

	std::map<std::string, AbstractMethod<T>*> methods;
	typedef typename std::map<std::string, AbstractMethod<T>*>::const_iterator NameMethodPair;
};

} // namespace slim

#endif	//	__CLASS_H__
