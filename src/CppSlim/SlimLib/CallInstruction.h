#ifndef __CALLINSTRUCTION__
#define __CALLINSTRUCTION__

#include "Instruction.h"
#include "SlimLibExports.h"

namespace slim {

class EXPORTING_SLIMLIB CallInstruction : public Instruction {
public:
    CallInstruction(const std::string& id);
	virtual void execute();
protected:
	std::string call(int callInstructionOffset);
private:
	std::string call();
};

};

#endif  //  __CALLINSTRUCTION__
