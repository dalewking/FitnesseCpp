#ifndef __UNSUPPORTED_ARGUMENTS_POLICY_H__
#define __UNSUPPORTED_ARGUMENTS_POLICY_H__

#include <string>
#include <sstream>
#include <vector>
#include <stdexcept>

namespace slim {

template <int expectedArgs>
class UnsupportedArgumentsPolicy {
public:
    virtual bool supports(const std::vector<std::string>& args) const {
        return args.size() == expectedArgs;
    }

    void validate(const std::vector<std::string>& args) const {
        if (!supports(args)) {
            throw std::runtime_error(errorMessage(args));
        }
    }

private:
    std::string errorMessage(const std::vector<std::string>& args) const {
        std::stringstream buff;
        buff << "passing in " << args.size() << " arguments";
        typedef std::vector<std::string>::const_iterator I;
        for (I i = args.begin(); i != args.end();) {
            if (i == args.begin()) buff << " (";
            buff << *i;
            i++;
            buff << (i != args.end() ? ", " : ")");
        }
        buff << " when " << expectedArgs << " arguments expected";
        return buff.str();
    }
};

} // namespace slim

#endif  //  __UNSUPPORTED_ARGUMENTS_POLICY_H__
