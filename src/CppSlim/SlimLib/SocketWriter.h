#ifndef __SOCKET_WRITER__
#define __SOCKET_WRITER__

#include <boost/asio.hpp>

namespace slim {

using boost::asio::ip::tcp;

class Writer {
    tcp::socket& socket_;
public:
    Writer( tcp::socket& socket);
    void write(const std::string& message);
};

} // namespace slim

#endif  //  __SOCKET_WRITER__
