#include "ImportInstruction.h"

#ifdef WIN32
#include "WindowsLibrary.h"
typedef slim::WindowsLibrary SharedLibrary;
#else
#include "UnixLibrary.h"
typedef UnixLibrary SharedLibrary;
#endif

namespace slim {

ImportInstruction::ImportInstruction(const std::string& id) : Instruction(id) {}

void ImportInstruction::execute() {
	recordId();
	addToResult(import());
}

void importLibrary(const std::string& libraryPath) {
    SharedLibrary library(libraryPath);
    library.call("registerClasses");
}

std::string ImportInstruction::import() {
    validateNumberOfArguments(1);
    importLibrary(arguments[0]);
    return "OK";
}

}
