#include "SymbolTable.h"

namespace slim {

SymbolTable::SymbolTable(void)
{
}

SymbolTable::~SymbolTable(void)
{
}


void SymbolTable::setSymbol(const std::string& key, const std::string& value) {
    symbols[key] = value;
}

std::string SymbolTable::valueForSymbol(const std::string& key) {
    return symbols[key];
}

void SymbolTable::clearSymbols() {
    symbols.clear();
}

bool SymbolTable::hasSymbol(const std::string& key) {
    return symbols.find(key) != symbols.end();
}

} // namespace slim
