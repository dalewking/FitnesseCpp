#include "InstructionFactory.h"
#include "Instruction.h"
#include "MakeInstruction.h"
#include "UnsupportedInstruction.h"
#include "CallAndAssignInstruction.h"
#include "ImportInstruction.h"

#include <iostream>
#include <stdexcept>


namespace slim {

class FactoryState {
    InstructionFactory* factory;
public:
    FactoryState(InstructionFactory* fac) : factory(fac) {}
    virtual FactoryState* operator()(const std::string& s) = 0;

    InstructionFactory* instructionFactory() const { return factory; }
};

class ConstructState : public FactoryState {
    FactoryState* nextState;

public:
    ConstructState(InstructionFactory* factory, FactoryState* next) : FactoryState(factory), nextState(next) {}
    FactoryState* operator()(const std::string& s) {
        instructionFactory()->setId(s);
        return nextState;
    }
};

class SetCommandState : public FactoryState {
    FactoryState* nextState;

public:
    SetCommandState(InstructionFactory* factory, FactoryState* next) : FactoryState(factory), nextState(next) {}
    FactoryState* operator()(const std::string& s) {
        instructionFactory()->setCommand(s);
        return nextState;
    }
};

class AddArgumentState : public FactoryState {
public:
    AddArgumentState(InstructionFactory* factory) : FactoryState(factory) {}
    FactoryState* operator()(const std::string& s) {
        instructionFactory()->addArgument(s);
        return this;
    }
};

class UninitializedState : public FactoryState {
public:
    UninitializedState():FactoryState(0){}
    virtual FactoryState* operator()(const std::string& s) {
        throw std::runtime_error("InstructionFactory must be initialized");
    }
};

UninitializedState UNINITIALIZED;

InstructionFactory::InstructionFactory() :
    add(&UNINITIALIZED),
    cmd(&UNINITIALIZED),
    ctor(&UNINITIALIZED),
    factoryState(ctor),
    firstState(factoryState),
    current(0) {}

void InstructionFactory::init()
{
    add = new AddArgumentState(this);
    cmd = new SetCommandState(this, add);
    ctor = new ConstructState(this, cmd);
    factoryState = ctor;
    firstState = factoryState;
}

InstructionFactory::~InstructionFactory() {
    delete ctor;
    delete cmd;
    delete add;
}

void InstructionFactory::reset() {
    factoryState = firstState;
}

void InstructionFactory::setId(const std::string& id) {
    nextId = id;
}

void InstructionFactory::setCommand(const std::string& command) {
	if (command == "make") {
        current = new MakeInstruction(nextId);
	} else if (command == "call") {
        current = new CallInstruction(nextId);
	} else if (command == "callAndAssign") {
        current = new CallAndAssignInstruction(nextId);
	} else if (command == "import") {
        current = new ImportInstruction(nextId);
	} else {
        current = new UnsupportedInstruction(nextId);
	}
    current->setCommand(command);
}

void InstructionFactory::addArgument(const std::string& argument) {
    current->addArgument(argument);
}

void InstructionFactory::operator()(const std::string& s) {
    factoryState = (*factoryState)(s);
}

Instruction* InstructionFactory::getInstruction() {
    return current;
}

bool InstructionFactory::hasInstruction() const {
    return current != 0;
}

} // namespace slim
