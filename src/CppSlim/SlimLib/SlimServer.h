#ifndef __SLIM_SERVER_H__
#define __SLIM_SERVER_H__

#include <boost/asio.hpp>
#include <boost/bind.hpp>

namespace slim {

using boost::asio::ip::tcp;

class SlimSession;

class SlimServer
{
public:
	SlimServer(boost::asio::io_service& io_service, short port);
	~SlimServer(void);
	void startSession(SlimSession* new_session,
	const boost::system::error_code& error);

private:
	void waitForConnection(SlimSession* new_session);

private:
  boost::asio::io_service& io_service_;
  tcp::acceptor acceptor_;
};

} // namespace slim

#endif	//	__SLIM_SERVER_H__
