#include "SlimServerLauncher.h"
#include "SlimServer.h"
#include "Flags.h"
#include "Log.h"

#include <string>
#include <iostream>

#include <boost/asio.hpp>

namespace slim {

SlimServerLauncher::SlimServerLauncher(int port) : portNumber(port)
{
}

SlimServerLauncher::~SlimServerLauncher(void)
{
}

int SlimServerLauncher::runServer()
{
    boost::asio::io_service io_service;

    SlimServer s(io_service, portNumber);

    io_service.run();

    if (Flags::VERBOSE()) Log::get().dump(std::cout);
    return 0;
}

int SlimServerLauncher::port()
{
    return portNumber;
}

} // namespace slim
