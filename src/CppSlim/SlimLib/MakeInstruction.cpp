#include "MakeInstruction.h"
#include "AbstractClassRegistry.h"
#include "AbstractClass.h"
#include "Instances.h"
#include "Log.h"
#include "Flags.h"
#include "StringUtilities.h"

namespace slim {

MakeInstruction::MakeInstruction(const std::string& id) : Instruction(id) {}
void MakeInstruction::execute() {
	recordId();
	addToResult(make());
}

void makeInstance(const std::string& instanceName, const std::string& className, const std::vector<std::string>& args) {
   	AbstractClass* clazz = AbstractClassRegistry::instance().forName(className);
    Instances::registerInstance(instanceName, clazz->newInstance(args));

	if (Flags::VERBOSE())
		log("made " + instanceName + " instance of " + className + " with " + (args.empty() ? "no args" : "args " + asString(args)));
}

std::string MakeInstruction::make() {
	validateNumberOfArguments(2);
	makeInstance(arguments[0], arguments[1], lookUpArgs(2));
    return "OK";
}

}
