#ifndef __VALIDATING_CONSTRUCTOR_H__
#define __VALIDATING_CONSTRUCTOR_H__

#include "AbstractConstructor.h"

namespace slim {

template<class T, class ArgumentPolicy>
class ValidatingConstructor: public AbstractConstructor<T> {
protected:
    ArgumentPolicy argumentPolicy;
public:
    virtual bool supports(const std::vector<std::string>& args) const {
        return argumentPolicy.supports(args); }
};

} // namespace slim

#endif  //  __VALIDATING_CONSTRUCTOR_H__
