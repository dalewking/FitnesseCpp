#include "Flags.h"

namespace slim {

Flag<bool> Flags::VERBOSE(false);
Flag<int> Flags::WAIT(0);

} // namespace slim
