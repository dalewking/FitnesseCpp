#ifndef __CONSTRUCTORS_H__
#define __CONSTRUCTORS_H__

#include "ValidatingConstructor.h"
#include "UnsupportedArgumentsPolicy.h"
#include "conversion.h"
#include <vector>
#include <map>
#include <string>

namespace slim {

template <class R>
class ZeroArgumentConstructor : public ValidatingConstructor<R, UnsupportedArgumentsPolicy<0> > {
public:
    R* invoke() const {
	}

    R* invoke(const std::vector<std::string>& args) const {
        ValidatingConstructor<R, UnsupportedArgumentsPolicy<0> >::argumentPolicy.validate(args);
        return new R();
    }
};

template <class R, typename T1>
class AbstractOneArgumentConstructor : public ValidatingConstructor<R, UnsupportedArgumentsPolicy<1> > {
public:
};

template <class R, typename T1>
class OneArgumentConstructor : public AbstractOneArgumentConstructor<R, T1> {
public:
    R* invoke(const std::vector<std::string>& args) const {
        ValidatingConstructor<R, UnsupportedArgumentsPolicy<1> >::argumentPolicy.validate(args);
        return new R(argumentOfType<T1>(args[0]));
    }
};

template <class R, typename T1>
class DirectOneArgumentConstructor : public AbstractOneArgumentConstructor<R, T1> {
    std::map<std::string, T1> argsByName;
public:
//    DirectOneArgumentConstructor(std::pair<std::string, T1> args){argsByName.insert(args);}
    void argumentValue(const std::string& argName, T1 arg){argsByName.insert(std::make_pair(argName, arg));}

    R* invoke(const std::vector<std::string>& args) const {
        ValidatingConstructor<R, UnsupportedArgumentsPolicy<1> >::argumentPolicy.validate(args);
//        std::map<std::string, T1>::const_iterator p = argsByName.find(args[0]);
        if (argsByName.find(args[0]) == argsByName.end()) throw std::runtime_error("unsupported argument " + args[0]);
		return new R((*argsByName.find(args[0])).second);
	}
};

template <class R, typename T1, typename T2>
class TwoArgumentConstructor : public ValidatingConstructor<R, UnsupportedArgumentsPolicy<2> > {
public:
    R* invoke(const T1& arg1, const T1& arg2) const {
		return new R(arg1, arg2);
	}

    R* invoke(const std::vector<std::string>& args) const {
        ValidatingConstructor<R, UnsupportedArgumentsPolicy<2> >::argumentPolicy.validate(args);
        return new R(argumentOfType<T1>(args[0]),argumentOfType<T2>(args[1]));
    }
};

} // namespace slim

#endif  //  __CONSTRUCTORS_H__
