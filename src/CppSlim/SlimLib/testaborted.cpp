/* 
 * File:   testaborted.cpp
 * Author: SG0173087
 * 
 * Created on May 15, 2009, 10:54 AM
 */

#include "TestAborted.h"

namespace slim {

	TestAborted::TestAborted() : std::runtime_error("__EXCEPTION__:ABORT_SLIM_TEST:") {}

} // namespace slim
