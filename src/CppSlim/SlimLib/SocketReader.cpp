#include "SocketReader.h"

namespace slim {

Reader::Reader( tcp::socket& socket) : socket_(socket) {}

std::string Reader::read(int amount) {
    char* c = new char[amount + 1];
    boost::scoped_ptr<char> buff(c);
    buff.get()[amount] = 0;
    boost::asio::read(socket_, boost::asio::buffer(buff.get(), amount), boost::asio::transfer_all());
    return buff.get();
}

} // namespace slim
