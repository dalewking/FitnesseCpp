#ifndef __SLIM_H__
#define __SLIM_H__

#include "SlimLibExports.h"

namespace slim {

class EXPORTING_SLIMLIB Slim
{
public:
	static int run(int argc, char* argv[]);

private:
	Slim();
	~Slim();
};

} // namespace slim

#endif	//	__SLIM_H__
