#include "AbstractClassRegistry.h"
#include "AbstractClass.h"
#include "NoClass.h"

namespace slim {
	AbstractClassRegistry AbstractClassRegistry::defaultInstance;

	AbstractClassRegistry& AbstractClassRegistry::instance() {
		return defaultInstance;
	}

	AbstractClassRegistry::AbstractClassRegistry() {
	}

	void AbstractClassRegistry::registerClass(AbstractClass* clazz) {
		classesByName[clazz->getName()] = clazz;
	}

	void AbstractClassRegistry::clear() {
		for (NameClassPair i = classesByName.begin(); i != classesByName.end(); i++)
			delete (*i).second;
		classesByName.clear();
	}

	AbstractClass* AbstractClassRegistry::forName(const std::string& name) {
		if (classesByName.find(name) == classesByName.end()) throw NoClass(name);
		return classesByName[name];
	}

	AbstractClass* AbstractClassRegistry::release(const std::string& name) {
		AbstractClass* toBeRemoved = forName(name);
		classesByName.erase(name);
		return toBeRemoved;
	}

	std::map<std::string, AbstractClass* > AbstractClassRegistry::classesByName;

} // namespace slim
