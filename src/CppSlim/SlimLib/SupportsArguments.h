#ifndef __SUPPORTS_ARGUMENTS_H__
#define __SUPPORTS_ARGUMENTS_H__

#include <string>
#include <vector>

namespace slim {

class SupportsArguments {
public:
	virtual ~SupportsArguments(){}
	virtual bool supports(const std::vector<std::string>& args) const = 0;
};

} // namespace slim

#endif	//	__SUPPORTS_ARGUMENTS_H__
