#include "WindowsLibrary.h"
#include <iostream>
#include <windows.h> 

namespace slim {
    class InstanceHolder {
        HINSTANCE handle;
    public:
        InstanceHolder(HINSTANCE instance) : handle(instance){}
        bool isLoaded() { return handle != 0; }
        HINSTANCE getInstance() { return handle; }
    };

    WindowsLibrary::WindowsLibrary(const std::string& path) : name(path), handle(0) {
        init(path);
    }

    WindowsLibrary::~WindowsLibrary(){
        //todo even though the holder is deleted, the library is never unloaded. Need to figure out when it is safe to unload the lib.
        delete handle;
    }

    void WindowsLibrary::init(const std::string& path) {
        handle = new InstanceHolder(::LoadLibraryA(path.c_str()));
        if (isLoaded())
            std::cout << "Loaded " << name << std::endl << std::flush;
        else
            std::cout << "failed to load " << name << std::endl << std::flush;
    }

    bool WindowsLibrary::isLoaded() const {
        return handle->isLoaded();
    }

    std::string WindowsLibrary::getName() const {
        return name;
    }

    void WindowsLibrary::call(const std::string& functionName) const {
        if (!isLoaded()) return;

        typedef void (*function)();
        function func = (function) ::GetProcAddress(handle->getInstance(), functionName.c_str());
        if (func == NULL) {
            std::cout << functionName << " could not be found in " << (*this) << std::endl;
        } else {
            std::cout << "registering " << (*this) << std::endl;
            func();
        }
    }


    void WindowsLibrary::unload() {
        ::FreeLibrary(handle->getInstance());
        delete handle;
    }

    std::ostream& operator<<(std::ostream& ostr, const WindowsLibrary& lib) {
        ostr << "library " << lib.getName();
        return ostr;
    }


}
