#include "UnixLibrary.h"
#include <dlfcn.h>
#include <iostream>

namespace slim {
    UnixLibrary::UnixLibrary(const std::string& path) : name(path), handle(0) {
        init(path, RTLD_NOW | RTLD_GLOBAL);
    }

    void UnixLibrary::init(const std::string& path, int flag) {
        handle = ::dlopen(path.c_str(), flag);
        if (isLoaded())
            std::cout << "Loaded " << name << std::endl << std::flush;
        else
            std::cout << "failed to load " << name << ": " << ::dlerror() << std::endl << std::flush;
    }

    bool UnixLibrary::isLoaded() const {
        return handle != 0;
    }

    std::string UnixLibrary::getName() const {
        return name;
    }

    void UnixLibrary::call(const std::string& functionName) const {
        if (!isLoaded()) return;

        ::dlerror(); /* clear error code */
        typedef void (*function)();
        function func = (function) ::dlsym(handle, functionName.c_str());
        char* err;
        if ((err = ::dlerror()) != NULL) {
            std::cout << functionName << " could not be found in " << (*this) << ": " << err << std::endl;
        } else {
            std::cout << "calling " << functionName << std::endl;
            func();
        }
    }


    void UnixLibrary::unload() {
        ::dlclose(handle);
        handle = 0;
    }

    std::ostream& operator<<(std::ostream& ostr, const UnixLibrary& lib) {
        ostr << "library " << lib.getName();
        return ostr;
    }


}
