#ifndef __SYMBOL_TABLE_H__
#define __SYMBOL_TABLE_H__

#include <map>
#include <string>

#include "SlimLibExports.h"

namespace slim {

class EXPORTING_SLIMLIB SymbolTable
{
public:
    SymbolTable(void);
    ~SymbolTable(void);

    void setSymbol(const std::string& key, const std::string& value);
    std::string valueForSymbol(const std::string& key);
    void clearSymbols();
    bool hasSymbol(const std::string& key);

private:
    std::map<std::string, std::string> symbols;

};

} // namespace slim

#endif  //  __SYMBOL_TABLE_H__
