#ifndef __LOG_H__
#define __LOG_H__

#include <iostream>
#include <list>
#include <string>

#include "SlimLibExports.h"

namespace slim {

class EXPORTING_SLIMLIB Log
{
public:
	static Log& get();
	void dump(std::ostream& s);
	void write(const std::string& message);
	void clear();
protected:
	Log();
	~Log();
private:
	static Log instance;
	std::list<std::string> messages;
};

void log(const std::string& message);

} // namespace slim

#endif	//	__LOG_H__
