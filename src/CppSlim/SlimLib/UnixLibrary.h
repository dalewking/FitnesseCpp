#ifndef __UNIX_LIBRARY__
#define __UNIX_LIBRARY__

#include <string>

namespace slim {

class UnixLibrary {
    std::string name;
    void* handle;

public:
    UnixLibrary(const std::string& path);

    void init(const std::string& path, int flag);

    bool isLoaded() const;

    std::string getName() const;

    void call(const std::string& functionName) const;

    void unload();
};

std::ostream& operator<<(std::ostream& ostr, const UnixLibrary& lib);

};

#endif  //  __UNIX_LIBRARY__
