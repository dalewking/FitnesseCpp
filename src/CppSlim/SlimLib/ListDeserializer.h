#ifndef __LIST_DESERIALIZER__
#define __LIST_DESERIALIZER__

#include "SlimList.h"
#include "conversion.h"

#include <string>

#include "SlimLibExports.h"
#include "SlimException.h"

namespace slim {

// Pretty much a straight port of the Java class from Fitnesse by Robert Martin
class EXPORTING_SLIMLIB ListDeserializer {
    std::string serialized;
    size_t index;
    StringList result;
    int maxDepth;

public:
    static StringList deserialize(const std::string& s);
    static StringList deserialize(const std::string& s, int maxDepth);

    class SyntaxError : public SlimException {
    public:
        SyntaxError(const std::string& message) : SlimException("MESSAGE_NOT_UNDERSTOOD: " + message) {}
    };

protected:
    ListDeserializer(const std::string& s);

    StringList deserialize();

    StringList deserializeString();

    StringList deserializeList();

    void deserializeItem();

    std::string getString(int length);

    int getLength();

    void checkForColon();

    void checkForOpenBracket();

    void checkForClosedBracket();

    bool charsLeft();
    char getChar();
    std::string getStringSoFar();

    void checkSerializedStringIsValid();

    void setMaxDepth(int depth);
};

namespace {
    template<>
    StringList Convert<std::string,StringList>(const std::string& in, StringList& out)
    {
        out = ListDeserializer::deserialize(in, -1);
        return out;
    }
};

} // namespace slim

#endif  //  __LIST_DESERIALIZER__
