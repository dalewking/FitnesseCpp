#ifndef __METHOD_SIGNATURE__
#define __METHOD_SIGNATURE__

#include <string>
#include <vector>

#include "SlimLibExports.h"

namespace slim {

class EXPORTING_SLIMLIB MethodSignature {
    std::string methodName;
    std::vector<std::string> args;
public:
    MethodSignature(const std::string& name, const std::vector<std::string>& arguments);
    std::string getName() const;
    std::vector<std::string> getArguments() const;
    std::string asString() const;
};

};

#endif  //  __METHOD_SIGNATURE__
