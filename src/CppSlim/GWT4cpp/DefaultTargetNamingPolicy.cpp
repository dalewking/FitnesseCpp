#include "DefaultTargetNamingPolicy.h"

namespace gwt {
	std::string DefaultTargetNamingPolicy::getTargetName(const std::string& className) const {
		return className + "_target";
	}
}
