#ifndef __REGEX__
#define __REGEX__

#include <vector>
#include <string>
#include "GWT4cppExports.h"

namespace gwt {
    class EXPORTING_GWT4CPP RegExArgCollector {
        std::string pattern;
    public:
        RegExArgCollector(const std::string& patternString);
		bool matches(const std::string methodString);
   		std::vector<std::string> collectArgs(const std::string &methodString);
        std::string getPattern() const;
    };
};

#endif  //  __REGEX__
