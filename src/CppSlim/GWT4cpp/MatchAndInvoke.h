#ifndef __MATCHANDINVOKE__
#define __MATCHANDINVOKE__

#include <string>

namespace gwt {
	class MatchAndInvoke {
	public:
		virtual std::string invoke(const std::string &methodString) = 0;
		virtual bool matches(const std::string methodString) = 0;

		virtual ~MatchAndInvoke() = 0;
	};
};

#endif	//	__MATCHANDINVOKE__
