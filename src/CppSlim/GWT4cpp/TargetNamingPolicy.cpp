#include "TargetNamingPolicy.h"

#include "DefaultTargetNamingPolicy.h"
#define TARGET_NAMING_POLICY DefaultTargetNamingPolicy

namespace gwt {
	std::string getTargetName(const std::string& classname) {
		TARGET_NAMING_POLICY targetNamingStrategy;
		return targetNamingStrategy.getTargetName(classname);
	}
};
