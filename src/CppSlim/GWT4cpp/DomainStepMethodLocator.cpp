#include "DomainStepMethodLocator.h"

#include "NullMatchAndInvoke.h"

#include <algorithm>
#include <functional>
#include <sstream>
#include <iostream>
#include <list>

namespace gwt {

	typedef std::vector<MethodAndInvocationTarget*>::const_iterator MI;

	std::list<RegexMethodMapping>& mappings() {
		static std::list<RegexMethodMapping> mappings;
		return mappings;
	}

	void DomainStepMethodLocator::registerDomainStep(const std::string& regex,
														const std::string& classname,
														const std::string& func) {
		mappings().push_back(RegexMethodMapping(regex, classname, func));
	}

	void DomainStepMethodLocator::clearMappings() {
		mappings().clear();
	}

	DomainStepMethodLocator::DomainStepMethodLocator() {
		for (std::list<RegexMethodMapping>::const_iterator iter = mappings().begin(); iter != mappings().end(); ++iter) {
			gwt::MethodAndInvocationTarget* mAndI = new gwt::MethodAndInvocationTarget((*iter).classname, (*iter).func, (*iter).regex);
			add((*iter).classname, mAndI);
		}
	}

	DomainStepMethodLocator::~DomainStepMethodLocator() {
		for (MI mAndI = matchAndInvokes.begin(); mAndI != matchAndInvokes.end(); mAndI++) {
			delete *mAndI;
		}
	}

	std::vector<MethodAndInvocationTarget*> DomainStepMethodLocator::getForClass(const std::string& className) {
		std::vector<MethodAndInvocationTarget*> forClass;
		for (MI mAndI = matchAndInvokes.begin(); mAndI != matchAndInvokes.end(); mAndI++) {
			if ((*mAndI)->isForClass(className))
				forClass.push_back(*mAndI);
		}
		return forClass;
	}

	void DomainStepMethodLocator::add(const std::string& classname, gwt::MethodAndInvocationTarget* matchAndInvoke) {
		matchAndInvokes.push_back(matchAndInvoke);
		invokersByClassName.insert(std::pair<std::string, gwt::MethodAndInvocationTarget*>(classname, matchAndInvoke));
	}

    NullMatchAndInvoke NULL_MATCH_AND_INVOKE;

	MethodAndInvocationTarget& DomainStepMethodLocator::find(const std::string& methodString) {
		std::vector<MethodAndInvocationTarget*> matches = findPossibleMatches(methodString);

        if (matches.empty())
			return NULL_MATCH_AND_INVOKE;
		if (matches.size() > 1) {
			handleAmbiguousStepDefinitions(methodString, matches);
		}

		return *(matches.back());
	}

	std::vector<MethodAndInvocationTarget*> DomainStepMethodLocator::findPossibleMatches(const std::string& methodString) {
		std::vector<MethodAndInvocationTarget*> matches;

		std::mem_fun1_t<bool, MethodAndInvocationTarget, std::string> fun = std::mem_fun(&MethodAndInvocationTarget::matches);
		MI found = matchAndInvokes.begin();
		MI end = matchAndInvokes.end();
		while (found != matchAndInvokes.end()) {
			found = std::find_if(found, end, std::bind2nd(fun, methodString));
			if (found != end) {
				matches.push_back(*found);
				++found;
			}
		}
		return matches;
	}

	void DomainStepMethodLocator::handleAmbiguousStepDefinitions(const std::string& methodString, const std::vector<MethodAndInvocationTarget*>& matches) {
		std::ostringstream buff;
		buff << "ambiguous step definitions found for \'" << methodString << "'" << std::endl;
		for (MI iter = matches.begin(); iter != matches.end(); ++iter) {
			buff << (*iter)->getMethodAsString() << " \"" << (*iter)->getPattern() << "\"" << std::endl;
		}
		throw std::runtime_error(buff.str());
	}
};
