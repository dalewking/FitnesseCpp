#ifndef __DOMAINSTEPNAMES__
#define __DOMAINSTEPNAMES__

#include <string>
#include "GWT4cppExports.h"

namespace gwt {
	class EXPORTING_GWT4CPP DomainStepNames
	{
	public:

		virtual std::string given(std::string methodString) = 0;
		virtual std::string when(std::string methodString) = 0;
		virtual std::string then(std::string methodString) = 0;
		virtual std::string _and(std::string methodString) = 0;

		virtual ~DomainStepNames(void);
	};
};

#endif	//	__DOMAINSTEPNAMES__
