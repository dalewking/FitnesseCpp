#ifndef __DOMAINSTEPNAMESACCESSOR__ 
#define __DOMAINSTEPNAMESACCESSOR__

#include "GWT4cppExports.h"

namespace gwt {
    class DomainStepNames;

    class EXPORTING_GWT4CPP DomainStepNamesAccessor
    {
    public:
        virtual ~DomainStepNamesAccessor(void);

        virtual DomainStepNames& getDomainStepNames();

        static void registerInstance(DomainStepNames*);
    protected:
        static DomainStepNames& getInstance();
    private:
        static DomainStepNames* instance;
    };
};

#endif  //  __DOMAINSTEPNAMESACCESSOR__
