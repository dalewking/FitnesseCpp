#ifndef __NullMatchAndInvoke__
#define __NullMatchAndInvoke__

#include "MethodAndInvocationTarget.h"
#include "SlimException.h"

#include <string>

namespace gwt {
	class NullMatchAndInvoke : public gwt::MethodAndInvocationTarget {
		public:
			NullMatchAndInvoke();
			virtual std::string invoke(const std::string &methodString);
			virtual bool matches(const std::string methodString);
	};
};
#endif	//	__NullMatchAndInvoke__
