#ifndef __DEFAULTTARGETNAMINGPOLICY__ 
#define __DEFAULTTARGETNAMINGPOLICY__

#include <string>

namespace gwt {
	class DefaultTargetNamingPolicy
	{
		public:
			std::string getTargetName(const std::string& className) const;
	};
};

#endif  //  __DEFAULTTARGETNAMINGPOLICY__
