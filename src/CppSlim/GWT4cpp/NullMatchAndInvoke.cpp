#include "NullMatchAndInvoke.h"

namespace gwt {
	NullMatchAndInvoke::NullMatchAndInvoke():MethodAndInvocationTarget("", "", "") {}
	std::string NullMatchAndInvoke::invoke(const std::string &methodString) {
		throw std::runtime_error("You need a step class with an annotated method matching this pattern: '" + methodString + "'\n" +
		"Example:\n" +
		"  class StepClass {\n" +
		"    public:\n" +
		"      DOMAIN_STEP(\"" + methodString + "\", StepClass, domainStep)\n" +
		"      void domainStep() {\n" +
		"      // TODO implement step\n" +
		"      }\n\n" +
		"      DOMAIN_STEPS(StepClass);\n" + 
		"  };\n" +
		"  REGISTER_CLASS(StepClass)\n" +
	    "    WITH_CONSTRUCTOR(StepClass)\n" +
	    "    WITH_METHOD(void, StepClass, domainStep)\n" +
        "  END_REGISTRATION\n");
	}

	bool NullMatchAndInvoke::matches(const std::string methodString) {
		return false;
	}
};
