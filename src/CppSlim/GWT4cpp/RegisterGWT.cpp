#include "RegisterGWT.h"
#include "../slimlib/RegisterClass.h"
#include "DomainStepsForSlim.h"

#if STATIC_BUILD
namespace gwt {
    void registerGWT() {
#else
using namespace gwt;
extern "C" {
	void registerClasses() {
#endif
        REGISTER(DomainStepsForSlim)
	}
};

