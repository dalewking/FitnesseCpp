#include "DomainStepNamesAccessor.h"

#include <stdexcept>
#include <string>

namespace gwt {
    DomainStepNames* DomainStepNamesAccessor::instance = 0;

    DomainStepNamesAccessor::~DomainStepNamesAccessor(void)
    {
    }

    DomainStepNames& DomainStepNamesAccessor::getDomainStepNames() {
        return getInstance();
    }

    DomainStepNames& DomainStepNamesAccessor::getInstance() {
        if (instance == 0)
            throw new std::runtime_error(std::string("no instance of DomainStepNames available"));
        return *instance;
    }

    void DomainStepNamesAccessor::registerInstance(DomainStepNames* domainStepNames) {
        instance = domainStepNames;
    }
};
