#ifndef __MARKED_CLASS__
#define __MARKED_CLASS__

#include <string>
#include <vector>

#include "GWT4cppExports.h"

namespace slim {
	class AbstractClass;
	class AbstractInstance;
};

namespace gwt {
	class EXPORTING_GWT4CPP MarkedClass {
		mutable slim::AbstractClass* clas;

	public:
		MarkedClass(slim::AbstractClass* clazz);
		~MarkedClass();
		slim::AbstractClass* getClass() const;
		std::string getName() const;
		slim::AbstractInstance* createInstance(const std::vector<std::string>& constructorArguments);
    private:
        slim::AbstractInstance* registeredInstance(const std::vector<std::string>& constructorArguments) const;
	};

};

#endif	//	__MARKED_CLASS__
