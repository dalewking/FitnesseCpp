#include "DomainStepsForSlim.h"
#include "DomainStepMethodLocator.h"
#include "MethodAndInvocationTarget.h"
#include "MarkedClassFinder.h"
#include "MarkedClass.h"
#include "DefaultInstantiator.h"
#include "ChainedInstantiator.h"
#include "DomainStepNamesAccessor.h"

#include <algorithm>
#include <vector>

namespace gwt {

	class MapInstancesToMethodAndInvocationTargets {
		DomainStepMethodLocator* locator;
	public:
		MapInstancesToMethodAndInvocationTargets(DomainStepMethodLocator* methodLocator) : locator(methodLocator) {}
		void operator()(slim::AbstractInstance* instance) {
			std::vector<MethodAndInvocationTarget*> methodsForClass = locator->getForClass(instance->className());
			for (std::vector<MethodAndInvocationTarget*>::iterator iter = methodsForClass.begin(); iter != methodsForClass.end(); ++iter)
				(*iter)->setInstance(instance);
		}
	};

	DomainStepsForSlim::DomainStepsForSlim(void) : methodLocator(new DomainStepMethodLocator) {
        std::vector<std::string> stepConstructorArgs;
        std::list<Instantiator*> customInstantiators;
		initialize(stepConstructorArgs, customInstantiators);
	}

	DomainStepsForSlim::DomainStepsForSlim(const std::vector<std::string>& stepConstructorArgs) : methodLocator(new DomainStepMethodLocator) {
        std::list<Instantiator*> customInstantiators;
		initialize(stepConstructorArgs, customInstantiators);
	}

	DomainStepsForSlim::DomainStepsForSlim(Instantiator* customInstantiator) : methodLocator(new DomainStepMethodLocator) {
        std::vector<std::string> stepConstructorArgs;
        std::list<Instantiator*> customInstantiators;
        customInstantiators.push_back(customInstantiator);
        initialize(stepConstructorArgs, customInstantiators);
	}

    void DomainStepsForSlim::initialize(const std::vector<std::string>& stepConstructorArgs, std::list<Instantiator*> customInstantiators) {
        DomainStepNamesAccessor::registerInstance(this);
		std::vector<MarkedClass*> markedClasses = MarkedClassFinder::instance().getMarkedClasses();
		std::vector<slim::AbstractInstance*> instancesCreated;

        DefaultInstantiator defaultInstantiator(stepConstructorArgs, instancesCreated);
        ChainedInstantiator instantiator(instancesCreated);

        std::for_each(customInstantiators.begin(), customInstantiators.end(), 
            std::bind1st(std::mem_fun(&ChainedInstantiator::add), &instantiator));

        instantiator.add(&defaultInstantiator);
        std::for_each(markedClasses.begin(), markedClasses.end(), std::bind1st(std::mem_fun(&Instantiator::operator()), &instantiator));

        for (std::list<Instantiator*>::iterator iter = customInstantiators.begin(); iter != customInstantiators.end(); ++iter)
        {
		    std::vector<slim::AbstractInstance*>& otherInstances = (*iter)->getInstances();
            std::copy(otherInstances.begin(), otherInstances.end(), std::back_inserter(instancesCreated));
        }
		MapInstancesToMethodAndInvocationTargets mapInstancesToMethodAndInvocationTargets(methodLocator);
		std::for_each(instancesCreated.begin(), instancesCreated.end(), mapInstancesToMethodAndInvocationTargets);
    }

	DomainStepsForSlim::~DomainStepsForSlim(void) {
		delete methodLocator;
        DomainStepNamesAccessor domainStepNamesAccessor;
        if (&domainStepNamesAccessor.getDomainStepNames() == this)
            domainStepNamesAccessor.registerInstance(0);
	}

	std::string DomainStepsForSlim::given(std::string methodString) {
		return execute(methodString);
	}

	std::string DomainStepsForSlim::when(std::string methodString) {
		return execute(methodString);
	}

	std::string DomainStepsForSlim::then(std::string methodString) {
		return execute(methodString);
	}

	std::string DomainStepsForSlim::_and(std::string methodString) {
		return execute(methodString);
	}

	std::string DomainStepsForSlim::execute(const std::string& methodString) {

        gwt::MethodAndInvocationTarget& mAndI = methodLocator->find(methodString);
		return mAndI.invoke(methodString);
	}

	REGISTER_CLASS(DomainStepsForSlim)
		WITH_CONSTRUCTOR(DomainStepsForSlim)
		WITH_1_ARG_METHOD(std::string, DomainStepsForSlim, given, std::string)
		WITH_1_ARG_METHOD(std::string, DomainStepsForSlim, when, std::string)
		WITH_1_ARG_METHOD(std::string, DomainStepsForSlim, then, std::string)
		WITH_1_ARG_METHOD(std::string, DomainStepsForSlim, _and, std::string)
	END_REGISTRATION

}
