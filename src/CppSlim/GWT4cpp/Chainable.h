#ifndef __CHAINABLE__
#define __CHAINABLE__

namespace gwt {
    template <class T>
    class Chainable {
        T* instance;
        Chainable<T>* nextInChain;
    public:
        Chainable(T* t) : instance(t), nextInChain(0) {}
        ~Chainable() { instance = 0; }
        template <class A>
        bool operator()(A a) {
            if ((*instance)(a)) return true;
            if (nextInChain == 0) return false;
            return (*nextInChain)(a);
        }

        void setNext(Chainable<T>* next) { nextInChain = next; }
    };
};
#endif  //  __CHAINABLE__
