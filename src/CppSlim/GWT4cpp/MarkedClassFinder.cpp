#include "MarkedClassFinder.h"
#include "MarkedClass.h"
#include "AbstractClass.h"

namespace gwt {

	MarkedClassFinder* MarkedClassFinder::singleton = 0;

	MarkedClassFinder& MarkedClassFinder::instance() {
		if (singleton == 0)
			singleton = new MarkedClassFinder();
		return *singleton;
	}

	MarkedClassFinder::MarkedClassFinder(void) {
	}

	MarkedClassFinder::~MarkedClassFinder(void)	{
		for (MarkedClassIterator mAndI = classes.begin(); mAndI != classes.end(); mAndI++) {
			delete *mAndI;
		}
		classes.clear();
	}

	void MarkedClassFinder::registerClass(slim::AbstractClass* clazz) {
		classes.push_back(new MarkedClass(clazz));
	}

	std::vector<MarkedClass*> MarkedClassFinder::getMarkedClasses() {
		return classes;
	}

	void MarkedClassFinder::cleanup() {
		delete singleton;
		singleton = 0;
	}
};
