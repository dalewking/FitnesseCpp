#ifndef __DOMAINSTEPNAMESINJECTINGINSTANTIATOR__
#define __DOMAINSTEPNAMESINJECTINGINSTANTIATOR__

#include "Instantiator.h"
#include "Instance.h"
#include "Instances.h"
#include "DomainStepNames.h"
#include "MarkedClass.h"
#include "Constructors.h"
#include "Class.h"
#include "TargetNamingPolicy.h"

#include <string>
#include <vector>

namespace gwt {

    template <class T>
    class DomainStepNamesInjectingInstantiator : public Instantiator {
        DomainStepNames* domainStepNames;
        std::vector<slim::AbstractInstance*> instances;

    public:
        DomainStepNamesInjectingInstantiator() : domainStepNames(0) {}
        DomainStepNamesInjectingInstantiator(DomainStepNames* domainStepNames)
                                                        : domainStepNames(domainStepNames) {
        }

		bool operator()(MarkedClass* markedClass) {
            std::vector<std::string> args;
            std::string instanceName = getTargetName(markedClass->getName());
            try {
                slim::Class<T>* clas = dynamic_cast<slim::Class<T>* >(markedClass->getClass());
                if (clas == 0) return false;
                slim::AbstractInstance* instance = clas->template newDirectInstance<gwt::DomainStepNames *>(domainStepNames);
                instances.push_back(instance);
                return true;
            } catch (slim::NoConstructorFound&) {
                return false;
            }
        }

	    ~DomainStepNamesInjectingInstantiator() {
        }

        std::vector<slim::AbstractInstance*>& getInstances() {
            return instances;
        }
    };
};

#endif  //  __DOMAINSTEPNAMESINJECTINGINSTANTIATOR__
