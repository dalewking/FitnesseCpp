#ifndef __MARKED_CLASS_FINDER__
#define __MARKED_CLASS_FINDER__

#include <vector>
#include <iostream>

#include "GWT4cppExports.h"

namespace slim {
	class AbstractClass;
};

namespace gwt {
	class MarkedClass;

	class EXPORTING_GWT4CPP MarkedClassFinder
	{
		static MarkedClassFinder* singleton;
		std::vector<MarkedClass*> classes;

		typedef std::vector<MarkedClass*>::const_iterator MarkedClassIterator;

	public:
		static MarkedClassFinder& instance();

		MarkedClassFinder(void);
		~MarkedClassFinder(void);

		void registerClass(slim::AbstractClass* clazz);
		std::vector<MarkedClass*> getMarkedClasses();

		static void cleanup();
	};
};

#endif	//	__MARKED_CLASS_FINDER__
