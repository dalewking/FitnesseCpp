#ifndef __CHAINEDINSTANTIATOR__
#define __CHAINEDINSTANTIATOR__

#include "Instantiator.h"
#include <vector>
#include <list>

#include "GWT4cppExports.h"

namespace gwt {
    template <class T> class Chainable;

    class EXPORTING_GWT4CPP ChainedInstantiator : public Instantiator {
        std::vector<slim::AbstractInstance*>& instances;
        std::list<gwt::Chainable<Instantiator>*> instantiators;
        typedef std::list<gwt::Chainable<Instantiator>*>::iterator II;
	    public:
            ChainedInstantiator(std::vector<slim::AbstractInstance*>& instances);
            virtual bool operator()(MarkedClass* markedClass);
            virtual ~ChainedInstantiator();
            virtual std::vector<slim::AbstractInstance*>& getInstances();
            void add(Instantiator* instantiator);
    };
}

#endif  //  __CHAINEDINSTANTIATOR__
