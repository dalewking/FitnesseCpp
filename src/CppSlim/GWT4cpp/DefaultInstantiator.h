#ifndef __DEFAULTINSTANTIATOR__
#define __DEFAULTINSTANTIATOR__

#include "Instantiator.h"

#include <string>
#include <vector>

namespace gwt {
	class MarkedClass;


	class DefaultInstantiator : public Instantiator {
		std::vector<slim::AbstractInstance*>& instancesCreated;
		std::vector<std::string> constructorArgs;
	public:
		DefaultInstantiator(std::vector<slim::AbstractInstance*>& instances);
		DefaultInstantiator::DefaultInstantiator(const DefaultInstantiator& rhs);
		DefaultInstantiator(const std::vector<std::string>& constructorArgs, std::vector<slim::AbstractInstance*>& instances);
		bool operator()(MarkedClass* markedClass);
		std::vector<slim::AbstractInstance*>& getInstances();
	};
};

#endif	//	__DEFAULTINSTANTIATOR__
