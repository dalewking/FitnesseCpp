#ifndef __TARGET_NAMING_POLICY__
#define __TARGET_NAMING_POLICY__

#include <string>
#include "GWT4cppExports.h"

namespace gwt {
	EXPORTING_GWT4CPP std::string getTargetName(const std::string& classname);
};

#endif	//	__TARGET_NAMING_POLICY__
