#ifndef __INSTANTIATOR__
#define __INSTANTIATOR__

#include <vector>

#include "GWT4cppExports.h"

namespace slim {
	class AbstractInstance;
};

namespace gwt {
	class MarkedClass;

	class EXPORTING_GWT4CPP Instantiator {
	public:
		virtual bool operator()(MarkedClass* markedClass) = 0;
		virtual ~Instantiator() = 0;
		virtual std::vector<slim::AbstractInstance*>& getInstances() = 0;
	};
};

#endif	//	__INSTANTIATOR__
