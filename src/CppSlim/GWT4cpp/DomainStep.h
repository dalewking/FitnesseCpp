#ifndef __DOMAIN_STEP__
#define __DOMAIN_STEP__

#include "DomainStepMethodLocator.h"
#include "MethodAndInvocationTarget.h"

#include <string>

namespace gwt {
	class RegisterDomainStep {
	public:

		RegisterDomainStep(const std::string& regex, const std::string& classname, const std::string& func) {
			DomainStepMethodLocator::registerDomainStep(regex, classname, func);
		}
	};
}

#define DOMAIN_STEP(regex, classname, methodname) \
	gwt::RegisterDomainStep register_##classname##_##methodname(regex, #classname, #methodname);


#endif	//	__DOMAIN_STEP__
