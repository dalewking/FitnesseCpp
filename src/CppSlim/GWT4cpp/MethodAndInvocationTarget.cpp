#include "MethodAndInvocationTarget.h"
#include "Instances.h"
#include "Methods.h"
#include "NoInstance.h"
#include "Flags.h"
#include "AbstractInstance.h"
#include "TargetNamingPolicy.h"
#include "MethodSignature.h"

namespace gwt {

MethodAndInvocationTarget::MethodAndInvocationTarget(const std::string& classname, 
													 const std::string& methodName, 
													 std::string methodString) : 
    regex(methodString),
	classname(classname), 
	methodName(methodName), 
	instance(0) {}

MethodAndInvocationTarget::~MethodAndInvocationTarget(){}

void MethodAndInvocationTarget::setInstance(slim::AbstractInstance* newInstance) {
	this->instance = newInstance;
}

slim::AbstractInstance* MethodAndInvocationTarget::getInstance() {
	if (instance != 0) return instance;
	return slim::Instances::find(getTargetName(classname));
}

std::string MethodAndInvocationTarget::invoke(const std::string &methodString) {
	std::vector<std::string> args = regex.collectArgs(methodString);
	slim::AbstractInstance* instance = getInstance();
	if (instance == 0) {
		throw slim::NoInstance(getTargetName(classname));
	}
    return instance->invoke(slim::MethodSignature(methodName, args));
}

bool MethodAndInvocationTarget::matches(const std::string methodString) {
	return regex.matches(methodString);
}

std::string MethodAndInvocationTarget::getMethodAsString() const {
	return classname + "::" + methodName;
}

std::string MethodAndInvocationTarget::getPattern() const {
	return regex.getPattern();
}

bool MethodAndInvocationTarget::isForClass(const std::string& name) const {
	return classname == name;
}

};
