#ifndef __DOMAINSTEPSFORSLIM__
#define __DOMAINSTEPSFORSLIM__

#include "DomainStepNames.h"
#include "../slimlib/RegisterClass.h"
#include "Instantiator.h"
#include <list>

#include "GWT4cppExports.h"

namespace gwt {
	class DomainStepMethodLocator;
};

namespace gwt {
	class MarkedClass;
	class Instantiator;

	class EXPORTING_GWT4CPP DomainStepsForSlim : public DomainStepNames {
		DomainStepMethodLocator* methodLocator;
		std::string execute(const std::string& methodString);

	public:
		DomainStepsForSlim(void);
		explicit DomainStepsForSlim(Instantiator* instatntiator);
		explicit DomainStepsForSlim(const std::vector<std::string>& stepConstructorArgs);
		virtual ~DomainStepsForSlim(void);

		std::string given(std::string methodString);
		std::string when(std::string methodString);
		std::string then(std::string methodString);
		std::string _and(std::string methodString);


		DECLARE_CLASS_REGISTRATION(DomainStepsForSlim);

    protected:
        void initialize(const std::vector<std::string>& stepConstructorArgs, std::list<Instantiator*> customInstantiators);
	};
}

#endif	//	__DOMAINSTEPSFORSLIM__
