#ifndef __DOMAINSTEPMETHODLOCATOR__
#define __DOMAINSTEPMETHODLOCATOR__

#include <string>
#include <vector>
#include <map>

#include "GWT4cppExports.h"

namespace gwt {
	class MethodAndInvocationTarget;

	struct RegexMethodMapping {
		std::string regex;
		std::string classname;
		std::string func;
		RegexMethodMapping(const std::string& r, const std::string& c, const std::string& f) :
			regex(r), classname(c), func(f){}
		RegexMethodMapping(const RegexMethodMapping& rhs) :
			regex(rhs.regex), classname(rhs.classname), func(rhs.func){}
		RegexMethodMapping& operator=(const RegexMethodMapping& rhs) {
			regex = rhs.regex;
			classname = rhs.classname;
			func = rhs.func;
			return *this;
		}
	};

	class EXPORTING_GWT4CPP DomainStepMethodLocator {
		std::vector<MethodAndInvocationTarget*> matchAndInvokes;
		std::multimap<std::string, MethodAndInvocationTarget*> invokersByClassName;
		

	public:
		static void registerDomainStep(const std::string& regex, const std::string& classname, const std::string& func);
		static void clearMappings();

		DomainStepMethodLocator();

		~DomainStepMethodLocator();

		void add(const std::string& classname, MethodAndInvocationTarget* matchAndInvoke);

		MethodAndInvocationTarget& find(const std::string& methodString);
		std::vector<MethodAndInvocationTarget*> DomainStepMethodLocator::getForClass(const std::string& className);

	private:
		std::vector<MethodAndInvocationTarget*> findPossibleMatches(const std::string& methodString);
		void handleAmbiguousStepDefinitions(const std::string& methodString, const std::vector<MethodAndInvocationTarget*>& matches);
	};
}

#endif	//	__DOMAINSTEPMETHODLOCATOR__
