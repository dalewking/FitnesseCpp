#include "MarkedClass.h"
#include "AbstractClass.h"
#include "Instances.h"
#include "NoConstructorFound.h"
#include "TargetNamingPolicy.h"

namespace gwt {
	MarkedClass::MarkedClass(slim::AbstractClass* clazz) : clas(clazz) {}
	MarkedClass::~MarkedClass() { 
		clas = 0;
	}
	slim::AbstractClass* MarkedClass::getClass() const { return clas; }
	std::string MarkedClass::getName() const { return getClass()->getName(); }
    slim::AbstractInstance* MarkedClass::registeredInstance(const std::vector<std::string>& constructorArguments) const {
        slim::AbstractInstance* instance = getClass()->newInstance(constructorArguments);
        slim::Instances::registerInstance(getTargetName(getClass()->getName()), instance);
		return instance;
    }
	slim::AbstractInstance* MarkedClass::createInstance(const std::vector<std::string>& constructorArguments) { 
		try {
            return registeredInstance(constructorArguments);
		} catch (slim::NoConstructorFound&) {
		}
		return registeredInstance(std::vector<std::string>()); 
	}

};
