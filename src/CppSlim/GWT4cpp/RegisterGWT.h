#ifndef __REGISTER_GWT_H__
#define __REGISTER_GWT_H__

#include "GWT4cppExports.h"

#if STATIC_BUILD
namespace gwt {
    EXPORTING_GWT4CPP void registerGWT();
};
#else
extern "C" {
	EXPORTING_GWT4CPP void registerClasses();
};
#endif

#endif	//	__REGISTER_GWT_H__
