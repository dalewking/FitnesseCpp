#include "MarkedClassFinder.h"
#include "RegisterClass.h"
#include "AbstractClassRegistry.h"

#define DOMAIN_STEPS(classname) \
DECLARE_CLASS_REGISTRATION(classname); \
static void classname::_registerDomainStepsIn##classname() { \
	slim::AbstractClass* clas = slim::AbstractClassRegistry::instance().forName(#classname); \
	gwt::MarkedClassFinder::instance().registerClass(clas); \
} 

#define FIND_MARKED_CLASS(classname) \
	REGISTER(classname) \
	classname::_registerDomainStepsIn##classname();
