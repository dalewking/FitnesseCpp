#ifndef __METHODANDINVOCATIONTARGET__
#define __METHODANDINVOCATIONTARGET__

#include "RegEx.h"

#include <string>
#include <vector>
#include "GWT4cppExports.h"

namespace slim {
	class AbstractInstance;
};

namespace gwt {
	class EXPORTING_GWT4CPP MethodAndInvocationTarget {
        RegExArgCollector regex;
		std::string classname;
		std::string methodName;
		slim::AbstractInstance* instance;

	public:
		MethodAndInvocationTarget(const std::string& classname, const std::string& methodName, std::string methodString);
        virtual ~MethodAndInvocationTarget();
		virtual std::string invoke(const std::string &methodString);
		virtual bool matches(const std::string methodString);

		std::string getMethodAsString() const;
        std::string getPattern() const;
		void setInstance(slim::AbstractInstance* instance);
		slim::AbstractInstance* getInstance();
		bool isForClass(const std::string& name) const;

	private:
		std::string instanceName();
	};
}; // namespace gwt

#endif	//	__METHODANDINVOCATIONTARGET__
