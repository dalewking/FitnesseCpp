#include "RegEx.h"
#include <boost/regex.hpp>

namespace gwt {
    boost::smatch getGroups(const std::string & pattern, const std::string &methodString) {
        boost::regex expression(pattern); 
        boost::smatch what;
        boost::regex_match(methodString, what, expression);
        return what;
    }

    RegExArgCollector::RegExArgCollector(const std::string& patternString) : pattern(patternString) {}

    std::string RegExArgCollector::getPattern() const {
	    return pattern;
    }

    bool RegExArgCollector::matches(const std::string methodString) {
        boost::regex expression(pattern); 
        boost::smatch what;
        return boost::regex_match(methodString, what, expression);
    }

    std::vector<std::string> RegExArgCollector::collectArgs(const std::string &methodString) {
        boost::smatch groups = getGroups(pattern, methodString);

        std::vector<std::string> args;
        boost::smatch::iterator iter = groups.begin();
        if (iter == groups.end()) return args;
        iter++;
        for (; iter != groups.end(); iter++)
        {
	        args.push_back(*iter);
        }
        return args;
    }
};

