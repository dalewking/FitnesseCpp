#include "DefaultInstantiator.h"
#include "MarkedClass.h"
#include "NoConstructorFound.h"
#include "AbstractInstance.h"

namespace gwt {

	DefaultInstantiator::DefaultInstantiator(std::vector<slim::AbstractInstance*>& instances) : instancesCreated(instances) {}
	DefaultInstantiator::DefaultInstantiator(const DefaultInstantiator& rhs) : instancesCreated(rhs.instancesCreated), constructorArgs(rhs.constructorArgs) {}
	DefaultInstantiator::DefaultInstantiator(const std::vector<std::string>& argsForConstructor, std::vector<slim::AbstractInstance*>& instances) : instancesCreated(instances), constructorArgs(argsForConstructor) {}

	bool DefaultInstantiator::operator()(MarkedClass* markedClass) {
        try {
    		slim::AbstractInstance* instance = markedClass->createInstance(constructorArgs);
	    	instancesCreated.push_back(instance);
            return true;
        } catch (slim::NoConstructorFound&) {
            return false;
        }
	}

	std::vector<slim::AbstractInstance*>& DefaultInstantiator::getInstances() {
		return instancesCreated;
	}
};
