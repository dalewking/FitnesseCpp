#include "ChainedInstantiator.h"
#include "Chainable.h"
#include "AbstractInstance.h"
#include "MarkedClass.h"

namespace gwt {

    ChainedInstantiator::ChainedInstantiator(std::vector<slim::AbstractInstance*>& instances) : instances(instances) {}

    bool ChainedInstantiator::operator()(MarkedClass* markedClass) {
        if (instantiators.empty()) return false;
        return (*(instantiators.front()))(markedClass);
    }
    
    ChainedInstantiator::~ChainedInstantiator() {
         for (II instantiator = instantiators.begin(); instantiator != instantiators.end(); ++instantiator)
             delete (*instantiator);
    }
    
    std::vector<slim::AbstractInstance*>& ChainedInstantiator::getInstances() {
        return instances;
    }
    
    void ChainedInstantiator::add(Instantiator* instantiator){
        Chainable<Instantiator>* nextLink = new Chainable<Instantiator>(instantiator);
        if (!instantiators.empty())
            instantiators.back()->setNext(nextLink);
        instantiators.push_back(nextLink);
    }
}
