#ifndef __EMPLOYEE_H__
#define __EMPLOYEE_H__

class Employee {
    int id;
    double salary;

public:
    explicit Employee():id(-1), salary(0){lastInstance = this;}
    explicit Employee(int i) : id(i), salary(0) {
        lastInstance = this;
    }
    Employee(int i, double d) : id(i), salary(d) {
        lastInstance = this;
    }
    int getId() { return id; }
    double getSalary() { return salary; }
    void setSalary(double newSalary) { salary = newSalary; }
    void increaseSalaryByPercent(double percentChange) { salary += salary * percentChange; }
    static Employee* lastInstance;
};

#endif  //  __EMPLOYEE_H__

