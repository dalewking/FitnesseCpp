#include <UnitTestHarness/TestHarness.h>
#include <Helpers/SimpleStringExtensions.h>
#include "Employee.h"

#include <typeinfo>
#include <stdexcept>
#include <iostream>

EXPORT_TEST_GROUP(TypeId)

namespace {
    void SetUp(){}
    void TearDown(){}

    class EmergencySplineReticulationStopTestException : public std::runtime_error {
    public:
        EmergencySplineReticulationStopTestException(const std::string& message) : std::runtime_error(message) {}
    };
    
    void foo() {
        throw EmergencySplineReticulationStopTestException("blech");
    }
};

TEST(TypeId, tryIt)
{
    Employee employee;
    CHECK(std::string(typeid(employee).name()).find("Employee") != std::string::npos);
}

TEST(TypeId, exceptionName)
{
    try {
        foo();
        FAIL("Should get exception");
    } catch(std::exception& ex) {
        std::string className(typeid(ex).name());
        CHECK(className.find("StopTest") != std::string::npos);
    }
}
