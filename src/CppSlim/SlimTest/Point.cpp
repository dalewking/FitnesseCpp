#include "Point.h"

bool operator==(const Point& lhs, const Point& rhs) {
	return lhs.x == rhs.x && lhs.y == rhs.y;
}

std::ostream& operator<<(std::ostream& s, Point udt) {
	return s << '(' << udt.x << ',' << udt.y << ')';
}

std::istream& operator>>(std::istream& s, Point& udt) {
	double x = 0;
	double y = 0;
	char c = 0;
	s >> c;
	if (c == '(') {
		s >> x >> c;
		if (c == ',') s >> y >> c;
		if (c != ')') s.clear(std::ios_base::badbit);
	} else {
		s.putback(c);
		s >> x;
	}
	if (s) udt = Point(x, y);
	return s;
}
