#include <UnitTestHarness/TestHarness.h>
#include <Helpers/SimpleStringExtensions.h>

#include <SlimLib/Instances.h>
#include "Employee.h"
#include <SlimLib/Class.h>

using namespace slim;
namespace { 
    InstancesImpl* instances;
    void SetUp() {instances = new InstancesImpl;} 
    void TearDown() {
        instances->doClear();
        delete instances;
    } 

    std::vector<std::string> makeArgs(const std::string& arg) {
        std::vector<std::string> args;
	    args.push_back(arg);
        return args;
    }
};


TEST(InstancesImpl, findInstanceWithMethod)
{
	Class<Employee> employeeClass("Employee");
	employeeClass.addConstructor(new TwoArgumentConstructor<Employee, int, double>());
	employeeClass.addMethod("setSalary", new OneArgumentMethod<Employee, void, double>(&Employee::setSalary));

    std::vector<std::string> args;
    args.push_back("123");
    args.push_back("7.50");
    AbstractInstance* instance = employeeClass.newInstance(args);
    instances->doRegisterInstance("library1", instance);

    args.clear();
    args.push_back("9.85");
    AbstractInstance* foundInstance = instances->doFindInstanceWithMethodSignature(MethodSignature("setSalary", args));
    POINTERS_EQUAL(instance, foundInstance);
}
