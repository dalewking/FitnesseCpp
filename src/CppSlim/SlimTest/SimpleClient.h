#ifndef __SIMPLE_CLIENT_H__
#define __SIMPLE_CLIENT_H__

#include <SlimLib/SlimList.h>

#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include <map>

class SimpleClient
{
    typedef slim::StringList StringList;
    typedef slim::string_tree_t string_tree_t;
    
    boost::asio::io_service io_service;
    boost::asio::ip::tcp::socket socket;
    boost::system::error_code ec;
    std::string slimVersion;
    std::string response;
    mutable std::map<std::string, string_tree_t> responsesById;

public:
    SimpleClient(const std::string& host, int port);
    ~SimpleClient(void);

    void sayBye();
    void writeToServer(const std::string& message);
    std::string readEncodedResponse();
    std::string readLine();
    bool isConnected();
    void reportProblem();
    void sendInstructionsAndGetResults(const StringList& instructions);
    std::string responseTo(const std::string& id) const;
};

#endif  //  __SIMPLE_CLIENT_H__

