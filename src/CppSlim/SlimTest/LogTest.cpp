#include <UnitTestHarness/TestHarness.h>
#include <Helpers/SimpleStringExtensions.h>

#include <SlimLib/Log.h>

#include <sstream>
#include <iostream>

EXPORT_TEST_GROUP(Log)

using namespace slim;

namespace {
    void SetUp() {}
    void TearDown() {Log::get().clear();}
};

TEST(Log, dump)
{
    std::stringstream buff;
    Log::get().write("Hello");
    Log::get().dump(buff);

    CHECK_EQUAL("[Slim]: Hello\n", buff.str());
}
/*
TEST(Log, dumpToCout)
{
    Log::get().write("Hello");
    Log::get().dump(std::cout);
}
*/
