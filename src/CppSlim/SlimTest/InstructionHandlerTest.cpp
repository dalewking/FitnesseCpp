#include <UnitTestHarness/TestHarness.h>
#include <Helpers/SimpleStringExtensions.h>

#include <SlimLib/InstructionHandler.h>
#include <SlimLib/ListUtilities.h>
#include <SlimLib/ListDeserializer.h>

#include <algorithm>
#include <iostream>
#include <boost/scoped_ptr.hpp>

EXPORT_TEST_GROUP(InstructionHandler)

namespace {
    void SetUp(){}
    void TearDown(){}
};

using namespace slim;

TEST(InstructionHandler, identifyInstruction)
{
    StringList instructionList;
    instructionList.push_back(asList("id1", "import", "test", 0));
    InstructionHandler handler;

    std::for_each(instructionList.begin(), instructionList.end(), boost::apply_visitor(handler));
    handler.finish();

    LONGS_EQUAL(1, handler.instructionCount());
    boost::scoped_ptr<Instruction> instruction ( handler.nextInstruction());
    CHECK_EQUAL("id1", instruction->getId());
}

TEST(InstructionHandler, identifyTwoInstruction)
{
    StringList instructionList;
    instructionList.push_back(asList("id1", "import", "test", 0));
    instructionList.push_back(asList("id2", "import", "another", 0));
    InstructionHandler handler;

    std::for_each(instructionList.begin(), instructionList.end(), boost::apply_visitor(handler));
    handler.finish();

    LONGS_EQUAL(2, handler.instructionCount());
    boost::scoped_ptr<Instruction> instruction1(handler.nextInstruction());
    CHECK_EQUAL("id1", instruction1->getId());
    boost::scoped_ptr<Instruction> instruction2(handler.nextInstruction());
    CHECK_EQUAL("id2", instruction2->getId());
}

TEST(InstructionHandler, makeWithArguments)
{
    StringList instructionList;
    instructionList.push_back(asList("m1", "make", "obj1", "TestClass", "1", 0));
    InstructionHandler handler;

    std::for_each(instructionList.begin(), instructionList.end(), boost::apply_visitor(handler));
    handler.finish();

    LONGS_EQUAL(1, handler.instructionCount());
    boost::scoped_ptr<Instruction> instruction1(handler.nextInstruction());
    CHECK_EQUAL("m1", instruction1->getId());
    CHECK_EQUAL("make", instruction1->getCommand());

}

TEST(InstructionHandler, bigNEstedInstruction)
{
    std::string instructionText = "[000005:000017:decisionTable_0_1:000004:call:000015:decisionTable_0:000005:table:000571:[000009:000101:[000004:000014:cash in wallet:000011:credit card:000023:pints of milk remaining:000012:go to store?:]:000047:[000004:000001:0:000002:no:000001:0:000002:no:]:000049:[000004:000002:10:000002:no:000001:0:000003:yes:]:000049:[000004:000001:0:000003:yes:000001:0:000003:yes:]:000050:[000004:000002:10:000003:yes:000001:0:000003:yes:]:000047:[000004:000001:0:000002:no:000001:1:000002:no:]:000048:[000004:000002:10:000002:no:000001:1:000002:no:]:000048:[000004:000001:0:000003:yes:000001:1:000002:no:]:000051:[000004:000002:10:000003:yes:000001:1:000004:nope:]:]:]";
    StringList instructionList = ListDeserializer::deserialize(instructionText, 0);

    InstructionHandler handler;

    std::for_each(instructionList.begin(), instructionList.end(), boost::apply_visitor(handler));
    handler.finish();

    LONGS_EQUAL(1, handler.instructionCount());
    boost::scoped_ptr<Instruction> instruction1(handler.nextInstruction());
    CHECK_EQUAL("decisionTable_0_1", instruction1->getId());
    CHECK_EQUAL("call", instruction1->getCommand());

}
