#include <UnitTestHarness/TestHarness.h>
#include <Helpers/SimpleStringExtensions.h>

#include <SlimLib/Instruction.h>
#include <SlimLib/MakeInstruction.h>
#include <SlimLib/CallAndAssignInstruction.h>
#include <SlimLib/MalformedInstruction.h>
#include <SlimLib/NoClass.h>
#include <SlimLib/RegisterClass.h>
#include <SlimLib/Log.h>
#include <SlimLib/Flags.h>
#include <SlimLib/SymbolTable.h>
#include <SlimLib/StringListWrapper.h>
#include <SlimLib/Instances.h>

#include "TestClass.h"

#include <sstream>
#include <boost/scoped_ptr.hpp>


EXPORT_TEST_GROUP(Instruction)

using namespace slim;

namespace {
    SymbolTable symbolTable;

    void SetUp(){
        TestClass::registerClass();
    }
    void TearDown(){
        AbstractClassRegistry::instance().clear();
        Instances::clear();
        TestClass::cleanUp();
        Log::get().clear();
        Flags::set(Flags::VERBOSE, false);
        symbolTable.clearSymbols();
    }

    void execute(Instruction& instruction) {
        instruction.setSymbolTable(&symbolTable);
        instruction.execute();
    }

    void makeTestClass(const std::string& arg) {
        MakeInstruction instruction("make1");
        instruction.setCommand("make");
        instruction.addArgument("obj1");
        instruction.addArgument("TestClass");
        instruction.addArgument(arg);

        execute(instruction);
    }

    void makeTestClass() {
        makeTestClass("5");
    }

    void checkAnswer(const Instruction& instruction, const std::string& expectedId, const std::string& expectedAnswer) {
        boost::scoped_ptr<StringListWrapper> result(instruction.getResult());
        LONGS_EQUAL(2, result->size());
        std::string& id = result->front();
        CHECK_EQUAL(expectedId, id);
        std::string& answer = result->back();
        CHECK_EQUAL(expectedAnswer, answer);
    }
};

TEST(Instruction, import)
{
/*
    Instruction instruction("id1");
    instruction.setCommand("import");
    instruction.addArgument("path");

    execute(instruction);
    checkAnswer(instruction, "id1", "OK");
    */
}

TEST(Instruction, Make_Malformed_NoInstanceOrClassname)
{
    MakeInstruction instruction("bad_make1");
    instruction.setCommand("make");

    try {
        execute(instruction);
        FAIL("should throw exception");
    } catch(MalformedInstruction& ex) {
        CHECK_EQUAL(std::string("__EXCEPTION__:message:<<MALFORMED_INSTRUCTION make >>"), ex.what());
    }
}

TEST(Instruction, Make_Malformed_NoClassname)
{
    MakeInstruction instruction("bad_make2");
    instruction.setCommand("make");
    instruction.addArgument("obj1");

    try {
        execute(instruction);
        FAIL("should throw exception");
    } catch(MalformedInstruction& ex) {
        CHECK_EQUAL(std::string("__EXCEPTION__:message:<<MALFORMED_INSTRUCTION make >>"), ex.what());
    }
}

TEST(Instruction, Make_UnknownClass)
{
    MakeInstruction instruction("make1");
    instruction.setCommand("make");
    instruction.addArgument("obj1");
    instruction.addArgument("BogusClassName");

    try {
        execute(instruction);
        FAIL("should throw exception");
    } catch (NoClass& ex) {
        CHECK_EQUAL(std::string("__EXCEPTION__:message:<<COULD_NOT_INVOKE_CONSTRUCTOR BogusClassName No class BogusClassName has been registered >>"), ex.what());
    }
}

TEST(Instruction, Make_KnownClass)
{
    MakeInstruction instruction("make1");
    instruction.setCommand("make");
    instruction.addArgument("obj1");
    instruction.addArgument("TestClass");

    execute(instruction);
    CHECK(TestClass::lastObjectConstructed != 0);
}

TEST(Instruction, Make_KnownClass_oneArgumentForTheCtor)
{
    makeTestClass();
    CHECK(TestClass::lastObjectConstructed != 0);
    LONGS_EQUAL(5, TestClass::lastObjectConstructed->getId());
}


TEST(Instruction, Call)
{
    makeTestClass();
    CallInstruction instruction("call1");
    instruction.setCommand("call");
    instruction.addArgument("obj1");
    instruction.addArgument("doubleId");

    execute(instruction);
    LONGS_EQUAL(10, TestClass::lastObjectConstructed->getId());
}

TEST(Instruction, Call_OneArgument)
{
    makeTestClass();
    CallInstruction instruction("call1");
    instruction.setCommand("call");
    instruction.addArgument("obj1");
    instruction.addArgument("setId");
    instruction.addArgument("13");

    execute(instruction);
    LONGS_EQUAL(13, TestClass::lastObjectConstructed->getId());
}

TEST(Instruction, logWhenVerbose)
{
    CHECK(!Flags::VERBOSE());
    Flags::set(Flags::VERBOSE, true);
    CHECK(Flags::VERBOSE());

    makeTestClass();
    std::stringstream buff;
    Log::get().dump(buff);
    CHECK_EQUAL("[Slim]: made obj1 instance of TestClass with args (5)\n", buff.str());
}

TEST(Instruction, callAndAssign)
{
    makeTestClass();
    CallAndAssignInstruction instruction("call1");
    instruction.setCommand("callAndAssign");
    instruction.addArgument("symbolX");
    instruction.addArgument("obj1");
    instruction.addArgument("getId");

    execute(instruction);

    CHECK_EQUAL("5", symbolTable.valueForSymbol("symbolX"));
}

TEST(Instruction, makeWithSymbolInArg)
{
    symbolTable.setSymbol("X", "331");

    MakeInstruction instruction("make1");
    instruction.setCommand("make");
    instruction.addArgument("obj1");
    instruction.addArgument("TestClass");
    instruction.addArgument("$X");

    execute(instruction);

    LONGS_EQUAL(331, TestClass::lastObjectConstructed->getId());
}

TEST(Instruction, callWithSymbolInArg)
{
    symbolTable.setSymbol("X", "714");

    makeTestClass();
    CallInstruction instruction("call1");
    instruction.setCommand("call");
    instruction.addArgument("obj1");
    instruction.addArgument("setId");
    instruction.addArgument("$X");

    execute(instruction);

    LONGS_EQUAL(714, TestClass::lastObjectConstructed->getId());
}
