#include <UnitTestHarness/TestHarness.h>
#include <Helpers/SimpleStringExtensions.h>

#include <SlimLib/ListDeserializer.h>
#include <SlimLib/ListSerializer.h>
#include <SlimLib/encoding.h>

#include <algorithm>

EXPORT_TEST_GROUP(ListDeserializer)

using namespace slim;

class StringAccumulator : public boost::static_visitor<>  {
    std::string& total;
public:
    StringAccumulator(std::string& s) : total(s) {}
    void operator()(const std::string& s) { total += s; }
    void operator()(const StringList& t) {  std::for_each(t.begin(), t.end(), boost::apply_visitor(*this)); }
};

namespace {
    StringList testList;
    void SetUp(){}
    void TearDown(){testList.clear();}

    void check() {
        std::string serialized = serialize(testList);
        StringList deserialized = ListDeserializer::deserialize(serialized);
        LONGS_EQUAL(testList.size(), deserialized.size());
        if (testList.size() == deserialized.size()) {
            std::string s1;
            std::string s2;
            StringAccumulator acc1(s1);
            StringAccumulator acc2(s2);
            std::for_each(testList.begin(), testList.end(), boost::apply_visitor(acc1));
            std::for_each(deserialized.begin(), deserialized.end(), boost::apply_visitor(acc2));
            CHECK_EQUAL(s1, s2);
        }
    }
};

TEST(ListDeserializer, cantDeserializeEmptyString)
{
    try {
        ListDeserializer::deserialize("");
        FAIL("Did not throw exception");
    } catch(ListDeserializer::SyntaxError&) {}
}

TEST(ListDeserializer, cantDeserializeStringThatDoesntStartWithBracket)
{
    try {
        ListDeserializer::deserialize("hello");
        FAIL("Did not throw exception");
    } catch(ListDeserializer::SyntaxError&) {}
}

TEST(ListDeserializer, cantDeserializeStringThatDoesntEndWithBracket)
{
    try {
        ListDeserializer::deserialize("[000000:");
        FAIL("Did not throw exception");
    } catch(ListDeserializer::SyntaxError&) {}
}

TEST(ListDeserializer, emptyList)
{
    check();
}

TEST(ListDeserializer, listWithOneElement)
{
    testList.push_back("hello");
    check();
}

TEST(ListDeserializer, listWithTwoElements)
{
    testList.push_back("hello");
    testList.push_back("world");
    check();
}

TEST(ListDeserializer, listWithSubList)
{
    StringList sublist;
    sublist.push_back("hello");
//    sublist.push_back("world");
    testList.push_back(sublist);
//    testList.push_back("single");
    check();
  }
