To get slimtest to run using slim as a dll, add ;SLIM_DLL to the 
Preprocessor Definitions property of the slimtest project 
(Properties -> ConfigurationProperties -> C/C++ -> Preprocessor). 
You will also need to exclude the following files from building: 
  ListDeserializerTest.cpp
  SimpleClient.cpp
  SimpleClient.h
  SlimServerLauncherTest.cpp
Control-click on each file to select them, right click and select 
Properties, select Configuration Properties, General and set 
Excluded From Build to Yes.
