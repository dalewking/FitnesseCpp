#include <UnitTestHarness/TestHarness.h>
#include <Helpers/SimpleStringExtensions.h>
#include <boost/function.hpp>
#include <boost/type_traits.hpp>
#include <iostream>

namespace { void SetUp() {} void TearDown() {} };

struct TestFunctor {
    void operator()(int i, const std::string& s) {
//      std::cout << "TestFunctor::operator()(" << i  << ", \"" << s << "\") called." << std::endl;
    }
};


TEST(Functional, simple)
{
    boost::function<void (int x, const std::string& s)> f;
    TestFunctor t;
    f = t;
    f(3, "X");
}

TEST(Functional, traits)
{
    LONGS_EQUAL(2, boost::function_traits<void (int, std::string)>::arity);
//  LONGS_EQUAL(2, boost::function_traits<TestFunctor::operator()>::arity);
}
