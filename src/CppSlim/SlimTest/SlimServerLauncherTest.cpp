#include <UnitTestHarness/TestHarness.h>
#include <Helpers/SimpleStringExtensions.h>
#include "../slimlib/SlimServerLauncher.h"
#include "../slimlib/ListUtilities.h"
#include "../slimlib/Log.h"
#include "../slimlib/Flags.h"
#include "../slimlib/AbstractClassRegistry.h"
#include "../slimlib/Instances.h"

#include "TestClass.h"

#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include <memory>
#include <string>

#include "SimpleClient.h"

EXPORT_TEST_GROUP(SlimServerLauncher)

using namespace slim;

using boost::asio::ip::tcp;

namespace {
	boost::thread serverThread;
	SimpleClient* client = 0;

	struct callable {
		void operator()(){
			SlimServerLauncher launcher(1234);
			launcher.runServer();
		}
	};

	boost::thread startServer() {
		return boost::thread(callable());
	}

	void SetUp(){
		TestClass::registerClass();
		serverThread = startServer();
   		client = new SimpleClient("127.0.0.1", 1234);
	}
	void TearDown(){
		client->sayBye();
		delete client;
		serverThread.join();

		AbstractClassRegistry::instance().clear();
		Instances::clear();
		TestClass::cleanUp();

		Log::get().clear();
		Flags::set(Flags::VERBOSE, false);
	}
};


TEST(SlimServerLauncher, emptySession)
{
	CHECK(client->isConnected());
}

TEST(SlimServerLauncher, testImport)
{
	CHECK(client->isConnected());
		
	StringList statements;
	statements.push_back(asList("i1", "import", "com.sabre.fixtures", 0));
	client->sendInstructionsAndGetResults(statements);
	CHECK_EQUAL("OK", client->responseTo("i1"));
}

TEST(SlimServerLauncher, testMultipleImport)
{
	CHECK(client->isConnected());
		
	StringList statements;
	statements.push_back(asList("i1", "import", "com.sabre.fixtures", 0));
	statements.push_back(asList("i2", "import", "com.sabre.more.fixtures", 0));
	statements.push_back(asList("i3", "import", "com.sabre.even.morefixtures", 0));
	client->sendInstructionsAndGetResults(statements);
	CHECK_EQUAL("OK", client->responseTo("i1"));
	CHECK_EQUAL("OK", client->responseTo("i2"));
	CHECK_EQUAL("OK", client->responseTo("i3"));
}


TEST(SlimServerLauncher, testMake)
{
	CHECK(client->isConnected());
	StringList statements;
	statements.push_back(asList("m1", "make", "t1", "TestClass", 0));
	client->sendInstructionsAndGetResults(statements);
	CHECK_EQUAL("OK", client->responseTo("m1"));
}
