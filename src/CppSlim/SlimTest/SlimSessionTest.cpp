#include <UnitTestHarness/TestHarness.h>
#include <Helpers/SimpleStringExtensions.h>

#include <SlimLib/encoding.h>
#include <SlimLib/ListUtilities.h>
#include <iostream>

#include <SlimLib/SlimList.h>
#include <SlimLib/InstructionRunner.h>
#include <SlimLib/ResultSender.h>

EXPORT_TEST_GROUP(SlimSession)

using namespace slim;

namespace {
    void SetUp(){}
    void TearDown(){}
class MockResultSender : public ResultSender {
public:
    bool givenResults;
    MockResultSender():givenResults(false){}
public:
    void sendResults(const StringList& results) {
        givenResults = true;
    }
};

};

TEST(SlimSession, processInstructions_bye)
{
    MockResultSender sender;
    InstructionRunner runner(&sender);
    runner.process("bye");
    CHECK(!sender.givenResults);
}

TEST(SlimSession, processInstructions_import)
{
    MockResultSender sender;
    InstructionRunner runner(&sender);
    StringList instructions;
    instructions.push_back(asList("id1", "import", "path", 0));
    runner.process(serialize(instructions));
    CHECK(sender.givenResults);
}
