#ifndef __TEST_CLASS__
#define __TEST_CLASS__

class TestClass {
    int id;
public:
    static TestClass* lastObjectConstructed;

    TestClass(void);
    TestClass(int id);
    ~TestClass(void);

    int getId() const;
    void setId(int newId);
    void doubleId();
    void willThrow();

    static void cleanUp();
    static void registerClass();
};

#include <stdexcept>

class PleaseStopTestNow : public std::runtime_error {
	static const std::string STOP_TEST_PREFIX;
public:
	PleaseStopTestNow(const std::string& why);
};


#endif  //  __TEST_CLASS__
