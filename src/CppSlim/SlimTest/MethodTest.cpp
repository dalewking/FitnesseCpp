#include <UnitTestHarness/TestHarness.h>
#include <Helpers/SimpleStringExtensions.h>

#include <SlimLib/Methods.h>

EXPORT_TEST_GROUP(OneArgument)
EXPORT_TEST_GROUP(OneArgumentMethod)
EXPORT_TEST_GROUP(TwoArgument)

class CharSaver {
public:
	char it;
	CharSaver():it('\0'){}
	CharSaver(const CharSaver& rhs) : it(rhs.it) {}

	void operator =(const CharSaver& rhs) { it = rhs.it; }
	void saveIt(char c) {
		it = c;
	}

	char nextChar() {
		return it + 1;
	}
};

class TwoArg {
public:
	int i;
	std::string s;
	void call(int x, std::string v) {
		i = x;
		s = v;
	}
};

using namespace slim;

namespace { 
	void SetUp() {} 
	void TearDown() {}

	CharSaver save(const std::string& c) {
		OneArgumentMethod<CharSaver, void, char> saveCharMethod(&CharSaver::saveIt);

		CharSaver saver;
		std::vector<std::string> args;
		args.push_back(c);

		saveCharMethod.invoke(saver, args);
		return saver;
	}
};

TEST(OneArgumentMethod, GivenOneArg)
{
	CharSaver saver = save("X");
	CHECK(saver.it == 'X');
}

TEST(OneArgumentMethod, GivenTwoArg)
{
	OneArgumentMethod<CharSaver, void, char> saveCharMethod(&CharSaver::saveIt);

	CharSaver saver;
	std::vector<std::string> args;
	args.push_back("X");
	args.push_back("Y");

	try {
		saveCharMethod.invoke(saver, args);
		FAIL("should throw exception");
	} catch(std::runtime_error&) {
	}
}

TEST(OneArgument, GetResultBack)
{
	CharSaver saver = save("X");
	ZeroArgumentMethod<CharSaver, char> nextCharMethod(&CharSaver::nextChar);

	std::vector<std::string> args;
	std::string result = nextCharMethod.invoke(saver, args);
	CHECK_EQUAL("Y", result);
}

TEST(TwoArgument, IntAndString)
{
	TwoArg twofer;
	TwoArgumentMethod<TwoArg, void, int, std::string> twoArgMethod(&TwoArg::call);

	std::vector<std::string> args;
	args.push_back("3");
	args.push_back("three");
	twoArgMethod.invoke(twofer, args);
	CHECK_EQUAL("three", twofer.s);
	LONGS_EQUAL(3, twofer.i);
}

TEST(TwoArgument, Copy)
{
	TwoArg twofer;
	TwoArgumentMethod<TwoArg, void, int, std::string> original(&TwoArg::call);
	TwoArgumentMethod<TwoArg, void, int, std::string> copy = original;

	std::vector<std::string> args;
	args.push_back("3");
	args.push_back("three");
	copy.invoke(twofer, args);
	CHECK_EQUAL("three", twofer.s);
	LONGS_EQUAL(3, twofer.i);
}

