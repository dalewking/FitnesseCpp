#include <UnitTestHarness/TestHarness.h>
#include <Helpers/SimpleStringExtensions.h>
#include <SlimLib/StringListWrapper.h>

#include <boost/scoped_ptr.hpp>

EXPORT_TEST_GROUP(Encoder)

using namespace slim;

namespace {
	StringListWrapper* list;
	void SetUp(){
        list = createStringListWrapper();
    }
	void TearDown(){
        delete list;
	}
};

TEST(Encoder, nullList)
{
	CHECK_EQUAL("[000000:]", list->serialize());
}

TEST(Encoder, oneItemListSerialize)
{
	list->push_back("hello");
	CHECK_EQUAL("[000001:000005:hello:]", list->serialize());
}

TEST(Encoder, nestedListsSerialize)
{
	list->push_back("hello");
    boost::scoped_ptr<StringListWrapper> sublist(createStringListWrapper());
	sublist->push_back("cruel");
	list->push_back(*sublist);
	list->push_back("world");

	CHECK_EQUAL("[000003:000005:hello:000022:[000001:000005:cruel:]:000005:world:]", list->serialize());
}

TEST(Encoder, serializeNestedList)
{
	boost::scoped_ptr<StringListWrapper> sublist(createStringListWrapper());
	sublist->push_back("element");
	list->push_back(*sublist);
	CHECK_EQUAL("[000001:000024:[000001:000007:element:]:]", list->serialize());
}

TEST(Encoder, twoElementListSerialized)
{
	list->push_back("hello");
	list->push_back("world");

	CHECK_EQUAL("[000002:000005:hello:000005:world:]", list->serialize());
}
