#include "TestClass.h"
#include <SlimLib/Class.h>
#include <SlimLib/AbstractClassRegistry.h>
#include <iostream>

using namespace slim;

TestClass* TestClass::lastObjectConstructed = 0;

TestClass::TestClass(void) :id(3158322) { lastObjectConstructed = this; }
TestClass::TestClass(int i) :id(i) { lastObjectConstructed = this; }

TestClass::~TestClass(void) { 
	if (lastObjectConstructed == this) lastObjectConstructed = 0; 
}

int TestClass::getId() const {
	return id;
}

void TestClass::setId(int newId) {
	id = newId;
}

void TestClass::doubleId() {
	id *= 2;
}

void TestClass::willThrow() {
    throw PleaseStopTestNow("I want to get off");
}

void TestClass::cleanUp() {
	if (lastObjectConstructed == 0) return;

	delete lastObjectConstructed;
	lastObjectConstructed = 0;
}

void TestClass::registerClass() {
	Class<TestClass>* testClassClass = new Class<TestClass>("TestClass");
	testClassClass->addConstructor(new ZeroArgumentConstructor<TestClass>);
	testClassClass->addConstructor(new OneArgumentConstructor<TestClass, int>);
	testClassClass->addMethod("doubleId", new ZeroArgumentMethod<TestClass,void>(&TestClass::doubleId));
	testClassClass->addMethod("willThrow", new ZeroArgumentMethod<TestClass,void>(&TestClass::willThrow));
	testClassClass->addMethod("setId", new OneArgumentMethod<TestClass, void, int>(&TestClass::setId));
	testClassClass->addMethod("getId", new ZeroArgumentConstMethod<TestClass, int>(&TestClass::getId));
	AbstractClassRegistry::instance().registerClass(testClassClass);
}

const std::string PleaseStopTestNow::STOP_TEST_PREFIX = "StopTest: ";
PleaseStopTestNow::PleaseStopTestNow(const std::string& why): std::runtime_error(STOP_TEST_PREFIX + why) {}
