#include <SlimLib/Class.h>
#include <SlimLib/AbstractClassRegistry.h>
#include <SlimLib/Instances.h>
#include <SlimLib/NoClass.h>

#include <UnitTestHarness/TestHarness.h>
#include <Helpers/SimpleStringExtensions.h>

#include "Employee.h"

#include <boost/scoped_ptr.hpp>

using namespace slim;

EXPORT_TEST_GROUP(Class)

namespace { 
	void SetUp() {} 
	void TearDown() {
		AbstractClassRegistry::instance().clear();
		Instances::clear();
	}
};

TEST(Class, undefinedClass)
{
	try {
		AbstractClassRegistry::instance().forName("Employee");
		FAIL("shoud throw exception when no class defined");
	} catch (NoClass&) {
	}
}

TEST(Class, getName)
{
	AbstractClassRegistry::instance().registerClass(new Class<Employee>("Employee"));
	AbstractClass* clazz = AbstractClassRegistry::instance().forName("Employee");
	CHECK_EQUAL("Employee", clazz->getName());
}

TEST(Class, newInstance)
{
	Class<Employee>* employeeClass = new Class<Employee>("Employee");
	AbstractClassRegistry::instance().registerClass(employeeClass);
	AbstractClass* clazz = AbstractClassRegistry::instance().forName("Employee");
	employeeClass->addConstructor(new TwoArgumentConstructor<Employee, int, double>());
	
	std::vector<std::string> args;
	args.push_back("7");
	args.push_back("1.21");

	boost::scoped_ptr<AbstractInstance> instance(clazz->newInstance(args));
	CHECK(Employee::lastInstance != 0);
	LONGS_EQUAL(7, Employee::lastInstance->getId());
	DOUBLES_EQUAL(1.21, Employee::lastInstance->getSalary(), 0.01);
}

TEST(Class, invokeMethod)
{
	Class<Employee>* employeeClass = new Class<Employee>("Employee");
	AbstractClassRegistry::instance().registerClass(employeeClass);
	employeeClass->addConstructor(new TwoArgumentConstructor<Employee, int, double>());
	employeeClass->addMethod("setSalary", new OneArgumentMethod<Employee, void, double>(&Employee::setSalary));
	std::vector<std::string> args;
	args.push_back("7");
	args.push_back("1.21");

    boost::scoped_ptr<AbstractInstance> instance(employeeClass->newInstance(args));

	args.clear();
	args.push_back("3.14");
	instance->invoke(slim::MethodSignature("setSalary", args));
	DOUBLES_EQUAL(3.14, Employee::lastInstance->getSalary(), 0.01);
}
TEST(Class, invokeMethod_pickCorrectMethod)
{
	Class<Employee>* employeeClass = new Class<Employee>("Employee");
	AbstractClassRegistry::instance().registerClass(employeeClass);
	employeeClass->addConstructor(new TwoArgumentConstructor<Employee, int, double>());
	employeeClass->addMethod("setSalary", new OneArgumentMethod<Employee, void, double>(&Employee::setSalary));
	employeeClass->addMethod("increaseSalaryByPercent", new OneArgumentMethod<Employee, void, double>(&Employee::increaseSalaryByPercent));
	std::vector<std::string> args;
	args.push_back("7");
	args.push_back("5.00");

	boost::scoped_ptr<AbstractInstance> instance(employeeClass->newInstance(args));

	args.clear();
	args.push_back("0.2");
    instance->invoke(slim::MethodSignature("increaseSalaryByPercent", args));
	DOUBLES_EQUAL(6.0, Employee::lastInstance->getSalary(), 0.01);

	args.clear();
	args.push_back("9834749.99");
	instance->invoke(slim::MethodSignature("setSalary", args));
	DOUBLES_EQUAL(9834749.99, Employee::lastInstance->getSalary(), 0.01);
}

TEST(Class, getConstructorTaking_ZeroArg)
{
	boost::scoped_ptr<Class<Employee> > employeeClass(new Class<Employee>("Employee"));
	employeeClass->addConstructor(new ZeroArgumentConstructor<Employee>());
	ZeroArgumentConstructor<Employee>* ctor = employeeClass->getConstructorTaking();
	NOT_NULL(ctor);
}

TEST(Class, getConstructorTaking_OneArg)
{
	boost::scoped_ptr<Class<Employee> > employeeClass(new Class<Employee>("Employee"));
	employeeClass->addConstructor(new OneArgumentConstructor<Employee, int>());
	AbstractOneArgumentConstructor<Employee, int>* ctor = employeeClass->getConstructorTaking<int>();
	NOT_NULL(ctor);
}

TEST(Class, getConstructorTaking_TwoArg)
{
	boost::scoped_ptr<Class<Employee> > employeeClass(new Class<Employee>("Employee"));
	employeeClass->addConstructor(new TwoArgumentConstructor<Employee, int, double>());
	TwoArgumentConstructor<Employee, int, double>* ctor = employeeClass->getConstructorTaking<int, double>();
	NOT_NULL(ctor);
}

TEST(Class, hasMethod)
{
    boost::scoped_ptr<Class<Employee> > employeeClass(new Class<Employee>("Employee"));
	employeeClass->addMethod("setSalary", new OneArgumentMethod<Employee, void, double>(&Employee::setSalary));

	std::vector<std::string> args;
	args.push_back("7.50");
    CHECK(employeeClass->hasMethod(slim::MethodSignature("setSalary", args)));
}

TEST(Class, hasMethod_wrongArgs)
{
    boost::scoped_ptr<Class<Employee> > employeeClass(new Class<Employee>("Employee"));
	employeeClass->addMethod("setSalary", new OneArgumentMethod<Employee, void, double>(&Employee::setSalary));

	std::vector<std::string> args;
    CHECK(!employeeClass->hasMethod(slim::MethodSignature("setSalary", args)));

   	args.push_back("7.50");
   	args.push_back("7.50");
    CHECK(!employeeClass->hasMethod(slim::MethodSignature("setSalary", args)));
}
