#include <UnitTestHarness/TestHarness.h>
#include <Helpers/SimpleStringExtensions.h>
#include <SlimLib/AbstractClassRegistry.h>
#include <SlimLib/Class.h>
#include <SlimLib/NoClass.h>
#include "Employee.h"

#include <boost/scoped_ptr.hpp>

EXPORT_TEST_GROUP(AbstractClassRegistry)

namespace {
	slim::AbstractClassRegistry* registry;
	void SetUp(){registry = new slim::AbstractClassRegistry;}
	void TearDown(){registry->clear(); delete registry; }
};

TEST(AbstractClassRegistry, registeredClassesAreAvailable)
{
	registry->registerClass(new slim::Class<Employee>("Employee"));
	slim::AbstractClass* clas = registry->forName("Employee");
	NOT_NULL(clas);
	CHECK_EQUAL("Employee", clas->getName());
}

TEST(AbstractClassRegistry, releasedClassesArenotInTheRegistry)
{
	registry->registerClass(new slim::Class<Employee>("Employee"));
	boost::scoped_ptr<slim::AbstractClass> clas(registry->release("Employee"));

	try {
		slim::AbstractClass* stillInRegistry = registry->forName("Employee");
		CHECK(!stillInRegistry);
	} catch (slim::NoClass&) {
	}
}
