#include <UnitTestHarness/TestHarness.h>
#include <Helpers/SimpleStringExtensions.h>

#include <SlimLib/InstructionFactory.h>
#include <SlimLib/Instruction.h>
#include <SlimLib/MakeInstruction.h>
#include <SlimLib/ImportInstruction.h>
#include <SlimLib/CallAndAssignInstruction.h>
#include <SlimLib/UnsupportedInstruction.h>

#include <boost/scoped_ptr.hpp>

EXPORT_TEST_GROUP(InstructionFactory)

namespace {
    void SetUp(){}
    void TearDown(){}
};

using namespace slim;

TEST(InstructionFactory, constructOnHeap)
{
    InstructionFactory *factory = new InstructionFactory;
    factory->init();
    delete factory;
}

TEST(InstructionFactory, make_noConstructorArguments)
{
    InstructionFactory factory;
    factory.init();
    CHECK(!factory.hasInstruction());
    factory("id1");
    factory("make");
    CHECK(factory.hasInstruction());

    boost::scoped_ptr<Instruction> instruction ( factory.getInstruction());
    CHECK_EQUAL("id1", instruction->getId());
    CHECK_EQUAL("make", instruction->getCommand());
    CHECK(0 != dynamic_cast<slim::MakeInstruction*>(instruction.get()));
}

TEST(InstructionFactory, make_twoConstructorArguments)
{
    InstructionFactory factory;
    factory.init();
    CHECK(!factory.hasInstruction());
    factory("id1");
    factory("make");
    factory("A");
    factory("B");
    CHECK(factory.hasInstruction());

    boost::scoped_ptr<Instruction> instruction ( factory.getInstruction());
    CHECK_EQUAL("id1", instruction->getId());
    CHECK_EQUAL("make", instruction->getCommand());
    LONGS_EQUAL(2, instruction->getArguments().size());
}

TEST(InstructionFactory, createImportInstruction)
{
    InstructionFactory factory;
    factory.init();
    CHECK(!factory.hasInstruction());
    factory("id1");
    factory("import");
    factory("12");
    CHECK(factory.hasInstruction());

    boost::scoped_ptr<Instruction> instruction ( factory.getInstruction());
    CHECK_EQUAL("id1", instruction->getId());
    CHECK_EQUAL("import", instruction->getCommand());
    CHECK(0 != dynamic_cast<slim::ImportInstruction*>(instruction.get()));
    LONGS_EQUAL(1, instruction->getArguments().size());
    CHECK_EQUAL("12", instruction->getArguments()[0]);
}

TEST(InstructionFactory, createCallInstruction)
{
    InstructionFactory factory;
    factory.init();
    CHECK(!factory.hasInstruction());
    factory("12312");
    factory("call");
    factory("hello");
    factory("world");
    CHECK(factory.hasInstruction());

    boost::scoped_ptr<Instruction> instruction ( factory.getInstruction());
    CHECK_EQUAL("12312", instruction->getId());
    CHECK_EQUAL("call", instruction->getCommand());
    CHECK(0 != dynamic_cast<slim::CallInstruction*>(instruction.get()));
    LONGS_EQUAL(2, instruction->getArguments().size());
    CHECK_EQUAL("hello", instruction->getArguments()[0]);
    CHECK_EQUAL("world", instruction->getArguments()[1]);
}

TEST(InstructionFactory, createCallAndAssignInstruction)
{
    InstructionFactory factory;
    factory.init();
    CHECK(!factory.hasInstruction());
    factory("12312");
    factory("callAndAssign");
    factory("X");
    factory("5");
    factory("times");
    CHECK(factory.hasInstruction());

    boost::scoped_ptr<Instruction> instruction ( factory.getInstruction());
    CHECK_EQUAL("12312", instruction->getId());
    CHECK_EQUAL("callAndAssign", instruction->getCommand());
    CHECK(0 != dynamic_cast<slim::CallAndAssignInstruction*>(instruction.get()));
    LONGS_EQUAL(3, instruction->getArguments().size());
    CHECK_EQUAL("X", instruction->getArguments()[0]);
    CHECK_EQUAL("5", instruction->getArguments()[1]);
    CHECK_EQUAL("times", instruction->getArguments()[2]);
}

TEST(InstructionFactory, unsupportedInstruction)
{
    InstructionFactory factory;
    factory.init();
    CHECK(!factory.hasInstruction());
    factory("A123Z/X983-sdf8");
    factory("selfDestruct");
    factory("4");
    factory("seconds");
    CHECK(factory.hasInstruction());

    boost::scoped_ptr<Instruction> instruction ( factory.getInstruction());
    CHECK_EQUAL("A123Z/X983-sdf8", instruction->getId());
    CHECK_EQUAL("selfDestruct", instruction->getCommand());
    CHECK(0 != dynamic_cast<slim::UnsupportedInstruction*>(instruction.get()));
    LONGS_EQUAL(2, instruction->getArguments().size());
    CHECK_EQUAL("4", instruction->getArguments()[0]);
    CHECK_EQUAL("seconds", instruction->getArguments()[1]);
}
