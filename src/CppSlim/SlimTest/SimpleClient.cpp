#include "SimpleClient.h"
#include <SlimLib/SlimList.h>
#include <SlimLib/encoding.h>
#include <SlimLib/SocketReader.h>
#include <SlimLib/StringUtilities.h>
#include <SlimLib/ListUtilities.h>
#include <SlimLib/ListDeserializer.h>

#include <iostream>

using boost::asio::ip::tcp;

using namespace slim;

SimpleClient::SimpleClient(const std::string& host, int port) : socket(io_service) {
    boost::asio::ip::address hostAddress(boost::asio::ip::address::from_string(host));
    tcp::endpoint hostEndpoint(hostAddress, port);
    socket.connect(hostEndpoint, ec);
    slimVersion = readLine();
}

SimpleClient::~SimpleClient(void)
{
}


void SimpleClient::sayBye() {
    boost::asio::streambuf request;
    std::ostream request_stream(&request);
    request_stream << serialize("bye");
    boost::asio::write(socket, request);
}

void SimpleClient::writeToServer(const std::string& message) {
    boost::asio::streambuf request;
    std::ostream request_stream(&request);
    request_stream << message;
    boost::asio::write(socket, request);
}

class ResultCollector : public boost::static_visitor<> {
    std::map<std::string, string_tree_t>& idToResult;
    bool collectingInstructions;
    std::string lastId;
public:
    ResultCollector(std::map<std::string, string_tree_t>& map) : idToResult(map), collectingInstructions(true), lastId("") {}

    void operator()(const std::string& s) {
        if (collectingInstructions) {
            idToResult[s] = "";
            collectingInstructions = false;
            lastId = s;
        } else {
            idToResult[lastId] = s;
            collectingInstructions = true;
        }
    }

    void operator()(const StringList& instructions) {
        if (collectingInstructions) {
            ResultCollector collector(*this);
            std::for_each(instructions.begin(), instructions.end(), boost::apply_visitor(collector));
        } else {
            idToResult[lastId] = instructions;
            collectingInstructions = true;
        }
    }
};

std::string SimpleClient::readEncodedResponse() {
    Reader reader(socket);
    std::string messageFromServer = reader.read(6);
    reader.read(1);

    messageFromServer = reader.read(parseInt(messageFromServer));
    StringList results = ListDeserializer::deserialize(messageFromServer);
    ResultCollector collector(responsesById);
    std::for_each(results.begin(), results.end(), boost::apply_visitor(collector));

    this->response = messageFromServer;
    return messageFromServer;
}

std::string SimpleClient::readLine() {
    boost::asio::streambuf response;
    std::istream response_stream(&response);

    boost::asio::read_until(socket, response, '\n');
    std::string messageFromServer;
    response_stream >> messageFromServer;
    return messageFromServer;
}

bool SimpleClient::isConnected() {
    return slimVersion.find_first_of("Slim -- V") != std::string::npos;
}

void SimpleClient::reportProblem() {
    std::cout << ec.category().name() << " error (" << ec.value() << "): " << ec.message() << std::endl;
}

void SimpleClient::sendInstructionsAndGetResults(const StringList& instructions) {
    writeToServer(prefixWithLength(serialize(instructions)));
    readEncodedResponse();
}

std::string SimpleClient::responseTo(const std::string& id) const {
    return boost::get<std::string>(responsesById[id]);
}
