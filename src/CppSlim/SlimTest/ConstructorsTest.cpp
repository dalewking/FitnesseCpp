#include <UnitTestHarness/TestHarness.h>
#include <Helpers/SimpleStringExtensions.h>

#include <SlimLib/Constructors.h>
#include "Employee.h"

#include <boost/scoped_ptr.hpp>


namespace { 
    void SetUp() {} 
    void TearDown() {} 

    std::vector<std::string> makeArgs(const std::string& arg) {
        std::vector<std::string> args;
	    args.push_back(arg);
        return args;
    }
};

using namespace slim;

TEST(OneArgumentConstructor, InvokedWithExplicitType)
{
    DirectOneArgumentConstructor<Employee, int> ctor;
    ctor.argumentValue("12", 12);
	boost::scoped_ptr<Employee> emp (ctor.invoke(makeArgs("12")));
	LONGS_EQUAL(12, emp->getId());
}

TEST(OneArgumentConstructor, OneArgument)
{
	OneArgumentConstructor<Employee, int> ctor;

	std::vector<std::string> args;
	args.push_back("7");

	boost::scoped_ptr<Employee> emp (ctor.invoke(args));
	LONGS_EQUAL(7, emp->getId());
	DOUBLES_EQUAL(0.0, emp->getSalary(), 0.01);
}

TEST(TwoArgumentConstructor, TwoArgument)
{
	TwoArgumentConstructor<Employee, int, double> ctor;

	std::vector<std::string> args;
	args.push_back("7");
	args.push_back("3.14");

	boost::scoped_ptr<Employee> emp (ctor.invoke(args));
	LONGS_EQUAL(7, emp->getId());
	DOUBLES_EQUAL(3.14, emp->getSalary(), 0.01);
}

TEST(OneArgumentConstructor, Given0Args)
{
	OneArgumentConstructor<Employee, int> ctor;

	std::vector<std::string> args;

	try {
		boost::scoped_ptr<Employee> emp (ctor.invoke(args));
		FAIL("should throw runtime_error");
	} catch (std::runtime_error&) {
	}
}
TEST(OneArgumentConstructor, Given2Args)
{
	OneArgumentConstructor<Employee, int> ctor;

	std::vector<std::string> args;
	args.push_back("7");
	args.push_back("3.14");

	try {
		boost::scoped_ptr<Employee> emp (ctor.invoke(args));
		FAIL("should throw runtime_error");
	} catch (std::runtime_error&) {
	}
}

TEST(TwoArgumentConstructor, Given1Arg)
{
	TwoArgumentConstructor<Employee, int, double> ctor;

	std::vector<std::string> args;
	args.push_back("7");

	try {
		boost::scoped_ptr<Employee> emp (ctor.invoke(args));
		FAIL("should throw runtime_error");
	} catch (std::runtime_error&) {
	}
}
