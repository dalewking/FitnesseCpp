#include <UnitTestHarness/TestHarness.h>
#include <Helpers/SimpleStringExtensions.h>

#include <SlimLib/conversion.h>
#include "Point.h"

#include <string>

EXPORT_TEST_GROUP(Convert)
EXPORT_TEST_GROUP(Converter)
EXPORT_TEST_GROUP(argumentOfType)
EXPORT_TEST_GROUP(stringstream)

namespace { void SetUp() {} void TearDown() {} };

using namespace slim;

TEST(Converter, partialSpecialization)
{
	Converter<int, std::string> convert;
	std::string s;
	s = convert(2, s);
	CHECK_EQUAL("2", s);
}

TEST(Converter, longToChar)
{
	Converter<long, char> convert;
	std::string s = "3";
	long i = 87349857;
	char c = convert(i, c);
	LONGS_EQUAL(56, c);
}

TEST(Converter, stringToLong)
{
	Converter<std::string, long> convert;
	std::string s = "3";
	long i = convert(s, i);
	LONGS_EQUAL(3, i);
}

struct XY {
	int x;
	int y;
	XY(int x, int y) : x(x), y(y) {}
};

std::ostream& operator<<(std::ostream& s, const XY& p) {
	return s << p.x << " " << p.y;
}

TEST(Converter, pointToString)
{
	Converter<XY, std::string> convert;
	std::string s;
	XY p(3,7);
	s = convert(p, s);
	CHECK_EQUAL("3 7", s);
}

TEST(Convert, stringToInt)
{
	int i;
	i = Convert<std::string, int>(std::string("32"), i);
	LONGS_EQUAL(32, i);
}

TEST(Convert, boolToString)
{
	std::string actual;
	actual = Convert<bool, std::string>(true, actual);
	CHECK_EQUAL("true", actual);
}

TEST(Convert, intToString)
{
	std::string actual;
	actual = Convert<int, std::string>(1, actual);
	CHECK_EQUAL("1", actual);
}

TEST(Convert, UserDefinedTypeToString)
{
	std::string actual;
	actual = Convert<Point, std::string>(Point(3,7), actual);
	CHECK_EQUAL("(3,7)", actual);
}

TEST(Convert, StringToUserDefinedType)
{
	std::string actual = "(3,7)";
	Point udt;
	Point converted = Convert<std::string, Point>(actual, udt);
	LONGS_EQUAL(3, udt.x);
	LONGS_EQUAL(7, udt.y);
}

TEST(Convert, StringToUserDefinedType_BadString)
{
	std::string actual = "(3a7)";
	Point udt;
	try {
		Convert<std::string, Point>(actual, udt);
		FAIL("should throw exception");
	} catch(std::ios_base::failure&) {
	}
}

TEST(argumentOfType, intAndDouble)
{
	LONGS_EQUAL(3, argumentOfType<int>("3"));
	DOUBLES_EQUAL(1.95, argumentOfType<double>("1.95"), 0.001);
	CHECK_EQUAL("Hello", argumentOfType<std::string>("Hello"));
}

TEST(stringstream, errors)
{
	Point udt;
	std::stringstream s;
	s << std::boolalpha << "(3a7)";
	s >> udt;
	CHECK(!s.good());
	DOUBLES_EQUAL(0.0, udt.x, 0.0001);
	DOUBLES_EQUAL(0.0, udt.y, 0.0001);
	try {
		s.exceptions(std::ios_base::badbit|std::ios_base::failbit);
		FAIL("should throw exception");
	} catch(std::ios_base::failure&) {
	}
}

TEST(stringstream, no_errors)
{
	Point udt;
	std::stringstream s;
	s << std::boolalpha << "(3,7)";
	s >> udt;
	CHECK(s.good());
	DOUBLES_EQUAL(3.0, udt.x, 0.0001);
	DOUBLES_EQUAL(7.0, udt.y, 0.0001);
	s.exceptions(std::ios_base::badbit|std::ios_base::failbit);
}
