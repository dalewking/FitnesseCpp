#include <UnitTestHarness/TestHarness.h>
#include <Helpers/SimpleStringExtensions.h>

#include <SlimLib/ListSerializer.h>

EXPORT_TEST_GROUP(ListSerializer)

namespace {
	std::list<std::string> list;

	void SetUp(){}
	void TearDown(){
		list.clear();
	}
};

using namespace slim;

TEST(ListSerializer, nullList)
{
	CHECK_EQUAL("[000000:]", ListSerializer::serialize(list));
}

TEST(ListSerializer, oneItemListSerialize)
{
	list.push_back("hello");
	CHECK_EQUAL("[000001:000005:hello:]", ListSerializer::serialize(list));
}

TEST(ListSerializer, twoItemListSerialize)
{
	list.push_back("hello");
	list.push_back("world");
	CHECK_EQUAL("[000002:000005:hello:000005:world:]", ListSerializer::serialize(list));
}

TEST(ListSerializer, serializeNestedList)
{
	std::list<std::string> sublist;
	sublist.push_back("element");
	std::list<std::list<std::string> > lists;
	lists.push_back(sublist);
	CHECK_EQUAL("[000001:000024:[000001:000007:element:]:]", ListSerializer::serialize(lists));
}

TEST(ListSerializer, emptyListOfLists)
{
	std::list<std::list<std::string> > lists;
	CHECK_EQUAL("[000000:]", ListSerializer::serialize(lists));
}

