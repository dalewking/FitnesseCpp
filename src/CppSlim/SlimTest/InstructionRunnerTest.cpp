#include <UnitTestHarness/TestHarness.h>
#include <Helpers/SimpleStringExtensions.h>

#include <SlimLib/encoding.h>
#include <SlimLib/ListUtilities.h>
#include <SlimLib/SlimList.h>
#include <SlimLib/InstructionRunner.h>
#include <SlimLib/ResultSender.h>
#include <SlimLib/AbstractClassRegistry.h>
#include <SlimLib/Instances.h>
#include <SlimLib/TestAborted.h>

#include "TestClass.h"

#include <string>

EXPORT_TEST_GROUP(InstructionRunner)

using namespace slim;

namespace {
    void SetUp(){
        TestClass::registerClass();
    }
    void TearDown(){
  //      TestClass::cleanUp();
        AbstractClassRegistry::instance().clear();
        Instances::clear();
    }
class MockResultSender : public ResultSender {
public:
    std::string result;
    void sendResults(const StringList& results) {
        result = serialize(results);
    }
};

};

TEST(InstructionRunner, StopTest)
{
    MockResultSender resultSender;
    InstructionRunner runner(&resultSender);

    StringList instructions;
    instructions.push_back(asList("make_1", "make", "obj1", "TestClass", "1", 0));
    instructions.push_back(asList("call_1", "call", "obj1", "willThrow", 0));
    instructions.push_back(asList("call_2", "call", "obj1", "doubleId", 0));

    std::string serializedInstructions(serialize(instructions));
    try {
        runner.process(serializedInstructions);
    } catch (PleaseStopTestNow& ex) {
		FAIL(std::string(std::string("I got the exception: ") + ex.what()).c_str());
	} catch (slim::TestAborted& ex) {
        FAIL(std::string(std::string("I got the exception: ") + ex.what()).c_str());
    }
    CHECK(resultSender.result.find("make_1") != std::string::npos);
    CHECK(resultSender.result.find("__EXCEPTION__:ABORT_SLIM_TEST:") != std::string::npos);
    CHECK(resultSender.result.find("call_2") == std::string::npos);
}
