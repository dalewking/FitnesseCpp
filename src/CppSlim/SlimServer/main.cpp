#include "../SlimLib/Slim.h"
#if STATIC_BUILD
#include "../ExampleCppSlim/Register.h"
#include "../GWT4cpp/RegisterGWT.h"
#include "../GWTExamples/RegisterGWTExamples.h"
#endif

#include <iostream>

int main(int argc, char* argv[])
{
#if STATIC_BUILD
	slimexample::registerSlimExamples();
	gwt::registerGWT();
	gwtexample::registerGwtExample();
#endif
    return slim::Slim::run(argc, argv);
}

