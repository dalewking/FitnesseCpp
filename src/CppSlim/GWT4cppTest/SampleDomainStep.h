#ifndef __SAMPLE_DOMAIN_STEP__
#define __SAMPLE_DOMAIN_STEP__

#include "DomainSteps.h"

class SampleDomainStep
{
	DOMAIN_STEPS(SampleDomainStep);

public:
	void domainStep(int x);
};

#endif	//	__SAMPLE_DOMAIN_STEP__
