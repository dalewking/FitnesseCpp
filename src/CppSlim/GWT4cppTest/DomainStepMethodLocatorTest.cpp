#include <UnitTestHarness/TestHarness.h>
#include <Helpers/SimpleStringExtensions.h>
#include "DomainStepMethodLocator.h"
#include "MethodAndInvocationTarget.h"

#include <iostream>

namespace {
	gwt::DomainStepMethodLocator* locator;
	void SetUp(){
		locator = new gwt::DomainStepMethodLocator;
	}
	void TearDown(){
		delete locator;
	}
};

TEST(DomainStepMethodLocator, handleNoMatches)
{
	try {
		locator->find("given a value 5").invoke("given a value 5");
		FAIL("should throw exception");
	} catch (std::runtime_error& e) {
	}
}

TEST(DomainStepMethodLocator, handleMatch)
{
	gwt::MethodAndInvocationTarget* xFoo = new gwt::MethodAndInvocationTarget("X", "foo", "given a value (\\d+)");
	locator->add("X", xFoo);
	gwt::MethodAndInvocationTarget& found = locator->find("given a value 5");
	CHECK(&found == xFoo);
}

TEST(DomainStepMethodLocator, handleAmbiguousDefinitions)
{
	locator->add("X", new gwt::MethodAndInvocationTarget("X", "foo", "given a value (\\d+)"));
	locator->add("X", new gwt::MethodAndInvocationTarget("X", "bar", "given a (.*)"));

	try {
		locator->find("given a value 5");
		FAIL("should throw exception");
	} catch (std::runtime_error& e) {
	}
}
