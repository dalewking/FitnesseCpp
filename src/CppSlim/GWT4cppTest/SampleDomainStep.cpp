#include "SampleDomainStep.h"
#include "DomainStep.h"

DOMAIN_STEP("I add (\\d+)", SampleDomainStep, domainStep)
void SampleDomainStep::domainStep(int x) {}

REGISTER_CLASS(SampleDomainStep)
WITH_CONSTRUCTOR(SampleDomainStep)
WITH_1_ARG_METHOD(void, SampleDomainStep, domainStep, int)
END_REGISTRATION
