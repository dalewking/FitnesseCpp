#include <UnitTestHarness/TestHarness.h>
#include <Helpers/SimpleStringExtensions.h>

#include "Instantiator.h"
#include "MarkedClass.h"
#include "AbstractInstance.h"
#include "Class.h"
#include "Instance.h"
#include "ChainedInstantiator.h"

#include <vector>

namespace {
    class A {};
    class B {};
    slim::Class<A> classA;
    slim::Class<B> classB;
    slim::Instance<A> a(classA, 0);
    slim::Instance<B> b(classB, 0);
    gwt::MarkedClass markedClassA(&classA);
    gwt::MarkedClass markedClassB(&classB);

	void SetUp(){}
	void TearDown(){}
};


class MockInstantiator : public gwt::Instantiator {
    std::vector<slim::AbstractInstance*>& instances;
    slim::AbstractInstance* instance;

	public:
    bool called;
    bool result;
        virtual bool operator()(gwt::MarkedClass* markedClass) {
            called = true;
            if (result)
                instances.push_back(instance);
            return result;
        }
        MockInstantiator(std::vector<slim::AbstractInstance*>& instances, slim::AbstractInstance* instance) : instances(instances), instance(instance), called(false), result(true) {
        }
        virtual ~MockInstantiator() {
        }
        virtual std::vector<slim::AbstractInstance*>& getInstances() {
            return instances;
        }
        void canInstantiate(bool canDoIt) { result = canDoIt; }
};

TEST(ChainedInstantiator, noInstantiator)
{
    std::vector<slim::AbstractInstance*> instances;
    gwt::ChainedInstantiator chainedInstantiator(instances);
    CHECK(!chainedInstantiator(&markedClassA));
    LONGS_EQUAL(0, instances.size());
}

TEST(ChainedInstantiator, oneInstantiator)
{
    std::vector<slim::AbstractInstance*> instances;
    gwt::ChainedInstantiator chainedInstantiator(instances);
	MockInstantiator instantiatorA(instances, &a);

	chainedInstantiator.add(&instantiatorA);
    CHECK(chainedInstantiator(&markedClassA));

    LONGS_EQUAL(1, instances.size());
    CHECK(instantiatorA.called);
}

TEST(ChainedInstantiator, oneInstantiatorButCannotInstantiate)
{
    std::vector<slim::AbstractInstance*> instances;
    gwt::ChainedInstantiator chainedInstantiator(instances);
	MockInstantiator instantiatorA(instances, &a);
    instantiatorA.canInstantiate(false);
	chainedInstantiator.add(&instantiatorA);

    CHECK(!chainedInstantiator(&markedClassB));

    LONGS_EQUAL(0, instances.size());
    CHECK(instantiatorA.called);
}

TEST(ChainedInstantiator, secondNotCalledWhenFirstInstantiates)
{
    std::vector<slim::AbstractInstance*> instances;
    gwt::ChainedInstantiator chainedInstantiator(instances);

	MockInstantiator instantiatorA(instances, &a);
    MockInstantiator instantiatorB(instances, &b);

	chainedInstantiator.add(&instantiatorA);
	chainedInstantiator.add(&instantiatorB);

    CHECK(chainedInstantiator(&markedClassA));

    LONGS_EQUAL(1, instances.size());
    CHECK(instantiatorA.called);
    CHECK(!instantiatorB.called);
}
