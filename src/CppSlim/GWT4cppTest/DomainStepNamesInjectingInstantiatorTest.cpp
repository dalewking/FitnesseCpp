#include <UnitTestHarness/TestHarness.h>
#include <Helpers/SimpleStringExtensions.h>

#include "DomainStepNamesInjectingInstantiator.h"

#include <boost/scoped_ptr.hpp>

class MockSteps {
public:
    MockSteps(gwt::DomainStepNames* gwt){}
};

class MockDomainStepNames : public gwt::DomainStepNames {
		virtual std::string given(std::string methodString) {return "";}
		virtual std::string when(std::string methodString) {return "";}
		virtual std::string then(std::string methodString) {return "";}
		virtual std::string _and(std::string methodString) {return "";}
};

namespace {
    MockDomainStepNames domainStepNames;
    slim::Class<MockSteps>* mockStepsClass;
    gwt::MarkedClass* markedClass;

    void SetUp(){
        mockStepsClass = new slim::Class<MockSteps>("MockSteps");
        mockStepsClass->addConstructor(new slim::DirectOneArgumentConstructor<MockSteps, gwt::DomainStepNames*>());
        markedClass = new gwt::MarkedClass(mockStepsClass);
    }
    void TearDown(){
        delete mockStepsClass;
        delete markedClass;
    }
};


TEST(DomainStepNamesInjectingInstantiator, instanceIsCreated)
{
    gwt::DomainStepNamesInjectingInstantiator<MockSteps> instantiator(&domainStepNames);
    CHECK(instantiator(markedClass));
    std::vector<slim::AbstractInstance*>& instances = instantiator.getInstances();
    LONGS_EQUAL(1, instances.size());
    boost::scoped_ptr<slim::AbstractInstance> instance(instances[0]);
    CHECK_EQUAL("MockSteps", instance->className());
}

