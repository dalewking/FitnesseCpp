#include <UnitTestHarness/TestHarness.h>
#include <Helpers/SimpleStringExtensions.h>

#include "MarkedClassFinder.h"
#include "MarkedClass.h"
#include "DomainSteps.h"
#include "AbstractClassRegistry.h"
#include "DomainStepMethodLocator.h"
#include "Instances.h"

#include <vector>

namespace {
	void SetUp(){
		gwt::MarkedClassFinder::instance();
	}

	void TearDown(){
		gwt::MarkedClassFinder::cleanup();
		slim::AbstractClassRegistry::instance().clear();
		gwt::DomainStepMethodLocator::clearMappings();

		slim::Instances::clear();
	}
};

class X {
public:
	DOMAIN_STEPS(X);
};
REGISTER_CLASS(X)
END_REGISTRATION

TEST(MarkedClassFinder, registerClass)
{
	FIND_MARKED_CLASS(X);

	std::vector<gwt::MarkedClass*> markedClasses = gwt::MarkedClassFinder::instance().getMarkedClasses();
	CHECK(markedClasses.size() == 1);
	gwt::MarkedClass* markedClass = markedClasses.back();
	CHECK_EQUAL("X", markedClass->getName());
}
TEST(MarkedClassFinder, registerClass2)
{
	FIND_MARKED_CLASS(X);

	std::vector<gwt::MarkedClass*> markedClasses = gwt::MarkedClassFinder::instance().getMarkedClasses();
	CHECK(markedClasses.size() == 1);
	gwt::MarkedClass* markedClass = markedClasses.back();
	CHECK_EQUAL("X", markedClass->getName());
}
