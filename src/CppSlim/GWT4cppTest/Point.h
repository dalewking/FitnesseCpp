#ifndef __POINT__
#define __POINT__

#include <iostream>

class Point {
public:
	double x;
	double y;

	Point() : x(0.0), y(0.0) {}
	~Point() {}
	Point(double x, double y) : x(x), y(y) {}
	Point(const Point& rhs) : x(rhs.x), y(rhs.y) {}
	void operator=(const Point& rhs) { x += rhs.x; y += rhs.y; }
	void reset() { x = 0; y = 0; }
};

bool operator==(const Point& lhs, const Point& rhs);
std::ostream& operator<<(std::ostream& s, Point udt);
std::istream& operator>>(std::istream& s, Point& udt);

#endif	//	__POINT__
