#include <UnitTestHarness/TestHarness.h>
#include <Helpers/SimpleStringExtensions.h>

#include "MethodAndInvocationTarget.h"
#include "RegisterClass.h"
#include "MethodSignature.h"
#include "Instances.h"

#include "Point.h"

#include <iostream>
#include <string>

namespace {

	class Sample {};

	class SampleInstance : public slim::Instance<Sample> {
	public:
        SampleInstance(slim::Class<Sample>& sampleClass) : slim::Instance<Sample>(sampleClass, (Sample *)0){}
		std::string invokedFunction;
		std::vector<std::string> actualArgs;
        virtual std::string invoke(const slim::MethodSignature& signature) {
			invokedFunction = signature.getName();
			actualArgs = signature.getArguments();
			return "SampleInstance";
		}
	};


	SampleInstance* sampleInstance;
    void SetUp(){
        slim::Class<Sample> sampleClass;
		sampleInstance = new SampleInstance(sampleClass);
		slim::Instances::registerInstance("Sample_target", sampleInstance);
	}
    void TearDown(){
		slim::Instances::clear();
	}
};

TEST(MethodAndInvocationTarget, invoke)
{
	gwt::MethodAndInvocationTarget methodOnTarget("Sample", "foo", "ignored");
	methodOnTarget.invoke("");
	CHECK_EQUAL("foo", sampleInstance->invokedFunction);
	CHECK(sampleInstance->actualArgs.empty());
}

TEST(MethodAndInvocationTarget, invoke_ArgumentParsedFromString)
{
	gwt::MethodAndInvocationTarget methodOnTarget("Sample", "bar", "call bar (\\d+)");

	std::string step = "call bar 5";
	CHECK(methodOnTarget.matches(step));

	methodOnTarget.invoke(step);

	CHECK_EQUAL("bar", sampleInstance->invokedFunction);
	LONGS_EQUAL(1, sampleInstance->actualArgs.size());
	CHECK_EQUAL("5", sampleInstance->actualArgs.back());
}

TEST(MethodAndInvocationTarget, invoke_MultipleArgumentsParsedFromString)
{
	gwt::MethodAndInvocationTarget methodOnTarget("Sample", "operate", "use (\\d+) and (.*)");

	std::string step = "use 5 and add";
	CHECK(methodOnTarget.matches(step));

	methodOnTarget.invoke(step);
	CHECK_EQUAL("operate", sampleInstance->invokedFunction);
	LONGS_EQUAL(2, sampleInstance->actualArgs.size());
	CHECK_EQUAL("5", sampleInstance->actualArgs[0]);
	CHECK_EQUAL("add", sampleInstance->actualArgs[1]);
}

TEST(MethodAndInvocationTarget, invoke_UserDefinedTypeAsArgument)
{
	gwt::MethodAndInvocationTarget methodOnTarget("Sample", "magnitude", "mag (\\(\\d+,\\d+\\))");

	std::string step = "mag (5,4)";
	CHECK(methodOnTarget.matches(step));
	methodOnTarget.invoke(step);

	CHECK_EQUAL("magnitude", sampleInstance->invokedFunction);
	LONGS_EQUAL(1, sampleInstance->actualArgs.size());
	CHECK_EQUAL("(5,4)", sampleInstance->actualArgs[0]);
}

TEST(MethodAndInvocationTarget, matches)
{
	gwt::MethodAndInvocationTarget methodOnTarget("Sample", "foo", "hello");
	CHECK(methodOnTarget.matches("hello"));
}

TEST(MethodAndInvocationTarget, canCopy)
{
	gwt::MethodAndInvocationTarget methodOnTarget("Sample", "foo", "hello");
	gwt::MethodAndInvocationTarget copy(methodOnTarget);
	CHECK(copy.matches("hello"));
	copy.invoke("hello");
	CHECK_EQUAL("foo", sampleInstance->invokedFunction);
	CHECK(sampleInstance->actualArgs.empty());
}

TEST(MethodAndInvocationTarget, exceptionWhenNoTarget)
{
	slim::Instances::clear();
	gwt::MethodAndInvocationTarget methodOnTarget("Sample", "foo", "hello");
	CHECK(methodOnTarget.matches("hello"));

	try {
		methodOnTarget.invoke("hello");
		FAIL("should throw exception");
	} catch (std::exception&) {
	}
}

