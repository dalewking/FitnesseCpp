#include <UnitTestHarness/TestHarness.h>
#include <Helpers/SimpleStringExtensions.h>

#include "DomainStep.h"
#include "DomainStepsForSlim.h"
#include "MethodAndInvocationTarget.h"
#include "Point.h"
#include "DomainStepMethodLocator.h"
#include "SlimException.h"
#include "DomainSteps.h"
#include "Instances.h"

#include <string>
#include <algorithm>
#include <functional>
#include <boost/scoped_ptr.hpp>

class Path {
public:
	static Point lastPoint;

	DOMAIN_STEPS(Path);

	void addPoint(Point newPoint);

	bool hasPoint(const Point& point);
};

void Path::addPoint(Point newPoint) {
	lastPoint = newPoint;
}

bool Path::hasPoint(const Point& point) {
	return lastPoint == point;
}

Point Path::lastPoint;

REGISTER_CLASS(Path)
	WITH_CONSTRUCTOR(Path)
	WITH_1_ARG_METHOD(void, Path, addPoint, Point)
END_REGISTRATION



namespace {
    void SetUp(){
		FIND_MARKED_CLASS(Path);
		DOMAIN_STEP("a point at (\\(\\d+,\\d+\\))", Path, addPoint);
	}
    void TearDown(){
		Path::lastPoint.reset();
		slim::Instances::clear();
		gwt::MarkedClassFinder::instance().cleanup();
		slim::AbstractClassRegistry::instance().clear();
		gwt::DomainStepMethodLocator::clearMappings();
	}
};

TEST(DomainStepsForSlim, noStepDefinition)
{
	std::string methodString = "some bogus non-existant step";

	gwt::DomainStepsForSlim fixture;
	try {
		fixture.given(methodString);
		FAIL("should throw exception");
	} catch(std::runtime_error& e) {
		CHECK_EQUAL("You need a step class with an annotated method matching this pattern: '" + methodString + "'\n" +
            "Example:\n" +
            "  class StepClass {\n" +
			"    public:\n" +
            "      DOMAIN_STEP(\"" + methodString + "\", StepClass, domainStep)\n" +
			"      void domainStep() {\n" +
            "      // TODO implement step\n" +
            "      }\n\n" +
			"      DOMAIN_STEPS(StepClass);\n" + 
			"  };\n" +
			"  REGISTER_CLASS(StepClass)\n" +
			"    WITH_CONSTRUCTOR(StepClass)\n" +
			"    WITH_METHOD(void, StepClass, domainStep)\n" +
			"  END_REGISTRATION\n", e.what());
	}
}

TEST(DomainStepsForSlim, macro)
{
	gwt::DomainStepMethodLocator::clearMappings();
	gwt::DomainStepsForSlim fixture;
	try {
		fixture.given("I add 52");
	} catch (std::runtime_error&) {
	}
}


TEST(DomainStepsForSlim, given)
{
	gwt::DomainStepsForSlim fixture;
	fixture.given("a point at (3,5)");

	Path path;
	CHECK(path.hasPoint(Point(3,5)));
}

TEST(DomainStepsForSlim, givenCalledTwiceOnSameObject)
{
	gwt::DomainStepsForSlim fixture;
	fixture.given("a point at (3,5)");
	fixture.given("a point at (3,5)");
	Path path;
	CHECK(path.hasPoint(Point(6, 10)));
}

class X {
public:
	virtual void operator()(const std::string s) = 0;
	virtual ~X() = 0;
};

X::~X(){}

class Y : public X {
public:
    std::list<std::string>& strings;

    Y(std::list<std::string>& received) : strings(received){}
	void operator()(const std::string s) { strings.push_back(s); /*std::cout << "{" << s << "}" << std::endl; */};
};

TEST(bind1st, callMember)
{
	std::list<std::string> strings;
	strings.push_back("Hello");
	strings.push_back("World");

	std::list<std::string> received;
	boost::scoped_ptr<X> x(new Y(received));

	std::for_each(strings.begin(), strings.end(), std::bind1st(std::mem_fun(&X::operator()), x.get()));
    LONGS_EQUAL(2, received.size());
    CHECK_EQUAL("Hello", received.front());
    received.pop_front();
    CHECK_EQUAL("World", received.front());
}


