#include <UnitTestHarness/CommandLineTestRunner.h>
#include <boost/regex.hpp>
#include "Instances.h"
#include "RegEx.h"

int main(int ac, char** av)
{
    // This is done so that the memory leaked by
    // the regex library is not recorded during
    // the test run
  boost::regex expression("^$"); 
  boost::smatch what;
  boost::regex_match(std::string(""), what, expression);
  slim::Instances::clear();
  gwt::RegExArgCollector regex("^.$");
  return CommandLineTestRunner::RunAllTests(ac, av);
}
