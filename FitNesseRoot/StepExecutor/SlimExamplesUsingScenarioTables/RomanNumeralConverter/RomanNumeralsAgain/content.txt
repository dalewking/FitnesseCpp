|values as roman numerals|
|value|romanNumeral|
|1|I|
|2|II|
|3|III|
|4|IV|
|2009|MMIX|
| 17 | XVII  |
| 1953 | MCMLIII  |
| 3303 | MMMCCCIII  |
|1934| MCMXXXIV |
